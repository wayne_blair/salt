# NOTE: 
#      * Cassandra and Datastax follow a non-standard RPM package version standard
#        The version of the product is defined by the version specific in the repository URL
#        in files/datastac-ddc.repo
#
#      * The salt tree has repo files for both Datastax repos for older versions of 
#        community Cassandra  (cassandra.repo) and the version 3 community Cassandra
#        that is now called datastax-ddc (datastax-ddc.repo)
#
# TODO:
#      * find syntax to extract cassandra home directory from a grain.
#      * DEV-9123 said we need to run cassandra with Oracle jre 1.7.
#        The intention is to install the oracle 1.7 and adapt the 
#        cassandra user to define JAVA_HOME accordingly.
#
#        This encountered two unexpected gotcha that are not yet resolved.
#          1) The cassandra package created the cassandra logon user without
#             the expected .bash* files. This state creates them.
#          2) The cassandra init script does not source in the user environment
#             and does not pick up the .bashrc that defined JAVA_HOME therfore
#             it uses the system wide java and we don't have a clean solution
#             will have to modify the package provided init script and have it source
#             in the cassandra user environment.
#
#        Since this was written, Oracle jre 1.7 requirement has been removed but I 
#        am leaving the code intact because I expect this is not the end of 
#        a specific jre version requirement from TK.
#
#        This state will need the following changes if the requirement returns:
#           1) Use a user grain to obtain the cassandra user's home directory; it 
#              is currently hardcoded to /var/lib/cassandra
#           2) Write a state that will have "su" source in the user environment
#              in /etc/init.d/cassandra
#           3) Redefine the JAVA variable to the state of the desired jre
#              ("oracle-jre" or "openjdk")
#
#

# {#% set JAVA = 'oracle-jre' %#}
{% set JAVA = 'openjdk' %}
{% set NAME = 'cassandra' %}
{% set RELEASE = '21' %}
{% set HOME = '/var/lib/cassandra' %}

include:
  - {{ JAVA }}

/etc/yum.repos.d/{{ NAME }}.repo:
  file.managed:
    - source: salt://cassandra/files/{{ NAME }}.repo
    - user: root
    - group: root
    - mode: '0440'


{{ NAME }}{{RELEASE}}:
  pkg.installed:
    - require:
      - file: /etc/yum.repos.d/{{ NAME }}.repo
      - sls: {{ JAVA }}

{{ NAME }}{{RELEASE}}-tools:
  pkg.installed:
    - require:
      - pkg: {{ NAME}}{{ RELEASE }}

# NOTE:  The cassandra account created by the datastax rpm is missing its copy of the /etc/skel
#        files which we need to define JAVA_HOME and add it to the path.
#        I will manage these independently for now.
#
#        the bash_* files seem to be required. The environment is not set when the bash_* files are missing.

{% for FILE in 'bashrc', 'bash_logout', 'bash_profile' %}
{{ HOME }}/.{{ FILE }}:
  file.managed:
    - source: salt://cassandra/files/{{ FILE }}
    - user: {{ NAME }}
    - group: {{ NAME }}
    - file_mode: 644
    - require:
      - pkg: {{ NAME }}{{ RELEASE }}
{% endfor %}

# The salt service module cannot be use for managing SYS-V serivces.
# I have to break this down into seperate cmd.run calls
# The salt service functionality runs systemctl list-unit-files and expect to find a .service
# file.  That file does not exist for SYS-V scripts because it is auto-generated when
# systemd is initialized or reloaded.

{{ NAME }}_enable:
  cmd.run:
    - name: chkconfig cassandra on
    - requires:
      - pkg: {{ NAME }}{{ RELEASE }}
      - file: {{ HOME }}/.bashrc

{{ NAME }}_start:
  cmd.run:
    - name: service cassandra restart
    - require:
      - cmd: {{ NAME }}_enable



