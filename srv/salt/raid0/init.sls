#/dev/md0:
#  raid.present:
#    - opts:
#      - level=0 
#      - chunk=256
#      - raid-devices=2
#      - /dev/sdc
#      - /dev/sdd

/dev/md0:
  raid.present:
    - level: 0
    - devices:
      - /dev/sdc
      - /dev/sdd
    - chunk: 256
    - run: True

