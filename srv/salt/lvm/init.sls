/dev/sdb:
  lvm.pv_present

/dev/sdc:
  lvm.pv_present

saltVG:
  lvm.vg_present:
    - devices: /dev/sdb,/dev/sdc
    - require:
      - lvm: /dev/sdb
      - lvm: /dev/sdc

saltLV:
  lvm.lv_present:
    - vgname: saltVG
    - size: 500M
    - require:
      - lvm: saltVG

