common-pkgs:
 pkg.latest:
    - pkgs:
      - unzip
      - wget
      - git
      - at
      - mlocate
      - lsof
      - dos2unix
      - sysstat

