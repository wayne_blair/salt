{% set VERSION = '9.4' %}
{% set ANNOYING_VERSION = '94' %}

pg_repo_pkg:
  pkg.installed:
    - sources:
      - pgdb-centos94: http://yum.postgresql.org/9.4/redhat/rhel-7-x86_64/pgdg-centos94-9.4-2.noarch.rpm

postgresql_server_pkg:
  pkg.installed:
    - pkgs:
      - postgresql{{  ANNOYING_VERSION }}
      - postgresql{{  ANNOYING_VERSION }}-server
      - postgresql{{  ANNOYING_VERSION }}-contrib
      - postgresql{{  ANNOYING_VERSION }}-libs
      - postgresql{{  ANNOYING_VERSION }}-plperl
      - postgresql{{  ANNOYING_VERSION }}-plpython
    - requires:
      - pkg: pg_repo_pkg

postgresql_config_cmd:
  cmd.run:
    - name: /usr/pgsql-{{ VERSION }}/bin/postgresql94-setup initdb
    - creates: /var/lib/pgsql/{{ VERSION }}/data/postgresql.conf
    - requires:
      - pkg: postgresql_server_pkg

postgresql_config_file:
  file.managed:
    - name: /var/lib/pgsql/{{ VERSION }}/data/postgresql.conf
    - source: salt://postgresql/files/postgresql.conf
    - user: postgres
    - mode: '0600'
    - require:
      - pkg: postgresql_server_pkg

postgresql_hba_file:
  file.managed:
    - name: /var/lib/pgsql/{{ VERSION }}/data/pg_hba.conf
    - source: salt://postgresql/files/pg_hba.conf
    - user: postgres
    - mode: '0600'
    - require:
      - pkg: postgresql_server_pkg



postgresql_service:
  service:
    - name: postgresql-{{ VERSION }}
    - running
    - enable: True
    - watch:
      - file: postgresql_config_file
    - requires:
      - file: postgresql_config_file
      - file: postgresql_hba_file
