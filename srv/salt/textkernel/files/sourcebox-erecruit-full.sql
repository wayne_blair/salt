--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: auditing_stamp(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION auditing_stamp() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    	BEGIN
        	NEW.date_modified := current_timestamp;
	        RETURN NEW;
    	END;
		$$;


ALTER FUNCTION public.auditing_stamp() OWNER TO postgres;

--
-- Name: create_or_adapt_sourcebox_schema(character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION create_or_adapt_sourcebox_schema(owner_user character varying, webapp_user character varying) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
DECLARE
    res BOOLEAN;
BEGIN

	SET client_encoding = 'UTF8';
	SET check_function_bodies = false;
	SET client_min_messages = warning;

	COMMENT ON SCHEMA public IS 'Standard public schema';
	RAISE INFO 'Creating/adapting sourcebox for owner_user %, webapp_user %', owner_user, webapp_user;


	SET search_path = public, pg_catalog;

	SET default_tablespace = '';

	SET default_with_oids = true;
	IF NOT EXISTS (SELECT * FROM pg_tables where tablename = 'client') THEN
		BEGIN
			CREATE TABLE client (
				id integer DEFAULT nextval(('client_id_seq'::text)::regclass) NOT NULL,
				enabled char DEFAULT 't',
				valid_until date,
				name character varying,
				credit_limit integer,
				product integer,
				account character varying,
				fourweekperiod char DEFAULT 'f',
				savepath character varying,
				has_custom_save_path char DEFAULT 'f',
				model character varying,
				start_date date,
				lang character varying,
				interface_lang character varying,
				external_repository character varying,
				external_repository_activated char,
				external_account_id character varying,
				persisting char DEFAULT 'f',
				cassandra_enabled char DEFAULT 'f',
				saml_idp character varying,
				saml_domain_mapping character varying,
				parent integer,
				custom_string_1 character varying,
				custom_string_2 character varying,
				custom_string_3 character varying,
				custom_string_4 character varying,
				custom_string_5 character varying,
				custom_string_6 character varying,
				custom_string_7 character varying,
				custom_string_8 character varying,
				custom_string_9 character varying,
				custom_string_10 character varying,
				custom_string_11 character varying,
				custom_string_12 character varying,
				custom_string_13 character varying,
				custom_string_14 character varying,
				custom_string_15 character varying,
				custom_string_16 character varying,
				custom_string_17 character varying,
				custom_string_18 character varying,
				custom_string_19 character varying,
				custom_string_20 character varying,
				custom_int_1 integer,
				custom_int_2 integer,
				custom_int_3 integer,
				custom_int_4 integer,
				custom_tstamp_1 timestamp without time zone,
				custom_tstamp_2 timestamp without time zone,
				custom_boolean_1 char,
				custom_boolean_2 char,
				custom_boolean_3 char,
				custom_boolean_4 char,
				custom_boolean_5 char,
				custom_boolean_6 char,
				linkedin_email character varying,
				linkedin_password character varying,
				linkedin_access_token character varying,
				linkedin_secret_token character varying,
				linkedin_activated char,
                search_activated char,
                search_config character varying,
                search_config_password character varying,
                search_url character varying,
                indexing_url character varying,
                indexing_config character varying,
                indexing_config_password character varying,
                last_indexing_update bigint,
                date_created timestamp default now() NOT NULL,
				date_modified timestamp,
				bullhorn_subscription_id character varying,
				bullhorn_last_request_id bigint,
				custom_search_roles_ro character varying,
				document_sharing_ro character varying
			);
			RAISE INFO 'Table `client` successfully created';
			EXCEPTION WHEN OTHERS THEN
			RAISE INFO 'Error occurred while creating table `client`';
			RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;


		END;
	ELSE
		BEGIN
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='external_account_id') THEN
                ALTER TABLE client ADD COLUMN external_account_id character varying;
            END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='custom_boolean_1') THEN
				ALTER TABLE client ADD COLUMN custom_boolean_1 char;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='custom_boolean_2') THEN
				ALTER TABLE client ADD COLUMN custom_boolean_2 char;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='custom_boolean_3') THEN
				ALTER TABLE client ADD COLUMN custom_boolean_3 char;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='custom_boolean_4') THEN
				ALTER TABLE client ADD COLUMN custom_boolean_4 char;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='custom_boolean_5') THEN
				ALTER TABLE client ADD COLUMN custom_boolean_5 char;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='custom_boolean_6') THEN
				ALTER TABLE client ADD COLUMN custom_boolean_6 char;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='linkedin_email') THEN
				ALTER TABLE client ADD COLUMN linkedin_email character varying;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='linkedin_password') THEN
				ALTER TABLE client ADD COLUMN linkedin_password character varying;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='linkedin_access_token') THEN
				ALTER TABLE client ADD COLUMN linkedin_access_token character varying;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='linkedin_secret_token') THEN
				ALTER TABLE client ADD COLUMN linkedin_secret_token character varying;
			END IF;
            IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='linkedin_activated') THEN
                ALTER TABLE client ADD COLUMN linkedin_activated char;
            END IF;
            IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='search_activated') THEN
                ALTER TABLE client ADD COLUMN search_activated char;
            END IF;
            IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='search_config') THEN
                ALTER TABLE client ADD COLUMN search_config character varying;
            END IF;
            IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='search_config_password') THEN
                ALTER TABLE client ADD COLUMN search_config_password character varying;
            END IF;
            IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='search_url') THEN
                ALTER TABLE client ADD COLUMN search_url character varying;
            END IF;
            IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='indexing_url') THEN
                ALTER TABLE client ADD COLUMN indexing_url character varying;
            END IF;
            IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='indexing_config') THEN
                ALTER TABLE client ADD COLUMN indexing_config character varying;
            END IF;
            IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='indexing_config_password') THEN
                ALTER TABLE client ADD COLUMN indexing_config_password character varying;
            END IF;
            IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='cassandra_enabled') THEN
                ALTER TABLE client ADD COLUMN cassandra_enabled char DEFAULT 'f';
                RAISE INFO 'Added column cassandra_enabled on table client.';
            END IF;
            IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='last_indexing_update') THEN
                ALTER TABLE client ADD COLUMN last_indexing_update bigint;
				RAISE INFO 'Added column last_indexing_update on table client.';
            END IF;
            IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='date_created') THEN
				ALTER TABLE client ADD COLUMN date_created timestamp;
				UPDATE client SET date_created = ('01-01-2000');
				ALTER TABLE client ALTER COLUMN date_created SET default now();
				ALTER TABLE client ALTER COLUMN date_created SET NOT NULL;
				RAISE INFO 'Column `client.date_created` successfully added';
			END IF;
            IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='date_modified') THEN
				ALTER TABLE client ADD COLUMN date_modified timestamp;
				RAISE INFO 'Column `client.date_modified` successfully added';
			END IF;
			IF EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='saveflag') THEN
                ALTER TABLE client RENAME COLUMN saveflag TO has_custom_save_path;
                RAISE INFO 'Renamed colum `savepath` to `has_custom_save_path` in table `client`';
            END IF;
            IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name='client' AND data_type='boolean') THEN
				ALTER TABLE client
					ALTER COLUMN enabled DROP DEFAULT,
					ALTER COLUMN fourweekperiod DROP DEFAULT,
					ALTER COLUMN has_custom_save_path DROP DEFAULT,
					ALTER COLUMN persisting DROP DEFAULT;
				ALTER TABLE client 
					ALTER COLUMN enabled TYPE char USING CASE WHEN enabled THEN 't' ELSE 'f' END,
					ALTER COLUMN fourweekperiod TYPE char USING CASE WHEN fourweekperiod THEN 't' ELSE 'f' END,
					ALTER COLUMN has_custom_save_path TYPE char USING CASE WHEN has_custom_save_path THEN 't' ELSE 'f' END,
					ALTER COLUMN persisting TYPE char USING CASE WHEN persisting THEN 't' ELSE 'f' END,
					ALTER COLUMN external_repository_activated TYPE char USING CASE WHEN external_repository_activated IS NOT NULL THEN CASE WHEN external_repository_activated THEN 't' ELSE 'f' END ELSE NULL END,
					ALTER COLUMN custom_boolean_1 TYPE char USING CASE WHEN custom_boolean_1 IS NOT NULL THEN CASE WHEN custom_boolean_1 THEN 't' ELSE 'f' END ELSE NULL END,
					ALTER COLUMN custom_boolean_2 TYPE char USING CASE WHEN custom_boolean_2 IS NOT NULL THEN CASE WHEN custom_boolean_2 THEN 't' ELSE 'f' END ELSE NULL END,
					ALTER COLUMN custom_boolean_3 TYPE char USING CASE WHEN custom_boolean_3 IS NOT NULL THEN CASE WHEN custom_boolean_3 THEN 't' ELSE 'f' END ELSE NULL END,
					ALTER COLUMN custom_boolean_4 TYPE char USING CASE WHEN custom_boolean_4 IS NOT NULL THEN CASE WHEN custom_boolean_4 THEN 't' ELSE 'f' END ELSE NULL END,
					ALTER COLUMN custom_boolean_5 TYPE char USING CASE WHEN custom_boolean_5 IS NOT NULL THEN CASE WHEN custom_boolean_5 THEN 't' ELSE 'f' END ELSE NULL END,
					ALTER COLUMN custom_boolean_6 TYPE char USING CASE WHEN custom_boolean_6 IS NOT NULL THEN CASE WHEN custom_boolean_6 THEN 't' ELSE 'f' END ELSE NULL END,
					ALTER COLUMN linkedin_activated TYPE char USING CASE WHEN linkedin_activated IS NOT NULL THEN CASE WHEN linkedin_activated THEN 't' ELSE 'f' END ELSE NULL END,
					ALTER COLUMN search_activated TYPE char USING CASE WHEN search_activated IS NOT NULL THEN CASE WHEN search_activated THEN 't' ELSE 'f' END ELSE NULL END;
				ALTER TABLE client 
					ALTER COLUMN enabled SET DEFAULT 't',
					ALTER COLUMN fourweekperiod SET DEFAULT 'f',
					ALTER COLUMN has_custom_save_path SET DEFAULT 'f',
					ALTER COLUMN persisting SET DEFAULT 'f';
				RAISE INFO 'Boolean columns in `client` successfully changed to char';
			END IF;
            IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='saml_idp') THEN
				ALTER TABLE client ADD COLUMN saml_idp character varying;
				RAISE INFO 'Column `client.saml_idp` successfully added';
			END IF;
            IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='saml_domain_mapping') THEN
				ALTER TABLE client ADD COLUMN saml_domain_mapping character varying;
				RAISE INFO 'Column `client.saml_domain_mapping` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='bullhorn_subscription_id') THEN
                ALTER TABLE client ADD COLUMN bullhorn_subscription_id character varying;
				RAISE INFO 'Column `client.bullhorn_subscription_id` successfully added';
            END IF;
            IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='bullhorn_last_request_id') THEN
                ALTER TABLE client ADD COLUMN bullhorn_last_request_id bigint;
				RAISE INFO 'Column `client.bullhorn_last_request_id` successfully added';
            END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='custom_string_10') THEN
				ALTER TABLE client ADD COLUMN custom_string_10 character varying;
				RAISE INFO 'Column `client.custom_string_10` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='custom_string_11') THEN
				ALTER TABLE client ADD COLUMN custom_string_11 character varying;
				RAISE INFO 'Column `client.custom_string_11` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='custom_string_12') THEN
				ALTER TABLE client ADD COLUMN custom_string_12 character varying;
				RAISE INFO 'Column `client.custom_string_12` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='custom_string_13') THEN
				ALTER TABLE client ADD COLUMN custom_string_13 character varying;
				RAISE INFO 'Column `client.custom_string_13` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='custom_string_14') THEN
				ALTER TABLE client ADD COLUMN custom_string_14 character varying;
				RAISE INFO 'Column `client.custom_string_14` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='custom_string_15') THEN
				ALTER TABLE client ADD COLUMN custom_string_15 character varying;
				RAISE INFO 'Column `client.custom_string_15` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='custom_string_16') THEN
				ALTER TABLE client ADD COLUMN custom_string_16 character varying;
				RAISE INFO 'Column `client.custom_string_16` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='custom_string_17') THEN
				ALTER TABLE client ADD COLUMN custom_string_17 character varying;
				RAISE INFO 'Column `client.custom_string_17` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='custom_string_18') THEN
				ALTER TABLE client ADD COLUMN custom_string_18 character varying;
				RAISE INFO 'Column `client.custom_string_18` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='custom_string_19') THEN
				ALTER TABLE client ADD COLUMN custom_string_19 character varying;
				RAISE INFO 'Column `client.custom_string_19` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='custom_string_20') THEN
				ALTER TABLE client ADD COLUMN custom_string_20 character varying;
				RAISE INFO 'Column `client.custom_string_20` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='custom_search_roles_ro') THEN
				ALTER TABLE client ADD COLUMN custom_search_roles_ro character varying;
				RAISE INFO 'Column `client.custom_search_roles_ro` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='client' AND column_name='document_sharing_ro') THEN
				ALTER TABLE client ADD COLUMN document_sharing_ro character varying;
				RAISE INFO 'Column `client.document_sharing_ro` successfully added';
			END IF;
			
			RAISE INFO 'Table `client` its columns are verified and possibly updated.';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating table `client`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;

		END;

	END IF;
	EXECUTE 'ALTER TABLE public.client OWNER TO ' || owner_user;
	IF NOT EXISTS (SELECT * FROM pg_statio_user_sequences where relname = 'client_id_seq') THEN
		BEGIN
			CREATE SEQUENCE client_id_seq
				INCREMENT BY 1
				NO MAXVALUE
				NO MINVALUE
				CACHE 1;
			RAISE INFO 'Sequence `client_id_seq` successfully created';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating sequence `client_id_seq`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;
	EXECUTE 'ALTER TABLE public.client_id_seq OWNER TO ' || owner_user;
	IF NOT EXISTS (SELECT * FROM pg_tables where tablename = 'codelist') THEN
		BEGIN
			CREATE TABLE codelist (
				id integer DEFAULT nextval(('codelist_id_seq'::text)::regclass) NOT NULL,
				client integer,
				numcode character varying,
				alphacode character varying,
				fieldname character varying,
				version character varying,
				versiondate date,
				description character varying,
				codeid character varying,
				date_created timestamp default now() NOT NULL,
				date_modified timestamp
			);
			RAISE INFO 'Table `codelist` successfully created';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating table `codelist`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	ELSE
		BEGIN
            IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='codelist' AND column_name='date_created') THEN
				ALTER TABLE codelist ADD COLUMN date_created timestamp;
				UPDATE codelist SET date_created = ('01-01-2000');
				ALTER TABLE codelist ALTER COLUMN date_created SET default now();
				ALTER TABLE codelist ALTER COLUMN date_created SET NOT NULL;
				RAISE INFO 'Column `codelist.date_created` successfully added';
			END IF;
			IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='codelist' AND column_name='date_modified') THEN
				ALTER TABLE codelist ADD COLUMN date_modified timestamp;
				RAISE INFO 'Column `codelist.date_modified` successfully added';
			END IF;
			RAISE INFO 'Table `codelist` its columns are verified and possibly updated.';
		EXCEPTION WHEN OTHERS THEN
			RAISE INFO 'Error occurred while altering table `codelist`';
			RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
			
		END;
	END IF;


	EXECUTE 'ALTER TABLE public.codelist OWNER TO ' || owner_user;
	IF NOT EXISTS (SELECT * FROM pg_tables where tablename = 'codelist_def') THEN
		BEGIN
			CREATE TABLE codelist_def (
				id serial NOT NULL,
				client integer,
				fieldname character varying,
				version character varying,
				versiondate date,
				current_ind char,
				versiontime time without time zone,
				date_created timestamp default now() NOT NULL,
				date_modified timestamp
			);
			RAISE INFO 'Table `codelist_def` successfully created';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating table `codelist_def`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	ELSE
		BEGIN
            IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='codelist_def' AND column_name='date_created') THEN
				ALTER TABLE codelist_def ADD COLUMN date_created timestamp;
				UPDATE codelist_def SET date_created = ('01-01-2000');
				ALTER TABLE codelist_def ALTER COLUMN date_created SET default now();
				ALTER TABLE codelist_def ALTER COLUMN date_created SET NOT NULL;
				RAISE INFO 'Column `codelist_def.date_created` successfully added';
			END IF;
			IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='codelist_def' AND column_name='date_modified') THEN
				ALTER TABLE codelist_def ADD COLUMN date_modified timestamp;
				RAISE INFO 'Column `codelist_def.date_modified` successfully added';
			END IF;
			IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name='codelist_def' AND column_name='current') THEN
				ALTER TABLE codelist_def RENAME current TO current_ind;
				RAISE INFO 'Column `codelist_def.current` successfully renamed';
			END IF;
            IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name='codelist_def' AND data_type='boolean') THEN
				ALTER TABLE codelist_def 
					ALTER COLUMN current_ind TYPE char USING CASE WHEN current_ind IS NOT NULL THEN CASE WHEN current_ind THEN 't' ELSE 'f' END ELSE NULL END;
				RAISE INFO 'Boolean column in `codelist_def` successfully changed to char';
			END IF;
			RAISE INFO 'Table `codelist_def` its columns are verified and possibly updated.';
		EXCEPTION WHEN OTHERS THEN
			RAISE INFO 'Error occurred while altering table `codelist_def`';
			RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;
	EXECUTE 'ALTER TABLE public.codelist_def OWNER TO ' || owner_user;

	IF NOT EXISTS (SELECT * FROM pg_statio_user_sequences where relname = 'codelist_id_seq') THEN
		BEGIN
			CREATE SEQUENCE codelist_id_seq
				INCREMENT BY 1
				NO MAXVALUE
				NO MINVALUE
				CACHE 1;
			RAISE INFO 'Sequence `codelist_id_seq` successfully created';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating sequence `codelist_id_seq`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;
	EXECUTE 'ALTER TABLE public.codelist_id_seq OWNER TO ' || owner_user;

	IF NOT EXISTS (SELECT * FROM pg_tables where tablename = 'codeproperty') THEN
		BEGIN
			CREATE TABLE codeproperty (
				id integer DEFAULT nextval(('codeproperty_id_seq'::text)::regclass) NOT NULL,
				codelistid integer,
				propertyname character varying,
				propertyvalue character varying,
				date_created timestamp default now() NOT NULL,
				date_modified timestamp
			);
			RAISE INFO 'Table `codeproperty` successfully created';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating table `codeproperty`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	ELSE
		BEGIN
            IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='codeproperty' AND column_name='date_created') THEN
				ALTER TABLE codeproperty ADD COLUMN date_created timestamp;
				UPDATE codeproperty SET date_created = ('01-01-2000');
				ALTER TABLE codeproperty ALTER COLUMN date_created SET default now();
				ALTER TABLE codeproperty ALTER COLUMN date_created SET NOT NULL;
				RAISE INFO 'Column `codeproperty.date_created` successfully added';
			END IF;
			IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='codeproperty' AND column_name='date_modified') THEN
				ALTER TABLE codeproperty ADD COLUMN date_modified timestamp;
				RAISE INFO 'Column `codeproperty.date_modified` successfully added';
			END IF;
			RAISE INFO 'Table `codeproperty` its columns are verified and possibly updated.';
		EXCEPTION WHEN OTHERS THEN
			RAISE INFO 'Error occurred while altering table `codeproperty`';
			RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;
	EXECUTE 'ALTER TABLE public.codeproperty OWNER TO ' || owner_user;

	IF NOT EXISTS (SELECT * FROM pg_statio_user_sequences where relname = 'codeproperty_id_seq') THEN
		BEGIN
			CREATE SEQUENCE codeproperty_id_seq
				INCREMENT BY 1
				NO MAXVALUE
				NO MINVALUE
				CACHE 1;
			RAISE INFO 'Sequence `codeproperty_id_seq` successfully created';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating sequence `codeproperty_id_seq`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;
	EXECUTE 'ALTER TABLE public.codeproperty_id_seq OWNER TO ' || owner_user;

	SET default_with_oids = false;
	IF NOT EXISTS (SELECT * FROM pg_tables where tablename = 'event_document') THEN
		BEGIN
			CREATE TABLE event_document (
				trxmlid integer NOT NULL,
				userid integer NOT NULL,
				client integer NOT NULL,
				document_name character varying,
				sent_date timestamp without time zone,
				received_date timestamp without time zone,
				ext_candidate_id character varying,
				oportunity_id character varying,
				candidate_name character varying,
				candidate_email character varying,
				candidate_birthdate date,
				unitid integer,
				date_created timestamp without time zone DEFAULT now() NOT NULL
				
			);
			RAISE INFO 'Table `event_document` successfully created';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating table `event_document`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	ELSE
		IF EXISTS(SELECT * FROM information_schema.columns WHERE table_name='event_document' AND column_name='userid') THEN
			ALTER TABLE event_document ALTER COLUMN userid SET NOT NULL;
		ELSE
			RAISE EXCEPTION 'Table `event_document` does not contain column `userid`';
		END IF;
		IF EXISTS(SELECT * FROM information_schema.columns WHERE table_name='event_document' AND column_name='client') THEN
			ALTER TABLE event_document ALTER COLUMN client SET NOT NULL;
		ELSE
			RAISE EXCEPTION 'Table `event_document` does not contain column `client`';
		END IF;
		RAISE INFO 'Table `event_document` its columns are verified and possibly updated.';
	END IF;
	EXECUTE 'ALTER TABLE public.event_document OWNER TO ' || owner_user;

	IF NOT EXISTS (SELECT * FROM pg_tables where tablename = 'event_history') THEN
		BEGIN
			CREATE TABLE event_history (
				id integer DEFAULT nextval(('event_history_seq'::text)::regclass) NOT NULL,
				trxmlid integer,
				userid integer,
				event_type character varying,
				event_date timestamp without time zone,
				event_content character varying,
				event_note character varying
			);
			RAISE INFO 'Table `event_history` successfully created';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating table `event_history`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;
	EXECUTE 'ALTER TABLE public.event_history OWNER TO ' || owner_user;

	IF NOT EXISTS (SELECT * FROM pg_statio_user_sequences where relname = 'event_history_seq') THEN
		BEGIN
			CREATE SEQUENCE event_history_seq
				INCREMENT BY 1
				NO MAXVALUE
				NO MINVALUE
				CACHE 1;
			RAISE INFO 'Sequence `event_history_seq` successfully created';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating sequence `event_history_seq`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;
	EXECUTE 'ALTER TABLE public.event_history_seq OWNER TO ' || owner_user;
	EXECUTE 'ALTER TABLE public.event_history_seq OWNER TO ' || owner_user;
	SET default_with_oids = true;

	--
	-- Name: processing_unit; Type: TABLE; Schema: public; Owner:; Tablespace:
	--
	IF NOT EXISTS (SELECT * FROM pg_tables where tablename = 'processing_unit') THEN
		BEGIN
			CREATE TABLE processing_unit (
				id serial NOT NULL,
				pu_date date NOT NULL default current_date,
				"time" time(0) without time zone NOT NULL default current_time,
				client integer NOT NULL,
				username character varying NOT NULL,
				documentname character varying,
				test char,
				branch character varying,
				cancel char
			);
			RAISE INFO 'Table `processing_unit` successfully created';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating table `processing_unit`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	ELSE
		BEGIN
			IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name='processing_unit' AND column_name='date') 
	        	AND NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='processing_unit' AND column_name='pu_date') 
	        THEN
    	    	ALTER TABLE processing_unit RENAME date TO pu_date;
				RAISE INFO 'Column `processing_unit.date` successfully renamed';
			END IF;

			IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name='processing_unit' AND column_name='pu_date'
			           AND column_default IS NULL) 
	        THEN
    	    	ALTER TABLE processing_unit ALTER COLUMN pu_date SET DEFAULT current_date;
				RAISE INFO 'Default of `processing_unit.date` successfully set to current_date';
			END IF;

			IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name='processing_unit' AND column_name='time'
			           AND column_default IS NULL) 
	        THEN
    	    	ALTER TABLE processing_unit ALTER COLUMN "time" SET DEFAULT current_time;
				RAISE INFO 'Default of `processing_unit.time` successfully set to current_time';
			END IF;

			
			IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name='processing_unit' AND data_type='boolean') THEN
				ALTER TABLE processing_unit 
					ALTER COLUMN test TYPE char USING 
						CASE 
							WHEN test IS NOT NULL 
							THEN 
								CASE 
							 		WHEN test 
							 		THEN 't' 
							 		ELSE 'f' 
					    		END 
							ELSE NULL 
						END,
					
					ALTER COLUMN cancel TYPE char USING 
						CASE 
							WHEN cancel IS NOT NULL 
							THEN 
								CASE 
									WHEN cancel 
									THEN 't' 
									ELSE 'f' 
								END
							ELSE NULL 
						END;
						
				RAISE INFO 'Boolean columns in `processing_unit` successfully changed to char';
			END IF;
			RAISE INFO 'Table `processing_unit` its columns are verified and possibly updated.';
		EXCEPTION WHEN OTHERS THEN
			RAISE INFO 'Error occurred while altering table `processing_unit`';
			RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	EXECUTE 'ALTER TABLE public.processing_unit OWNER TO ' || owner_user;

	--
	-- Name: product; Type: TABLE; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM pg_tables where tablename = 'product') THEN
		BEGIN
			CREATE TABLE product (
				id integer DEFAULT nextval(('product_id_seq'::text)::regclass) NOT NULL,
				name character varying,
				screenname character varying,
				date_created timestamp default now() NOT NULL,
				date_modified timestamp
			);
			RAISE INFO 'Table `product` successfully created';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating table `product`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	ELSE
		BEGIN
            IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='product' AND column_name='date_created') THEN
				ALTER TABLE product ADD COLUMN date_created timestamp;
				UPDATE product SET date_created = ('01-01-2000');
				ALTER TABLE product ALTER COLUMN date_created SET default now();
				ALTER TABLE product ALTER COLUMN date_created SET NOT NULL;
				RAISE INFO 'Column `product.date_created` successfully added';
			END IF;
			IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='product' AND column_name='date_modified') THEN
				ALTER TABLE product ADD COLUMN date_modified timestamp;
				RAISE INFO 'Column `product.date_modified` successfully added';
			END IF;
			IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name='product' AND column_name='model') THEN
				ALTER TABLE product drop COLUMN model;
				RAISE INFO 'Column `product.model` successfully removed';
			END IF;
			IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name='product' AND column_name='authentication_type') THEN
				ALTER TABLE product drop COLUMN authentication_type;
				RAISE INFO 'Column `authentication_type.model` successfully removed';
			END IF;
			RAISE INFO 'Table `product` its columns are verified and possibly updated.';
		EXCEPTION WHEN OTHERS THEN
			RAISE INFO 'Error occurred while altering table `product`';
			RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	EXECUTE 'ALTER TABLE public.product OWNER TO ' || owner_user;

	--
	-- Name: product_id_seq; Type: SEQUENCE; Schema: public; Owner:
	--

	IF NOT EXISTS (SELECT * FROM pg_statio_user_sequences where relname = 'product_id_seq') THEN
		BEGIN
			CREATE SEQUENCE product_id_seq
				START WITH 1
				INCREMENT BY 1
				NO MAXVALUE
				NO MINVALUE
				CACHE 1;
			RAISE INFO 'Sequence `product_id_seq` successfully created';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating sequence `product_id_seq`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	EXECUTE 'ALTER TABLE public.product_id_seq OWNER TO ' || owner_user;

	--
	-- populate the product table
	--
        BEGIN
	        IF NOT EXISTS(select *  from product where name='bullhorn') THEN
                insert into product (name, screenname) values ('bullhorn','Bullhorn');
            END IF;
	        IF NOT EXISTS(select *  from product where name='foederis') THEN
                insert into product (name, screenname) values ('foederis','Foederis');
            END IF;            
            IF NOT EXISTS(select *  from product where name='carerixnew') THEN
                insert into product (name, screenname) values ('carerixnew','Carerix');
            END IF;
            IF NOT EXISTS(select *  from product where name='connexys') THEN
                insert into product (name, screenname) values ('connexys','Connexys');
            END IF;
            IF NOT EXISTS(select *  from product where name='demo') THEN
                insert into product (name, screenname) values ('demo','Demo');
            END IF;
            IF NOT EXISTS(select *  from product where name='getprofile') THEN
                insert into product (name, screenname) values ('getprofile','Get Profile');
            END IF;
            IF NOT EXISTS(select *  from product where name='icams') THEN
                insert into product (name, screenname) values ('icams','iCams');
            END IF;
            IF NOT EXISTS(select *  from product where name='max') THEN
                insert into product (name, screenname) values ('max','MAX');
            END IF;
            IF NOT EXISTS(select *  from product where name='maxstore') THEN
                insert into product (name, screenname) values ('maxstore','MAXStore');
            END IF;
            IF NOT EXISTS(select *  from product where name='multipartxml') THEN
                insert into product (name, screenname) values ('multipartxml','YER');
            END IF;
            IF NOT EXISTS(select *  from product where name='novesta') THEN
                insert into product (name, screenname) values ('novesta','Embedded');
            END IF;
            IF NOT EXISTS(select *  from product where name='profile2000') THEN
                insert into product (name, screenname) values ('profile2000','Profile 2000');
            END IF;
            IF NOT EXISTS(select *  from product where name='search') THEN
                insert into product (name, screenname) values ('search','Search!');
            END IF;
            IF NOT EXISTS(select *  from product where name='sftp') THEN
                insert into product (name, screenname) values ('sftp','SFTP');
            END IF;
            IF NOT EXISTS(select *  from product where name='simplexml') THEN
                insert into product (name, screenname) values ('simplexml','Simple XML Post');
            END IF;
            IF NOT EXISTS(select *  from product where name='soap') THEN
                insert into product (name, screenname) values ('soap','Soap');
            END IF;
			IF NOT EXISTS(select *  from product where name='taleo') THEN
                insert into product (name, screenname) values ('taleo','Taleo');
            END IF;
            IF NOT EXISTS(select *  from product where name='trxmlid') THEN
                insert into product (name, screenname) values ('trxmlid','Trxmlid');
            END IF;
            IF NOT EXISTS(select *  from product where name='vitaeflex') THEN
                insert into product (name, screenname) values ('vitaeflex','vitaeflex');
            END IF;
            IF NOT EXISTS(select *  from product where name='peakitsf') THEN
                insert into product (name, screenname) values ('peakitsf','PeakIT SF');
            END IF;
            IF NOT EXISTS(select *  from product where name='postprofile') THEN
                insert into product (name, screenname) values ('postprofile','Post Profile Redirect');
            END IF;
            IF NOT EXISTS(select *  from product where name='umantis') THEN
                insert into product (name, screenname) values ('umantis','Umantis');
            END IF;
            IF NOT EXISTS(select *  from product where name='rest') THEN
                insert into product (name, screenname) values ('rest', 'REST');
            END IF;
            IF NOT EXISTS(select *  from product where name='formpost') THEN
                insert into product (name, screenname) values ('formpost', 'Post Form');
            END IF;
            IF NOT EXISTS(select *  from product where name='multipartformpost') THEN
                insert into product (name, screenname) values ('multipartformpost', 'Post Multi-Part Form');
            END IF;

			/*
			 * NOT ADDED BECAUSE CUSTOMER DOESN'T WANT TO BE EXPOSED IN PRODUCT DROPDOWN   
			 * 
            IF NOT EXISTS(select *  from product where name='tangram') THEN
                insert into product (name, screenname) values ('tangram','Tangram');
            END IF;
			*/
            RAISE INFO 'List of products successfully updated.';
            EXCEPTION WHEN OTHERS THEN
                RAISE INFO 'Error occurred while altering table `products`';
                RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;

        END;

	--
	-- Name: role; Type: TABLE; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM pg_tables where tablename = 'role') THEN
		BEGIN
			CREATE TABLE "role" (
				id integer DEFAULT nextval(('role_id_seq'::text)::regclass) NOT NULL,
				rolename character varying,
				userid integer,
				date_created timestamp default now() NOT NULL,
				date_modified timestamp
			);
			RAISE INFO 'Table `role` successfully created';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating table `role`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	ELSE
		BEGIN
            IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='role' AND column_name='date_created') THEN
				ALTER TABLE role ADD COLUMN date_created timestamp;
				UPDATE role SET date_created = ('01-01-2000');
				ALTER TABLE role ALTER COLUMN date_created SET default now();
				ALTER TABLE role ALTER COLUMN date_created SET NOT NULL;
				RAISE INFO 'Column `role.date_created` successfully added';
			END IF;
			IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='role' AND column_name='date_modified') THEN
				ALTER TABLE role ADD COLUMN date_modified timestamp;
				RAISE INFO 'Column `role.date_modified` successfully added';
			END IF;
			RAISE INFO 'Table `role` its columns are verified and possibly updated.';
		EXCEPTION WHEN OTHERS THEN
			RAISE INFO 'Error occurred while altering table `role`';
			RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	EXECUTE 'ALTER TABLE public."role" OWNER TO ' || owner_user;

	--
	-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner:
	--

	IF NOT EXISTS (SELECT * FROM pg_statio_user_sequences where relname = 'role_id_seq') THEN
		BEGIN
			CREATE SEQUENCE role_id_seq
				INCREMENT BY 1
				NO MAXVALUE
				NO MINVALUE
				CACHE 1;
			RAISE INFO 'Sequence `role_id_seq` successfully created';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating sequence `role_id_seq`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	EXECUTE 'ALTER TABLE public.role_id_seq OWNER TO ' || owner_user;


	--
	-- Name: routing; Type: TABLE; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM pg_tables where tablename = 'routing') THEN
		BEGIN
			CREATE TABLE routing (
				id integer DEFAULT nextval(('routing_id_seq'::text)::regclass) NOT NULL,
				task character varying NOT NULL,
				address character varying NOT NULL,
				"type" character varying,
				target character varying,
				client integer NOT NULL,
				lang character varying NOT NULL,
				enabled character(1) NOT NULL,
				is_default character(1) NOT NULL,
				extra_textractor_parameters character varying,
	            step integer DEFAULT 1 NOT NULL,
				date_created timestamp default now() NOT NULL,
				date_modified timestamp,
				code_tables character(1) NOT NULL default 'f'
			);
			RAISE INFO 'Table `routing` successfully created';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating table `routing`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	ELSE
		BEGIN
            IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='routing' AND column_name='date_created') THEN
				ALTER TABLE routing ADD COLUMN date_created timestamp;
				UPDATE routing SET date_created = ('01-01-2000');
				ALTER TABLE routing ALTER COLUMN date_created SET default now();
				ALTER TABLE routing ALTER COLUMN date_created SET NOT NULL;
				RAISE INFO 'Column `routing.date_created` successfully added';
			END IF;
			IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='routing' AND column_name='date_modified') THEN
				ALTER TABLE routing ADD COLUMN date_modified timestamp;
				RAISE INFO 'Column `routing.date_modified` successfully added';
			END IF;
			IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='routing' AND column_name='extra_textractor_parameters') THEN
				ALTER TABLE routing ADD COLUMN extra_textractor_parameters character varying;
				RAISE INFO 'Column `routing.extra_textractor_parameters` successfully added';
			END IF;
			IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='routing' AND column_name='step') THEN
				ALTER TABLE routing ADD COLUMN step integer DEFAULT 1 NOT NULL;
				RAISE INFO 'Column `routing.extra_textractor_parameters` successfully added';
			END IF;
			IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='routing' AND column_name='code_tables') THEN
				ALTER TABLE routing ADD COLUMN code_tables character(1);
				UPDATE routing set code_tables = 't' WHERE task = 'normalization' AND is_default = 't';
				UPDATE routing set code_tables = 'f' WHERE code_tables IS NULL;
				ALTER TABLE routing ALTER COLUMN code_tables SET NOT NULL;
				RAISE INFO 'Column `routing.code_tables` successfully added';
			END IF;
			RAISE INFO 'Table `routing` its columns are verified and possibly updated.';
		EXCEPTION WHEN OTHERS THEN
			RAISE INFO 'Error occurred while altering table `routing`';
			RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	EXECUTE 'ALTER TABLE public.routing OWNER TO ' || owner_user;

	--
	-- Name: routing_id_seq; Type: SEQUENCE; Schema: public; Owner:
	--

	IF NOT EXISTS (SELECT * FROM pg_statio_user_sequences where relname = 'routing_id_seq') THEN
		BEGIN
			CREATE SEQUENCE routing_id_seq
				INCREMENT BY 1
				NO MAXVALUE
				NO MINVALUE
				CACHE 1;
			RAISE INFO 'Sequence `routing_id_seq` successfully created';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating sequence `routing_id_seq`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	EXECUTE 'ALTER TABLE public.routing_id_seq OWNER TO ' || owner_user;

	SET default_with_oids = false;

	--
	-- Name: status; Type: TABLE; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM pg_tables where tablename = 'status') THEN
		BEGIN
			CREATE TABLE status (
				name character varying NOT NULL,
				status character varying,
				start_date timestamp without time zone,
				last_email_date timestamp without time zone
			);
			RAISE INFO 'Table `status` successfully created';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating table `status`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	EXECUTE 'ALTER TABLE public.status OWNER TO ' || owner_user;

	SET default_with_oids = true;

	--
	-- Name: trxml_attachment; Type: TABLE; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM pg_tables where tablename = 'trxml_attachment') THEN
		BEGIN
			CREATE TABLE trxml_attachment (
				id integer DEFAULT nextval(('trxml_attachment_seq'::text)::regclass) NOT NULL,
				trxml_object integer NOT NULL,
				attachmentfilename character varying NOT NULL,
				attachmentfiledisplayname character varying NOT NULL,
				attachmentcontenttype character varying NOT NULL,
				date_created date DEFAULT current_date NOT NULL,
				date_modified date,
				attachmentlanguage character varying,
				attachmentcharset character varying
			);
			RAISE INFO 'Table `trxml_attachment` successfully created';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating table `trxml_attachment`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	ELSE
		BEGIN
			-- trxml_attachment was using a non-existing sequence trxml_object_attachment, lets fix it
			IF EXISTS(SELECT * from information_schema.columns where table_name='trxml_attachment' and column_name='id' and column_default ~ 'trxml_object_attachment') THEN
				ALTER TABLE ONLY trxml_attachment ALTER COLUMN id SET DEFAULT nextval(('trxml_attachment_seq'::text)::regclass);
				RAISE INFO 'Sequence in `trxml_attachment` successfully replaced with `trxml_attachment_seq`';			
			END IF;			
		END;
	END IF;


	EXECUTE 'ALTER TABLE public.trxml_attachment OWNER TO ' || owner_user;

	--
	-- Name: trxml_attachment_seq; Type: SEQUENCE; Schema: public; Owner:
	--

	IF NOT EXISTS (SELECT * FROM pg_statio_user_sequences where relname = 'trxml_attachment_seq') THEN
		BEGIN
			CREATE SEQUENCE trxml_attachment_seq
				INCREMENT BY 1
				NO MAXVALUE
				NO MINVALUE
				CACHE 1;
			RAISE INFO 'Sequence `trxml_attachment_seq` successfully created';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating sequence `trxml_attachment_seq`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	EXECUTE 'ALTER TABLE public.trxml_attachment_seq OWNER TO ' || owner_user;

	--
	-- Name: trxml_object; Type: TABLE; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM pg_tables where tablename = 'trxml_object') THEN
		BEGIN
			CREATE TABLE trxml_object (
				id integer DEFAULT nextval(('trxml_object_seq'::text)::regclass) NOT NULL,
				userid integer NOT NULL,
				client integer NOT NULL,
				document_name character varying,
				original_document text,
				trxml text,
				insert_state integer NOT NULL,
				division character varying,
				sent_date timestamp without time zone,
				received_date timestamp without time zone,
				ext_candidate_id character varying,
				oportunity_id character varying,
				candidate_name character varying,
				candidate_email character varying,
				candidate_birthdate date,
				candidate_first_name character varying,
				candidate_last_name character varying,
				candidate_middle_name character varying,
				candidate_initials character varying,
				candidate_photo character varying,
				candidate_city character varying,
				date_created timestamp without time zone DEFAULT now() NOT NULL,
				date_modified timestamp without time zone,
				store_file character varying,
				store_path character varying,
				content_type character varying,
				dedup_style character varying,
				editing character varying,
				locked character varying,
				attachment_id integer,
				email_sent character(6),
				distance integer,
				years_of_experience integer,
				editing_user integer,
				edited_date timestamp without time zone,
				custom_tstamp_1 timestamp without time zone,
				custom_int_1 integer,
				custom_string_1 character varying,
				custom_string_2 character varying,
				custom_string_3 character varying,
				custom_string_4 character varying,
				custom_string_5 character varying,
				custom_string_6 character varying,
				custom_string_7 character varying,
				custom_string_8 character varying,
				custom_string_9 character varying,
				custom_int_2 integer,
				custom_int_3 integer,
				custom_int_4 integer,
				custom_tstamp_2 timestamp without time zone,
				touch_count integer,
				custom_boolean_1 char,
				custom_boolean_2 char,
				custom_boolean_3 char,
				custom_boolean_4 char,
				custom_boolean_5 char,
				custom_boolean_6 char
			);
			RAISE INFO 'Table `trxml_object` successfully created';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating table `trxml_object`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	ELSE
		BEGIN
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='trxml_object' AND column_name='custom_boolean_1') THEN
				ALTER TABLE trxml_object ADD COLUMN custom_boolean_1 char;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='trxml_object' AND column_name='custom_boolean_2') THEN
				ALTER TABLE trxml_object ADD COLUMN custom_boolean_2 char;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='trxml_object' AND column_name='custom_boolean_3') THEN
				ALTER TABLE trxml_object ADD COLUMN custom_boolean_3 char;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='trxml_object' AND column_name='custom_boolean_4') THEN
				ALTER TABLE trxml_object ADD COLUMN custom_boolean_4 char;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='trxml_object' AND column_name='custom_boolean_5') THEN
				ALTER TABLE trxml_object ADD COLUMN custom_boolean_5 char;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='trxml_object' AND column_name='custom_boolean_6') THEN
				ALTER TABLE trxml_object ADD COLUMN custom_boolean_6 char;
			END IF;
			-- Adding new columns for candidate information for feature "extended trxml info" in 3.0.30:
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='trxml_object' AND column_name='candidate_first_name') THEN
				ALTER TABLE trxml_object ADD COLUMN candidate_first_name character varying;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='trxml_object' AND column_name='candidate_last_name') THEN
				ALTER TABLE trxml_object ADD COLUMN candidate_last_name character varying;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='trxml_object' AND column_name='candidate_middle_name') THEN
				ALTER TABLE trxml_object ADD COLUMN candidate_middle_name character varying;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='trxml_object' AND column_name='candidate_initials') THEN
				ALTER TABLE trxml_object ADD COLUMN candidate_initials character varying;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='trxml_object' AND column_name='candidate_city') THEN
				ALTER TABLE trxml_object ADD COLUMN candidate_city character varying;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='trxml_object' AND column_name='candidate_photo') THEN
				ALTER TABLE trxml_object ADD COLUMN candidate_photo character varying;
			END IF;
		
			---
			--- Making sure unwanted columns are deleted from existing databases
			IF EXISTS(SELECT * FROM information_schema.columns WHERE table_name='trxml_object' AND column_name='batch_id') THEN
				ALTER TABLE trxml_object DROP COLUMN batch_id;
			END IF;	

	               IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name='trxml_object' AND data_type='boolean') THEN
				ALTER TABLE trxml_object 
					ALTER COLUMN custom_boolean_1 TYPE char USING CASE WHEN custom_boolean_1 IS NOT NULL THEN CASE WHEN custom_boolean_1 THEN 't' ELSE 'f' END ELSE NULL END,
					ALTER COLUMN custom_boolean_2 TYPE char USING CASE WHEN custom_boolean_2 IS NOT NULL THEN CASE WHEN custom_boolean_3 THEN 't' ELSE 'f' END ELSE NULL END,
					ALTER COLUMN custom_boolean_3 TYPE char USING CASE WHEN custom_boolean_3 IS NOT NULL THEN CASE WHEN custom_boolean_3 THEN 't' ELSE 'f' END ELSE NULL END,
					ALTER COLUMN custom_boolean_4 TYPE char USING CASE WHEN custom_boolean_4 IS NOT NULL THEN CASE WHEN custom_boolean_4 THEN 't' ELSE 'f' END ELSE NULL END,
					ALTER COLUMN custom_boolean_5 TYPE char USING CASE WHEN custom_boolean_5 IS NOT NULL THEN CASE WHEN custom_boolean_5 THEN 't' ELSE 'f' END ELSE NULL END,
					ALTER COLUMN custom_boolean_6 TYPE char USING CASE WHEN custom_boolean_6 IS NOT NULL THEN CASE WHEN custom_boolean_6 THEN 't' ELSE 'f' END ELSE NULL END;
				RAISE INFO 'Boolean columns in `trxml_object` successfully changed to char';
			END IF;

			RAISE INFO 'Table `trxml_object` its columns are verified and possibly updated.';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while altering table `trxml_object`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;

		END;
	END IF;


	
	IF NOT EXISTS (SELECT tableowner FROM pg_tables where tablename = 'trxml_object' 
		and tableowner = owner_user) THEN
		BEGIN
			EXECUTE 'ALTER TABLE public.trxml_object OWNER TO ' || owner_user;
			RAISE INFO 'Owner of table  public.trxml_object successfully changed to %', owner_user;
		END;
	END IF;
	

	
	
	--
	-- Name: trxml_object_seq; Type: SEQUENCE; Schema: public; Owner:
	--
	IF NOT EXISTS (SELECT * FROM pg_statio_user_sequences where relname = 'trxml_object_seq') THEN
		BEGIN
			CREATE SEQUENCE trxml_object_seq
				INCREMENT BY 1
				NO MAXVALUE
				NO MINVALUE
				CACHE 1;
			RAISE INFO 'Sequence `trxml_object_seq` successfully created';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating sequence `trxml_object_seq`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	EXECUTE 'ALTER TABLE public.trxml_object_seq OWNER TO ' || owner_user;

	--
	-- Name: users; Type: TABLE; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM pg_tables where tablename = 'users') THEN
		BEGIN
			CREATE TABLE users (
				id integer DEFAULT nextval(('users_id_seq'::text)::regclass) PRIMARY KEY,
				client integer,
				enabled char DEFAULT 't',
				valid_until date,
				name character varying,
				"password" character varying,
				email character varying,
				test char DEFAULT 'f',
				fullname character varying,
				branch character varying,
				custom_int_1 integer,
				custom_int_2 integer,
				custom_int_3 integer,
				custom_int_4 integer,
				custom_tstamp_1 timestamp without time zone,
				custom_tstamp_2 timestamp without time zone,
				custom_string_1 character varying,
				custom_string_2 character varying,
				custom_string_3 character varying,
				custom_string_4 character varying,
				custom_string_5 character varying,
				custom_string_6 character varying,
				custom_string_7 character varying,
				custom_string_8 character varying,
				custom_string_9 character varying,
				custom_string_10 character varying,
				custom_string_11 character varying,
				custom_string_12 character varying,
				custom_string_13 character varying,
				custom_string_14 character varying,
				custom_string_15 character varying,
				custom_string_16 character varying,
				custom_string_17 character varying,
				custom_string_18 character varying,
				custom_string_19 character varying,
				custom_string_20 character varying,
				custom_boolean_1 char,
				custom_boolean_2 char,
				custom_boolean_3 char,
				custom_boolean_4 char,
				custom_boolean_5 char,
				custom_boolean_6 char,
				date_created timestamp default now() NOT NULL,
				date_modified timestamp,
				monster_cat character varying,
				external_user_id character varying,
				auto_created char DEFAULT 'f',
				custom_product character varying,
				custom_target character varying,
				custom_search_roles_ro character varying,
				document_sharing_ro character varying,
				widget_token character varying,
				widget_domain character varying
				);
			RAISE INFO 'Table `users` successfully created';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating table `users`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	ELSE
		BEGIN
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_int_1') THEN
				ALTER TABLE users ADD COLUMN custom_int_1 integer;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_int_2') THEN
				ALTER TABLE users ADD COLUMN custom_int_2 integer;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_int_3') THEN
				ALTER TABLE users ADD COLUMN custom_int_3 integer;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_int_4') THEN
				ALTER TABLE users ADD COLUMN custom_int_4 integer;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_tstamp_1') THEN
				ALTER TABLE users ADD COLUMN custom_tstamp_1 timestamp without time zone;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_tstamp_2') THEN
				ALTER TABLE users ADD COLUMN custom_tstamp_2 timestamp without time zone;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_string_1') THEN
				ALTER TABLE users ADD COLUMN custom_string_1 character varying;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_string_2') THEN
				ALTER TABLE users ADD COLUMN custom_string_2 character varying;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_string_3') THEN
				ALTER TABLE users ADD COLUMN custom_string_3 character varying;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_string_4') THEN
				ALTER TABLE users ADD COLUMN custom_string_4 character varying;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_string_5') THEN
				ALTER TABLE users ADD COLUMN custom_string_5 character varying;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_string_6') THEN
				ALTER TABLE users ADD COLUMN custom_string_6 character varying;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_string_7') THEN
				ALTER TABLE users ADD COLUMN custom_string_7 character varying;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_string_8') THEN
				ALTER TABLE users ADD COLUMN custom_string_8 character varying;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_string_9') THEN
				ALTER TABLE users ADD COLUMN custom_string_9 character varying;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_boolean_1') THEN
				ALTER TABLE users ADD COLUMN custom_boolean_1 char;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_boolean_2') THEN
				ALTER TABLE users ADD COLUMN custom_boolean_2 char;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_boolean_3') THEN
				ALTER TABLE users ADD COLUMN custom_boolean_3 char;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_boolean_4') THEN
				ALTER TABLE users ADD COLUMN custom_boolean_4 char;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_boolean_5') THEN
				ALTER TABLE users ADD COLUMN custom_boolean_5 char;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_boolean_6') THEN
				ALTER TABLE users ADD COLUMN custom_boolean_6 char;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='external_user_id') THEN
				ALTER TABLE users ADD COLUMN external_user_id character varying;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='auto_created') THEN
				ALTER TABLE users ADD COLUMN auto_created character varying;
				RAISE INFO 'Column `users.auto_created` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_product') THEN
				ALTER TABLE users ADD COLUMN custom_product character varying;
				RAISE INFO 'Column `users.custom_product` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_target') THEN
				ALTER TABLE users ADD COLUMN custom_target character varying;
				RAISE INFO 'Column `users.custom_target` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='document_sharing_ro') THEN
				ALTER TABLE users ADD COLUMN document_sharing_ro character varying;
				RAISE INFO 'Column `users.document_sharing_ro` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='widget_token') THEN
				ALTER TABLE users ADD COLUMN widget_token character varying;
				RAISE INFO 'Column `users.widget_token` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='widget_domain') THEN
				ALTER TABLE users ADD COLUMN widget_domain character varying;
				RAISE INFO 'Column `users.widget_domain` successfully added';
			END IF;

			---
			--- Make sure unwated/not-needed columns are deleted
			---
			IF EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='current_batch') THEN
				ALTER TABLE users DROP COLUMN current_batch;
			END IF;

            		IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='date_created') THEN
				ALTER TABLE users ADD COLUMN date_created timestamp;
				UPDATE users SET date_created = ('01-01-2000');
				ALTER TABLE users ALTER COLUMN date_created SET default now();
				ALTER TABLE users ALTER COLUMN date_created SET NOT NULL;
				RAISE INFO 'Column `users.date_created` successfully added';
			END IF;
			IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='date_modified') THEN
				ALTER TABLE users ADD COLUMN date_modified timestamp;
				RAISE INFO 'Column `users.date_modified` successfully added';
			END IF;
            		IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name='users' AND data_type='boolean') THEN
				ALTER TABLE users
					ALTER COLUMN enabled DROP DEFAULT,
					ALTER COLUMN test DROP DEFAULT;
				ALTER TABLE users 
					ALTER COLUMN enabled TYPE char USING CASE WHEN enabled THEN 't' ELSE 'f' END,
					ALTER COLUMN test TYPE char USING CASE WHEN test THEN 't' ELSE 'f' END,
					ALTER COLUMN custom_boolean_1 TYPE char USING CASE WHEN custom_boolean_1 IS NOT NULL THEN CASE WHEN custom_boolean_1 THEN 't' ELSE 'f' END ELSE NULL END,
					ALTER COLUMN custom_boolean_2 TYPE char USING CASE WHEN custom_boolean_2 IS NOT NULL THEN CASE WHEN custom_boolean_2 THEN 't' ELSE 'f' END ELSE NULL END,
					ALTER COLUMN custom_boolean_3 TYPE char USING CASE WHEN custom_boolean_3 IS NOT NULL THEN CASE WHEN custom_boolean_3 THEN 't' ELSE 'f' END ELSE NULL END,
					ALTER COLUMN custom_boolean_4 TYPE char USING CASE WHEN custom_boolean_4 IS NOT NULL THEN CASE WHEN custom_boolean_4 THEN 't' ELSE 'f' END ELSE NULL END,
					ALTER COLUMN custom_boolean_5 TYPE char USING CASE WHEN custom_boolean_5 IS NOT NULL THEN CASE WHEN custom_boolean_5 THEN 't' ELSE 'f' END ELSE NULL END,
					ALTER COLUMN custom_boolean_6 TYPE char USING CASE WHEN custom_boolean_6 IS NOT NULL THEN CASE WHEN custom_boolean_6 THEN 't' ELSE 'f' END ELSE NULL END;
				ALTER TABLE users 
					ALTER COLUMN enabled SET DEFAULT 't',
					ALTER COLUMN test SET DEFAULT 'f';
				RAISE INFO 'Boolean columns in `users` successfully changed to char';
			END IF;
			
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='monster_cat') THEN
				ALTER TABLE users ADD COLUMN monster_cat character varying;
				RAISE INFO 'Column `users.monster_cat` successfully added';
			END IF;

			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_string_10') THEN
				ALTER TABLE users ADD COLUMN custom_string_10 character varying;
				RAISE INFO 'Column `users.custom_string_10` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_string_11') THEN
				ALTER TABLE users ADD COLUMN custom_string_11 character varying;
				RAISE INFO 'Column `users.custom_string_11` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_string_12') THEN
				ALTER TABLE users ADD COLUMN custom_string_12 character varying;
				RAISE INFO 'Column `users.custom_string_12` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_string_13') THEN
				ALTER TABLE users ADD COLUMN custom_string_13 character varying;
				RAISE INFO 'Column `users.custom_string_13` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_string_14') THEN
				ALTER TABLE users ADD COLUMN custom_string_14 character varying;
				RAISE INFO 'Column `users.custom_string_14` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_string_15') THEN
				ALTER TABLE users ADD COLUMN custom_string_15 character varying;
				RAISE INFO 'Column `users.custom_string_15` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_string_16') THEN
				ALTER TABLE users ADD COLUMN custom_string_16 character varying;
				RAISE INFO 'Column `users.custom_string_16` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_string_17') THEN
				ALTER TABLE users ADD COLUMN custom_string_17 character varying;
				RAISE INFO 'Column `users.custom_string_17` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_string_18') THEN
				ALTER TABLE users ADD COLUMN custom_string_18 character varying;
				RAISE INFO 'Column `users.custom_string_18` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_string_19') THEN
				ALTER TABLE users ADD COLUMN custom_string_19 character varying;
				RAISE INFO 'Column `users.custom_string_19` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_string_20') THEN
				ALTER TABLE users ADD COLUMN custom_string_20 character varying;
				RAISE INFO 'Column `users.custom_string_20` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='users' AND column_name='custom_search_roles_ro') THEN
				ALTER TABLE users ADD COLUMN custom_search_roles_ro character varying;
				RAISE INFO 'Column `users.custom_search_roles_ro` successfully added';
			END IF;

			
			RAISE INFO 'Table `users` its columns are verified and possibly updated.';
		EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while altering table `users`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;

		END;
	END IF;


	EXECUTE 'ALTER TABLE public.users OWNER TO ' || owner_user;

	--
	-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner:
	--

	IF NOT EXISTS (SELECT * FROM pg_statio_user_sequences where relname = 'users_id_seq') THEN
		BEGIN
			CREATE SEQUENCE users_id_seq
				INCREMENT BY 1
				NO MAXVALUE
				NO MINVALUE
				CACHE 1;
			RAISE INFO 'Sequence `users_id_seq` successfully created';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating sequence `users_id_seq`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	EXECUTE 'ALTER TABLE public.users_id_seq OWNER TO ' || owner_user;


	--
	-- Table mailbox holds information on mailboxes that can be read by the TKMRA
	-- Name: mailbox; Type: TABLE; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM pg_tables where tablename = 'mailbox') THEN
		BEGIN
			CREATE TABLE mailbox (
    			id integer DEFAULT nextval('mailbox_seq'::text) PRIMARY KEY,
				name character varying,  -- tevens dripad
				enabled char,
				keepMailOnServer char default 'f',
				skipDeduplication char default 'f',
				skipStore char default 't',
				doValidation char default 'f',
				server character varying,
				password character varying,
				protocol character varying,
				account character varying,
				pop3port integer not null default 110, 
				email_address character varying, 
				divisionname character varying,
				secretarymail character varying,
				sourceBoxUser integer NOT NULL references users(id),
				acceptedSenderList character varying,
				archiveMailbox character varying,
				-- diread
				work_directory_path character varying,
   				output_directory_path character varying,
				sweeping char,
    			mbx_comment character varying,
				last_swept timestamp,
    			txtoradress character varying,
    			dir_tstamp timestamp,
    			mbx_mode integer not null default 1,
    			-- mb status
    			status integer default 0,
    			status_flags character varying,
				status_tstamp timestamp default now(),
				-- reset
				reset char not null default 'f',
				date_created timestamp default now() NOT NULL,
				date_modified timestamp
			);
			RAISE INFO 'Table `mailbox` successfully created';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating table `mailbox`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	ELSE
		BEGIN
			IF EXISTS(SELECT * FROM information_schema.columns WHERE table_name='mailbox' AND column_name='sourceboxaccount') THEN
				ALTER TABLE mailbox DROP COLUMN sourceBoxAccount;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='mailbox' AND column_name='status') THEN
				ALTER TABLE mailbox  ADD COLUMN status integer not null default 0;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='mailbox' AND column_name='status_tstamp') THEN
				ALTER TABLE mailbox  ADD COLUMN status_tstamp timestamp not null default now();
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='mailbox' AND column_name='status_flags') THEN
				ALTER TABLE mailbox  ADD COLUMN status_flags character varying;
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='mailbox' AND column_name='reset') THEN
				ALTER TABLE mailbox  ADD COLUMN reset char not null default 'f';
			END IF;
            IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='mailbox' AND column_name='date_created') THEN
				ALTER TABLE mailbox ADD COLUMN date_created timestamp;
				UPDATE mailbox SET date_created = ('01-01-2000');
				ALTER TABLE mailbox ALTER COLUMN date_created SET default now();
				ALTER TABLE mailbox ALTER COLUMN date_created SET NOT NULL;
				RAISE INFO 'Column `mailbox.date_created` successfully added';
			END IF;
			IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='mailbox' AND column_name='date_modified') THEN
				ALTER TABLE mailbox ADD COLUMN date_modified timestamp;
				RAISE INFO 'Column `mailbox.date_modified` successfully added';
			END IF;
			IF EXISTS(SELECT * FROM information_schema.columns WHERE table_name='mailbox' AND column_name='comment') THEN
				ALTER TABLE mailbox	RENAME comment TO mbx_comment;
				RAISE INFO 'Column `mailbox.comment` successfully renamed';
			END IF;
			IF EXISTS(SELECT * FROM information_schema.columns WHERE table_name='mailbox' AND column_name='mode') THEN
				ALTER TABLE mailbox	RENAME mode TO mbx_mode;
				RAISE INFO 'Column `mailbox.mode` successfully renamed';
			END IF;
            IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name='mailbox' AND data_type='boolean') THEN
				ALTER TABLE mailbox
					ALTER COLUMN reset DROP DEFAULT;
				ALTER TABLE mailbox 
					ALTER COLUMN enabled TYPE char USING CASE WHEN enabled IS NOT NULL THEN CASE WHEN enabled THEN 't' ELSE 'f' END ELSE NULL END,
					ALTER COLUMN sweeping TYPE char USING CASE WHEN sweeping IS NOT NULL THEN CASE WHEN sweeping THEN 't' ELSE 'f' END ELSE NULL END,
					ALTER COLUMN reset TYPE char USING CASE WHEN reset THEN 't' ELSE 'f' END;
				ALTER TABLE mailbox 
					ALTER COLUMN reset SET DEFAULT 'f';
				RAISE INFO 'Boolean columns in `mailbox` successfully changed to char';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='mailbox' AND column_name='pop3port') THEN
				ALTER TABLE mailbox ADD COLUMN pop3port integer not null default 110;
				RAISE INFO 'Column `pop3port` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='mailbox' AND column_name='email_address') THEN
				ALTER TABLE mailbox ADD COLUMN email_address character varying;
				RAISE INFO 'Column `email_address` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='mailbox' AND column_name='keepmailonserver') THEN
				ALTER TABLE mailbox ADD COLUMN keepMailOnServer char default 'f';
				RAISE INFO 'Column `keepMailOnServer` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='mailbox' AND column_name='skipstore') THEN
				ALTER TABLE mailbox ADD COLUMN skipStore char default 't';
				RAISE INFO 'Column `skipStore` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='mailbox' AND column_name='dovalidation') THEN
				ALTER TABLE mailbox ADD COLUMN doValidation char default 'f';
				RAISE INFO 'Column `doValidation` successfully added';
			END IF;
			IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='mailbox' AND column_name='skipdeduplication') THEN
				ALTER TABLE mailbox ADD COLUMN skipDeduplication char default 'f';
				RAISE INFO 'Column `skipDeduplication` successfully added';
			END IF;
			RAISE INFO 'Table `mailbox` its columns are verified and possibly updated.';
		EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while altering table `mailbox`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
			
		END;
	END IF;

	--
	-- Name: mailbox_protocol; Type: CONSTRAINT; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM information_schema.table_constraints WHERE table_name='mailbox' AND constraint_name='mailbox_protocol') THEN
		BEGIN
			ALTER TABLE ONLY mailbox
				ADD CONSTRAINT mailbox_protocol CHECK (protocol in('pop3','pop3s'));
			RAISE INFO 'Constraint `mailbox_protocol` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `mailbox_protocol`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;




	EXECUTE 'ALTER TABLE public.mailbox OWNER TO ' || owner_user;

	--
	-- Name: trxml_attachment_seq; Type: SEQUENCE; Schema: public; Owner:
	--

	IF NOT EXISTS (SELECT * FROM pg_statio_user_sequences where relname = 'mailbox_seq') THEN
		BEGIN
			CREATE SEQUENCE mailbox_seq
				INCREMENT BY 1
				NO MAXVALUE
				NO MINVALUE
				CACHE 1;
			RAISE INFO 'Sequence `mailbox_seq` successfully created';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating sequence `mailbox_seq`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	EXECUTE 'ALTER TABLE public.mailbox_seq OWNER TO ' || owner_user;

    -- add initial 'textkernel' account
    if 0 = (select count(id) from client) then
        begin
            insert into client (
                account,
                enabled,
                interface_lang,
                lang,
                model,
                name,
                persisting,
                product,
                has_custom_save_path,
                savepath,
                search_activated
            )
            values (
                'textkernel',
                't',
                'english',
                'english',
                'TextkernelAdministrator.xml',
                'Textkernel BV',
                't',
                (select id from product where name='getprofile'),
                't',
                '/tmp/textkernel',
                'f'
            );
            RAISE INFO 'Added `textkernel` account';
        exception
        when others then
			RAISE INFO 'Could not create initial `textkernel` account';
			RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
            
        end;
    end if;

	-- add initial 'admin' user for the 'textkernel' account
    if 0 = (select count(id) from users) then
        begin
            insert into users (
                    client,
                    email,
                    enabled,
                    fullname,
                    name,
                    password,
                    test
            )
            (select id,
                'service@textkernel.nl',
                't',
                'Administrator',
                'admin',
                '21232f297a57a5a743894a0e4a801fc3', -- md5sum of the password 'admin'
                'f'
            from client where account='textkernel');
            RAISE INFO 'Added `admin` user';
        exception
        when others then
            RAISE INFO 'Could not create initial `admin` user';
            RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
        end;
    end if;

    -- IMPORTS table
    IF NOT EXISTS (SELECT * FROM pg_tables where tablename = 'import_info') THEN
		BEGIN
			CREATE TABLE import_info (
    			id integer DEFAULT nextval('import_info_seq'::text) PRIMARY KEY,
				client integer NOT NULL,
    			import_activated char default 'f',
    			import_type character varying,
				import_file character varying,
				import_status character varying DEFAULT 'DONE',
				update_period_start timestamp,
				update_period_end timestamp
			);
			RAISE INFO 'Table `import_info` successfully created';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating table `import_info`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	ELSE
		BEGIN
		-- changes go here
		END;
	END IF;


	IF NOT EXISTS (SELECT * FROM pg_statio_user_sequences where relname = 'import_info_seq') THEN
		BEGIN
			CREATE SEQUENCE import_info_seq
				INCREMENT BY 1
				NO MAXVALUE
				NO MINVALUE
				CACHE 1;
			RAISE INFO 'Sequence `import_info_seq` successfully created';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating sequence `imports_seq`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

    --
    -- give initial 'admin' user the 'admin' role
    --
    if 0 = (select count(id) from role) then
    begin
        insert into role (
            rolename,
            userid
        )
        (select
            'admin',
            id
         from users where name = 'admin');
    RAISE INFO 'Gave `admin` user the `admin` role';
    exception
    when others then
        RAISE INFO 'Could not give user `admin` the `admin` role';
        RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
    end;
    end if;



	--
	-- Name: client_pkey; Type: CONSTRAINT; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE table_name='client' AND constraint_name='client_pkey') THEN
		BEGIN
			ALTER TABLE ONLY client
				ADD CONSTRAINT client_pkey PRIMARY KEY (id);
			RAISE INFO 'Constraint `client_pkey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `client_pkey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;




	--
	-- Name: codelist_pkey; Type: CONSTRAINT; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE table_name='codelist' AND constraint_name='codelist_pkey') THEN
		BEGIN
			ALTER TABLE ONLY codelist
				ADD CONSTRAINT codelist_pkey PRIMARY KEY (id);
			RAISE INFO 'Constraint `codelist_pkey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `codelist_pkey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

    --
    -- Name: codeproperty_pkey; Type: CONSTRAINT; Schema: public; Owner:; Tablespace:
    --

    IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE table_name='codeproperty' AND constraint_name='codeproperty_pkey') THEN
        BEGIN
            ALTER TABLE ONLY codeproperty
                ADD CONSTRAINT codeproperty_pkey PRIMARY KEY (id);
            RAISE INFO 'Constraint `codeproperty_pkey` successfully added';
            EXCEPTION WHEN OTHERS THEN
                RAISE INFO 'Error occurred while adding constraint `codeproperty_pkey`';
                RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
        END;
    END IF;

    --
    -- Name: processing_unit_pkey; Type: CONSTRAINT; Schema: public; Owner:; Tablespace:
    --

    IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE table_name='processing_unit' AND constraint_name='processing_unit_pkey') THEN
        BEGIN
            ALTER TABLE ONLY processing_unit
                ADD CONSTRAINT processing_unit_pkey PRIMARY KEY (id);
            RAISE INFO 'Constraint `processing_unit_pkey` successfully added';
            EXCEPTION WHEN OTHERS THEN
                RAISE INFO 'Error occurred while adding constraint `processing_unit_pkey`';
                RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
        END;
    END IF;


	--
	-- Name: event_document_pkey; Type: CONSTRAINT; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE table_name='event_document' AND constraint_name='event_document_pkey') THEN
		BEGIN
			ALTER TABLE ONLY event_document
				ADD CONSTRAINT event_document_pkey PRIMARY KEY (trxmlid);
			RAISE INFO 'Constraint `event_document_pkey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `event_document_pkey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	--
	-- Name: event_history_pkey; Type: CONSTRAINT; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE table_name='event_history' AND constraint_name='event_history_pkey') THEN
		BEGIN
			ALTER TABLE ONLY event_history
				ADD CONSTRAINT event_history_pkey PRIMARY KEY (id);
			RAISE INFO 'Constraint `event_history_pkey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `event_history_pkey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	--
	-- Name: product_pkey; Type: CONSTRAINT; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE table_name='product' AND constraint_name='product_pkey') THEN
		BEGIN
			ALTER TABLE ONLY product
				ADD CONSTRAINT product_pkey PRIMARY KEY (id);
			RAISE INFO 'Constraint `product_pkey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `product_pkey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	--
	-- Name: role_pkey; Type: CONSTRAINT; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE table_name='role' AND constraint_name='role_pkey') THEN
		BEGIN
			ALTER TABLE ONLY "role"
				ADD CONSTRAINT role_pkey PRIMARY KEY (id);
			RAISE INFO 'Constraint `role_pkey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `role_pkey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	--
	-- Name: routing_pkey; Type: CONSTRAINT; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE table_name='routing' AND constraint_name='routing_pkey') THEN
		BEGIN
			ALTER TABLE ONLY routing
				ADD CONSTRAINT routing_pkey PRIMARY KEY (id);
			RAISE INFO 'Constraint `routing_pkey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `routing_pkey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	--
	-- Name: status_pkey; Type: CONSTRAINT; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE table_name='status' AND constraint_name='status_pkey') THEN
		BEGIN
			ALTER TABLE ONLY status
				ADD CONSTRAINT status_pkey PRIMARY KEY (name);
			RAISE INFO 'Constraint `status_pkey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `status_pkey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	--
	-- Name: trxml_attachment_pkey; Type: CONSTRAINT; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE table_name='trxml_attachment' AND constraint_name='trxml_attachment_pkey') THEN
		BEGIN
			ALTER TABLE ONLY trxml_attachment
				ADD CONSTRAINT trxml_attachment_pkey PRIMARY KEY (id);
			RAISE INFO 'Constraint `trxml_attachment_pkey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `trxml_attachment_pkey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	--
	-- Name: trxml_object_pkey; Type: CONSTRAINT; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE table_name='trxml_object' AND constraint_name='trxml_object_pkey') THEN
		BEGIN
			ALTER TABLE ONLY trxml_object
				ADD CONSTRAINT trxml_object_pkey PRIMARY KEY (id);
			RAISE INFO 'Constraint `trxml_object_pkey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `trxml_object_pkey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	--
	-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE table_name='users' AND constraint_name='users_pkey') THEN
		BEGIN
			ALTER TABLE ONLY users
				ADD CONSTRAINT users_pkey PRIMARY KEY (id);
			RAISE INFO 'Constraint `users_pkey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `users_pkey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

		--
	-- Name: codelist_ac_codeid; Type: INDEX; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM pg_indexes WHERE tablename='codelist' AND indexname='codelist_ac_codeid') THEN
		BEGIN
			CREATE INDEX codelist_ac_codeid ON codelist USING btree (client, fieldname, codeid);
			RAISE INFO 'Index `codelist_ac_codeid` successfully added to table `codelist`';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding index `codelist_ac_codeid` to table `codelist`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	--
	-- Name: codelist_ac_description; Type: INDEX; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM pg_indexes WHERE tablename='codelist' AND indexname='codelist_ac_description') THEN
		BEGIN
			CREATE INDEX codelist_ac_description ON codelist USING btree (client, fieldname, description);
			RAISE INFO 'Index `codelist_ac_description` successfully added to table `codelist`';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding index `codelist_ac_description` to table `codelist`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	--
	-- Name: codelist_codeid; Type: INDEX; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM pg_indexes WHERE tablename='codelist' AND indexname='codelist_codeid') THEN
		BEGIN
			CREATE INDEX codelist_codeid ON codelist USING btree (codeid);
			RAISE INFO 'Index `codelist_codeid` successfully added to table `codelist`';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding index `codelist_codeid` to table `codelist`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	--
	-- Name: codelist_description; Type: INDEX; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM pg_indexes WHERE tablename='codelist' AND indexname='codelist_description') THEN
		BEGIN
			CREATE INDEX codelist_description ON codelist USING btree (description);
			RAISE INFO 'Index `codelist_description` successfully added to table `codelist`';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding index `codelist_description` to table `codelist`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	--
	-- Name: codelist_index; Type: INDEX; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM pg_indexes WHERE tablename='codelist' AND indexname='codelist_index') THEN
		BEGIN
			CREATE INDEX codelist_index ON codelist USING btree (client, fieldname);
			RAISE INFO 'Index `codelist_index` successfully added to table `codelist`';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding index `codelist_index` to table `codelist`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	--
	-- Name: codeproperty_codelistid; Type: INDEX; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM pg_indexes WHERE tablename='codeproperty' AND indexname='codeproperty_codelistid') THEN
		BEGIN
			CREATE INDEX codeproperty_codelistid ON codeproperty USING btree (codelistid);
			RAISE INFO 'Index `codeproperty_codelistid` successfully added to table `codeproperty`';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding index `codeproperty_codelistid` to table `codeproperty`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	--
	-- Name: codeproperty_index; Type: INDEX; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM pg_indexes WHERE tablename='codeproperty' AND indexname='codeproperty_index') THEN
		BEGIN
			CREATE INDEX codeproperty_index ON codeproperty USING btree (propertyname, propertyvalue);
			RAISE INFO 'Index `codeproperty_index` successfully added to table `codeproperty`';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding index `codeproperty_index` to table `codeproperty`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	--
	-- Name: ta_idx_to; Type: INDEX; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM pg_indexes WHERE tablename='trxml_attachment' AND indexname='ta_idx_to') THEN
		BEGIN
			CREATE INDEX ta_idx_to ON trxml_attachment USING btree (trxml_object);
			RAISE INFO 'Index `ta_idx_to` successfully added to table `trxml_attachment`';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding index `ta_idx_to` to table `trxml_attachment`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	
	--
	-- Name: to_idx_client; Type: INDEX; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM pg_indexes WHERE tablename='trxml_object' AND indexname='to_idx_client') THEN
		BEGIN
			CREATE INDEX to_idx_client ON trxml_object USING btree (client);
			RAISE INFO 'Index `to_idx_client` successfully added to table `trxml_object`';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding index `to_idx_client` to table `trxml_object`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	--
	-- Name: to_idx_editing; Type: INDEX; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM pg_indexes WHERE tablename='trxml_object' AND indexname='to_idx_editing') THEN
		BEGIN
			CREATE INDEX to_idx_editing ON trxml_object USING btree (editing);
			RAISE INFO 'Index `to_idx_editing` successfully added to table `trxml_object`';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding index `to_idx_editing` to table `trxml_object`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	--
	-- Name: to_idx_locked; Type: INDEX; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM pg_indexes WHERE tablename='trxml_object' AND indexname='to_idx_locked') THEN
		BEGIN
			CREATE INDEX to_idx_locked ON trxml_object USING btree (locked);
			RAISE INFO 'Index `to_idx_locked` successfully added to table `trxml_object`';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding index `to_idx_locked` to table `trxml_object`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	--
	-- Name: to_idx_moddate; Type: INDEX; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM pg_indexes WHERE tablename='trxml_object' AND indexname='to_idx_moddate') THEN
		BEGIN
			CREATE INDEX to_idx_moddate ON trxml_object USING btree (date_modified);
			RAISE INFO 'Index `to_idx_moddate` successfully added to table `trxml_object`';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding index `to_idx_moddate` to table `trxml_object`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	--
	-- Name: to_idx_received_date; Type: INDEX; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM pg_indexes WHERE tablename='trxml_object' AND indexname='to_idx_received_date') THEN
		BEGIN
			CREATE INDEX to_idx_received_date ON trxml_object USING btree (received_date);
			RAISE INFO 'Index `to_idx_received_date` successfully added to table `trxml_object`';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding index `to_idx_received_date` to table `trxml_object`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	--
	-- Name: to_idx_userid; Type: INDEX; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM pg_indexes WHERE tablename='trxml_object' AND indexname='to_idx_userid') THEN
		BEGIN
			CREATE INDEX to_idx_userid ON trxml_object USING btree (userid);
			RAISE INFO 'Index `to_idx_userid` successfully added to table `trxml_object`';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding index `to_idx_userid` to table `trxml_object`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

		--
	-- Name: to_idx_ext_cand_id; Type: INDEX; Schema: public; Owner:; Tablespace:
	--
	IF NOT EXISTS (SELECT * FROM pg_indexes WHERE tablename='trxml_object' AND indexname='to_idx_ext_cand_id') THEN
		BEGIN
			CREATE INDEX to_idx_ext_cand_id ON trxml_object (ext_candidate_id);
			RAISE INFO 'Index `to_idx_ext_cand_id` successfully added to table `trxml_object`';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding index `to_idx_ext_cand_id` to table `trxml_object`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	--
	-- Name: pu_idx_client; Type: INDEX; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM pg_indexes WHERE tablename='processing_unit' AND indexname='pu_idx_client') THEN
		BEGIN
			CREATE INDEX pu_idx_client ON processing_unit USING btree (client);
			RAISE INFO 'Index `pu_idx_client` successfully added to table `processing_unit`';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding index `pu_idx_client` to table `processing_unit`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	--
	-- Name: eh_idx_event_type; Type: INDEX; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM pg_indexes WHERE tablename='event_history' AND indexname='eh_idx_event_type') THEN
		BEGIN
			CREATE INDEX eh_idx_event_type on event_history (event_type);
			RAISE INFO 'Index `eh_idx_event_type` successfully added to table `event_history`';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding index `eh_idx_event_type` to table `event_history`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	--
	-- Name: eh_idx_userid; Type: INDEX; Schema: public; Owner:; Tablespace:
	--

	IF NOT EXISTS (SELECT * FROM pg_indexes WHERE tablename='event_history' AND indexname='eh_idx_userid') THEN
		BEGIN
			CREATE INDEX eh_idx_userid on event_history (userid);
			RAISE INFO 'Index `eh_idx_userid` successfully added to table `event_history`';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding index `eh_idx_userid` to table `event_history`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	--
	-- Name: client_parent_fkey; Type: FK CONSTRAINT; Schema: public; Owner:
	--

	IF NOT EXISTS (SELECT * FROM information_schema.table_constraints WHERE table_name='client' AND constraint_name='client_parent_fkey') THEN
		BEGIN
			ALTER TABLE ONLY client
				ADD CONSTRAINT client_parent_fkey FOREIGN KEY (parent) REFERENCES client(id) ON DELETE SET NULL;
			RAISE INFO 'Constraint `client_parent_fkey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `client_parent_fkey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	--
	-- Name: event_document_client_fkey; Type: FK CONSTRAINT; Schema: public; Owner:
	--

	IF NOT EXISTS (SELECT * FROM information_schema.table_constraints WHERE table_name='event_document' AND constraint_name='event_document_client_fkey') THEN
		BEGIN
			res := make_data_consistent('event_document', 'client', 'client', 'id');
			ALTER TABLE ONLY event_document
				ADD CONSTRAINT event_document_client_fkey FOREIGN KEY (client) REFERENCES client(id) ON DELETE CASCADE;
			RAISE INFO 'Constraint `event_document_client_fkey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `event_document_client_fkey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	--
	-- Name: event_document_userid_fkey; Type: FK CONSTRAINT; Schema: public; Owner:
	--

	IF NOT EXISTS (SELECT * FROM information_schema.table_constraints WHERE table_name='event_document' AND constraint_name='event_document_userid_fkey') THEN
		BEGIN
			res := make_data_consistent('event_document', 'users', 'userid', 'id');
			ALTER TABLE ONLY event_document
				ADD CONSTRAINT event_document_userid_fkey FOREIGN KEY (userid) REFERENCES users(id) ON DELETE CASCADE;
			RAISE INFO 'Constraint `event_document_userid_fkey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `event_document_userid_fkey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	--
	-- Name: event_history_trxmlid_fkey; Type: FK CONSTRAINT; Schema: public; Owner:
	--

	IF NOT EXISTS (SELECT * FROM information_schema.table_constraints WHERE table_name='event_history' AND constraint_name='event_history_trxmlid_fkey') THEN
		BEGIN
			res := make_data_consistent('event_history', 'event_document', 'trxmlid', 'trxmlid');
			ALTER TABLE ONLY event_history
				ADD CONSTRAINT event_history_trxmlid_fkey FOREIGN KEY (trxmlid) REFERENCES event_document(trxmlid) ON DELETE CASCADE;
			RAISE INFO 'Constraint `event_history_trxmlid_fkey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `event_history_trxmlid_fkey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	--
	-- Name: event_history_userid_fkey; Type: FK CONSTRAINT; Schema: public; Owner:
	--

	IF NOT EXISTS (SELECT * FROM information_schema.table_constraints WHERE table_name='event_history' AND constraint_name='event_history_userid_fkey') THEN
		BEGIN
			res := make_data_consistent('event_history', 'users', 'userid', 'id');
			ALTER TABLE ONLY event_history
				ADD CONSTRAINT event_history_userid_fkey FOREIGN KEY (userid) REFERENCES users(id) ON DELETE CASCADE;
			RAISE INFO 'Constraint `event_history_userid_fkey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `event_history_userid_fkey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	--
	-- Name: trxml_object_client_fkey; Type: FK CONSTRAINT; Schema: public; Owner:
	--

	IF NOT EXISTS (SELECT * FROM information_schema.table_constraints WHERE table_name='trxml_object' AND constraint_name='trxml_object_client_fkey') THEN
		BEGIN
			res := make_data_consistent('trxml_object', 'client', 'client', 'id');
			ALTER TABLE ONLY trxml_object
				ADD CONSTRAINT trxml_object_client_fkey FOREIGN KEY (client) REFERENCES client(id) ON DELETE CASCADE;
			RAISE INFO 'Constraint `trxml_object_client_fkey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `trxml_object_client_fkey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;


	--
	-- Name: trxml_object_userid_fkey; Type: FK CONSTRAINT; Schema: public; Owner:
	--

	IF NOT EXISTS (SELECT * FROM information_schema.table_constraints WHERE table_name='trxml_object' AND constraint_name='trxml_object_userid_fkey') THEN
		BEGIN
			res := make_data_consistent('trxml_object', 'users', 'userid', 'id');
			ALTER TABLE ONLY trxml_object
				ADD CONSTRAINT trxml_object_userid_fkey FOREIGN KEY (userid) REFERENCES users(id) ON DELETE CASCADE;
			RAISE INFO 'Constraint `trxml_object_userid_fkey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `trxml_object_userid_fkey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;



	-- 
	-- Now make sure the FK trxml_attachment_trxml_object_fkey does not exist, and replace with a trigger
	--

	IF EXISTS (SELECT * FROM information_schema.table_constraints WHERE table_name='trxml_attachment' AND constraint_name='trxml_attachment_trxml_object_fkey') THEN
		BEGIN
			ALTER TABLE trxml_attachment DROP CONSTRAINT trxml_attachment_trxml_object_fkey;
			RAISE INFO 'Constraint `trxml_attachment_trxml_object_fkey` successfully removed';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while removing constraint `trxml_attachment_trxml_object_fkey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	--
	-- Create the triggers
	--
	CREATE OR REPLACE FUNCTION trxml_attachment_require_trxml_object()
	RETURNS TRIGGER LANGUAGE plpgsql AS
	$BODY$

	BEGIN
	    PERFORM 1 FROM trxml_object tobj WHERE NEW.trxml_object = tobj.id;
	    IF NOT FOUND THEN
		RAISE EXCEPTION 'No trxml_object with id %', NEW.trxml_object;
		END IF;
	    RETURN NEW;
	END;
	$BODY$;

	CREATE OR REPLACE FUNCTION trxml_object_delete_attachment_on_delete()
	RETURNS TRIGGER LANGUAGE plpgsql AS
	$BODY$
	BEGIN

	    DELETE FROM trxml_attachment WHERE trxml_object = OLD.id;
	    RETURN OLD;

	END;
	$BODY$;

	--
	-- And add the triggers to trxml_attachment and trxml_object
	--

	IF NOT EXISTS (SELECT * FROM information_schema.triggers WHERE trigger_name = 'trxml_object_require') THEN
		BEGIN

			CREATE TRIGGER trxml_object_require
		       		BEFORE INSERT ON trxml_attachment
		    		FOR EACH ROW EXECUTE PROCEDURE trxml_attachment_require_trxml_object();

			RAISE INFO 'Trigger `trxml_object_require` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding trigger `trxml_object_require`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;
	IF NOT EXISTS (SELECT * FROM information_schema.triggers WHERE trigger_name = 'delete_attachment_on_delete') THEN
		BEGIN

			CREATE TRIGGER delete_attachment_on_delete
				BEFORE DELETE ON trxml_object
				FOR EACH ROW EXECUTE PROCEDURE trxml_object_delete_attachment_on_delete();

			RAISE INFO 'Trigger `delete_attachment_on_delete` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding trigger `delete_attachment_on_delete`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	--
	-- Name: public; Type: ACL; Schema: -; Owner:
	--

	REVOKE ALL ON SCHEMA public FROM PUBLIC;
	EXECUTE 'REVOKE ALL ON SCHEMA public FROM ' || owner_user;
	EXECUTE 'GRANT ALL ON SCHEMA public TO ' || owner_user;
	GRANT ALL ON SCHEMA public TO PUBLIC;



	--
	-- Name: client; Type: ACL; Schema: public; Owner:
	--

	REVOKE ALL ON TABLE client FROM PUBLIC;
	EXECUTE 'GRANT INSERT,SELECT,UPDATE,DELETE ON TABLE client TO ' || webapp_user;


	--
	-- Name: client_id_seq; Type: ACL; Schema: public; Owner:
	--

	REVOKE ALL ON TABLE client_id_seq FROM PUBLIC;
	EXECUTE 'REVOKE ALL ON TABLE client_id_seq FROM ' || owner_user;
	EXECUTE 'GRANT ALL ON TABLE client_id_seq TO ' || owner_user;
	EXECUTE 'GRANT SELECT,UPDATE ON TABLE client_id_seq TO ' || webapp_user;


	--
	-- Name: codelist; Type: ACL; Schema: public; Owner:
	--

	REVOKE ALL ON TABLE codelist FROM PUBLIC;
	EXECUTE 'GRANT INSERT,SELECT,UPDATE,DELETE ON TABLE codelist TO ' || webapp_user;


	--
	-- Name: codelist_def; Type: ACL; Schema: public; Owner:
	--

	REVOKE ALL ON TABLE codelist_def FROM PUBLIC;
	EXECUTE 'GRANT INSERT,SELECT,UPDATE,DELETE ON TABLE codelist_def TO ' || webapp_user;


	--
	-- Name: codelist_def_id_seq; Type: ACL; Schema: public; Owner:
	--

	REVOKE ALL ON TABLE codelist_def_id_seq FROM PUBLIC;
	EXECUTE 'GRANT SELECT,UPDATE ON TABLE codelist_def_id_seq TO ' || webapp_user;


	--
	-- Name: codelist_id_seq; Type: ACL; Schema: public; Owner:
	--

	REVOKE ALL ON TABLE codelist_id_seq FROM PUBLIC;
	EXECUTE 'REVOKE ALL ON TABLE codelist_id_seq FROM ' || owner_user;
	EXECUTE 'GRANT ALL ON TABLE codelist_id_seq TO ' || owner_user;
	EXECUTE 'GRANT SELECT,UPDATE ON TABLE codelist_id_seq TO ' || webapp_user;


	--
	-- Name: codeproperty; Type: ACL; Schema: public; Owner:
	--

	REVOKE ALL ON TABLE codeproperty FROM PUBLIC;
	EXECUTE 'GRANT INSERT,SELECT,UPDATE,DELETE ON TABLE codeproperty TO ' || webapp_user;


	--
	-- Name: codeproperty_id_seq; Type: ACL; Schema: public; Owner:
	--

	REVOKE ALL ON TABLE codeproperty_id_seq FROM PUBLIC;
	EXECUTE 'REVOKE ALL ON TABLE codeproperty_id_seq FROM ' || owner_user;
	EXECUTE 'GRANT ALL ON TABLE codeproperty_id_seq TO ' || owner_user;
	EXECUTE 'GRANT SELECT,UPDATE ON TABLE codeproperty_id_seq TO ' || webapp_user;


	--
	-- Name: event_document; Type: ACL; Schema: public; Owner:
	--

	REVOKE ALL ON TABLE event_document FROM PUBLIC;
	EXECUTE 'REVOKE ALL ON TABLE event_document FROM ' || owner_user;
	EXECUTE 'GRANT ALL ON TABLE event_document TO ' || owner_user;
	EXECUTE 'GRANT INSERT,SELECT,DELETE ON TABLE event_document TO ' || webapp_user;


	--
	-- Name: event_history; Type: ACL; Schema: public; Owner:
	--

	REVOKE ALL ON TABLE event_history FROM PUBLIC;
	EXECUTE 'REVOKE ALL ON TABLE event_history FROM ' || owner_user;
	EXECUTE 'GRANT ALL ON TABLE event_history TO ' || owner_user;
	EXECUTE 'GRANT INSERT,SELECT,DELETE ON TABLE event_history TO ' || webapp_user;


	--
	-- Name: event_history_seq; Type: ACL; Schema: public; Owner:
	--

	REVOKE ALL ON TABLE event_history_seq FROM PUBLIC;
	EXECUTE 'REVOKE ALL ON TABLE event_history_seq FROM ' || owner_user;
	EXECUTE 'GRANT ALL ON TABLE event_history_seq TO ' || owner_user;
	EXECUTE 'GRANT SELECT,UPDATE ON TABLE event_history_seq TO ' || webapp_user;


	--
	-- Name: processing_unit; Type: ACL; Schema: public; Owner:
	--

	REVOKE ALL ON TABLE processing_unit FROM PUBLIC;
	EXECUTE 'GRANT INSERT,SELECT,UPDATE,DELETE ON TABLE processing_unit TO ' || webapp_user;


	--
	-- Name: processing_unit_id_seq; Type: ACL; Schema: public; Owner:
	--

	REVOKE ALL ON TABLE processing_unit_id_seq FROM PUBLIC;
	EXECUTE 'GRANT SELECT,UPDATE ON TABLE processing_unit_id_seq TO ' || webapp_user;


	--
	-- Name: product; Type: ACL; Schema: public; Owner:
	--

	REVOKE ALL ON TABLE product FROM PUBLIC;
	EXECUTE 'GRANT INSERT,SELECT,UPDATE,DELETE ON TABLE product TO ' || webapp_user;


	--
	-- Name: product_id_seq; Type: ACL; Schema: public; Owner:
	--

	REVOKE ALL ON TABLE product_id_seq FROM PUBLIC;
	EXECUTE 'REVOKE ALL ON TABLE product_id_seq FROM ' || owner_user;
	EXECUTE 'GRANT ALL ON TABLE product_id_seq TO ' || owner_user;
	EXECUTE 'GRANT SELECT,UPDATE ON TABLE product_id_seq TO ' || webapp_user;


	--
	-- Name: role; Type: ACL; Schema: public; Owner:
	--

	REVOKE ALL ON TABLE "role" FROM PUBLIC;
	EXECUTE 'GRANT INSERT,SELECT,UPDATE,DELETE ON TABLE "role" TO ' || webapp_user;


	--
	-- Name: role_id_seq; Type: ACL; Schema: public; Owner:
	--

	REVOKE ALL ON TABLE role_id_seq FROM PUBLIC;
	EXECUTE 'REVOKE ALL ON TABLE role_id_seq FROM ' || owner_user;
	EXECUTE 'GRANT ALL ON TABLE role_id_seq TO ' || owner_user;
	EXECUTE 'GRANT SELECT,UPDATE ON TABLE role_id_seq TO ' || webapp_user;


	--
	-- Name: routing; Type: ACL; Schema: public; Owner:
	--

	REVOKE ALL ON TABLE routing FROM PUBLIC;
	EXECUTE 'GRANT INSERT,SELECT,UPDATE,DELETE ON TABLE routing TO ' || webapp_user;


	--
	-- Name: routing_id_seq; Type: ACL; Schema: public; Owner:
	--

	REVOKE ALL ON TABLE routing_id_seq FROM PUBLIC;
	EXECUTE 'REVOKE ALL ON TABLE routing_id_seq FROM ' || owner_user;
	EXECUTE 'GRANT ALL ON TABLE routing_id_seq TO ' || owner_user;
	EXECUTE 'GRANT SELECT,UPDATE ON TABLE routing_id_seq TO ' || webapp_user;


	--
	-- Name: status; Type: ACL; Schema: public; Owner:
	--

	REVOKE ALL ON TABLE status FROM PUBLIC;
	EXECUTE 'REVOKE ALL ON TABLE status FROM ' || owner_user;
	EXECUTE 'GRANT ALL ON TABLE status TO ' || owner_user;
	EXECUTE 'GRANT ALL ON TABLE status TO ' || webapp_user;


	--
	-- Name: trxml_attachment; Type: ACL; Schema: public; Owner:
	--

	REVOKE ALL ON TABLE trxml_attachment FROM PUBLIC;
	EXECUTE 'GRANT INSERT,SELECT,UPDATE,DELETE ON TABLE trxml_attachment TO ' || webapp_user;


	--
	-- Name: trxml_attachment_seq; Type: ACL; Schema: public; Owner:
	--

	REVOKE ALL ON TABLE trxml_attachment_seq FROM PUBLIC;
	EXECUTE 'REVOKE ALL ON TABLE trxml_attachment_seq FROM ' || owner_user;
	EXECUTE 'GRANT ALL ON TABLE trxml_attachment_seq TO ' || owner_user;
	EXECUTE 'GRANT SELECT,UPDATE ON TABLE trxml_attachment_seq TO ' || webapp_user;


	--
	-- Name: trxml_object; Type: ACL; Schema: public; Owner:
	--

	REVOKE ALL ON TABLE trxml_object FROM PUBLIC;
	EXECUTE 'GRANT INSERT,SELECT,UPDATE,DELETE ON TABLE trxml_object TO ' || webapp_user;


	--
	-- Name: trxml_object_seq; Type: ACL; Schema: public; Owner:
	--

	REVOKE ALL ON TABLE trxml_object_seq FROM PUBLIC;
	EXECUTE 'REVOKE ALL ON TABLE trxml_object_seq FROM ' || owner_user;
	EXECUTE 'GRANT ALL ON TABLE trxml_object_seq TO ' || owner_user;
	EXECUTE 'GRANT SELECT,UPDATE ON TABLE trxml_object_seq TO ' || webapp_user;


	--
	-- Name: users; Type: ACL; Schema: public; Owner:
	--

	REVOKE ALL ON TABLE users FROM PUBLIC;
	EXECUTE 'GRANT INSERT,SELECT,UPDATE,DELETE ON TABLE users TO ' || webapp_user;


	--
	-- Name: users_id_seq; Type: ACL; Schema: public; Owner:
	--

	REVOKE ALL ON TABLE users_id_seq FROM PUBLIC;
	EXECUTE 'REVOKE ALL ON TABLE users_id_seq FROM ' || owner_user;
	EXECUTE 'GRANT ALL ON TABLE users_id_seq TO ' || owner_user;
	EXECUTE 'GRANT SELECT,UPDATE ON TABLE users_id_seq TO ' || webapp_user;


	-- Name: users; Type: ACL; Schema: public; Owner:
	--

	REVOKE ALL ON TABLE mailbox FROM PUBLIC;
	EXECUTE 'GRANT INSERT,SELECT,UPDATE,DELETE ON TABLE mailbox TO ' || webapp_user;


	--
	-- Name: users_id_seq; Type: ACL; Schema: public; Owner:
	--

	REVOKE ALL ON TABLE mailbox_seq FROM PUBLIC;
	EXECUTE 'REVOKE ALL ON TABLE mailbox_seq FROM ' || owner_user;
	EXECUTE 'GRANT ALL ON TABLE mailbox_seq TO ' || owner_user;
	EXECUTE 'GRANT SELECT,UPDATE ON TABLE mailbox_seq TO ' || webapp_user;


	--
	-- Name: import_info; Type: ACL; Schema: public; Owner:
	--
	REVOKE ALL ON TABLE import_info FROM PUBLIC;
	EXECUTE 'GRANT INSERT,SELECT,UPDATE,DELETE ON TABLE import_info TO ' || webapp_user;

		--
	-- Name: import_info_seq; Type: ACL; Schema: public; Owner:
	--
	REVOKE ALL ON TABLE import_info_seq FROM PUBLIC;
	EXECUTE 'REVOKE ALL ON TABLE import_info_seq FROM ' || owner_user;
	EXECUTE 'GRANT ALL ON TABLE import_info_seq TO ' || owner_user;
	EXECUTE 'GRANT SELECT,UPDATE ON TABLE import_info_seq TO ' || webapp_user;

	
	-- For Indexing Trxmls for Search:

	-- Define a sequence that creates unique and incremental update numbers:
	IF NOT EXISTS (SELECT * FROM pg_statio_user_sequences where relname = 'trxml_update_seq') THEN
		BEGIN
			CREATE SEQUENCE trxml_update_seq;
			RAISE INFO 'Sequence `trxml_update_seq` is created';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while creating sequence `trxml_update_seq`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	EXECUTE 'ALTER TABLE public.trxml_update_seq OWNER TO ' || owner_user;

	REVOKE ALL ON public.trxml_update_seq FROM PUBLIC;
	EXECUTE 'REVOKE ALL ON TABLE public.trxml_update_seq FROM ' || owner_user;
	EXECUTE 'GRANT ALL ON TABLE public.trxml_update_seq TO ' || owner_user;
	EXECUTE 'GRANT SELECT,UPDATE ON TABLE public.trxml_update_seq TO ' || webapp_user;


	-- Add the automatically incrementing sequence column to the trxml_object table:
	-- (Adding the column now will add the incremental sequence value to all existing rows)
	IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='trxml_object' AND column_name='update_seq') THEN
		ALTER TABLE trxml_object ADD COLUMN update_seq bigint DEFAULT nextval('trxml_update_seq');
		RAISE INFO 'Column `update_seq` added to: trxml_object';
	END IF;

	-- ADD a index on trxml_object.update.seq

	IF NOT EXISTS (SELECT * FROM pg_indexes WHERE tablename='trxml_object' AND indexname='to_idx_update_seq') THEN
		BEGIN
			CREATE INDEX to_idx_update_seq ON trxml_object USING btree (update_seq);
			RAISE INFO 'Index `to_idx_update_seq` successfully added to table `trxml_object`';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding index `to_idx_update_seq` to table `trxml_object`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;
	
	-- Add the automatically incrementing sequence column to the event_history table to keep track of deletes:
	-- (Adding the column now will add the incremental sequence value to all existing rows)
	IF NOT EXISTS(SELECT * FROM information_schema.columns WHERE table_name='event_history' AND column_name='update_seq') THEN
		ALTER TABLE event_history ADD COLUMN update_seq bigint DEFAULT nextval('trxml_update_seq');
		RAISE INFO 'Column `update_seq` added to: event_history';
	END IF;
	
		-- ADD a index on event_history.update.seq
	
	IF NOT EXISTS (SELECT * FROM pg_indexes WHERE tablename='event_history' AND indexname='eh_idx_update_seq') THEN
		BEGIN
			CREATE INDEX eh_idx_update_seq ON event_history USING btree (update_seq);
			RAISE INFO 'Index `eh_idx_update_seq` successfully added to table `event_history`';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding index `eh_idx_update_seq` to table `event_history`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;
	
	
	-- drop unsued table codelist_values
	IF EXISTS (SELECT * FROM pg_tables where tablename = 'codelist_values') THEN
		BEGIN
			DROP TABLE codelist_values;
			RAISE INFO 'Table `codelist_values` dropped';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while dropping table `codelist_values`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;
		
	

	-- Improvements to the datamodel, adding UK's, FK's and PK's where needed

	-- Add PK's

	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE table_name='codelist_def' AND constraint_name='codelist_def_pkey') THEN
		BEGIN
			ALTER TABLE ONLY codelist_def ADD CONSTRAINT codelist_def_pkey PRIMARY KEY (id);
			RAISE INFO 'Constraint `codelist_def_pkey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `codelist_def_pkey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	-- Add UK's
	
	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE table_name='client' AND constraint_name='client_ukey') THEN
		BEGIN
			ALTER TABLE ONLY client ADD CONSTRAINT client_ukey UNIQUE (account);
			RAISE INFO 'Constraint `client_ukey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `client_ukey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;
	
	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE table_name='client' AND constraint_name='client_ext_ukey') THEN
		BEGIN
			ALTER TABLE ONLY client ADD CONSTRAINT client_ext_ukey UNIQUE (external_account_id);
			RAISE INFO 'Constraint `client_ext_ukey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `client_ext_ukey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE table_name='codelist_def' AND constraint_name='codelist_def_ukey') THEN
		BEGIN
			DELETE FROM codelist_def WHERE version <> '1.0';
			ALTER TABLE ONLY codelist_def ADD CONSTRAINT codelist_def_ukey UNIQUE (client,fieldname);
			RAISE INFO 'Constraint `codelist_def_ukey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `codelist_def_ukey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE table_name='codelist' AND constraint_name='codelist_ukey') THEN
		BEGIN
			DELETE FROM codelist WHERE version <> '1.0';
--			DELETE FROM codelist WHERE id IN (856363, 877009);
			ALTER TABLE ONLY codelist ADD CONSTRAINT codelist_ukey UNIQUE (client,fieldname,codeid);
			RAISE INFO 'Constraint `codelist_ukey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `codelist_ukey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE table_name='codeproperty' AND constraint_name='codeproperty_ukey') THEN
		BEGIN
			DELETE FROM codeproperty WHERE NOT EXISTS (SELECT 1 FROM codelist c WHERE codelistid = c.id);
			ALTER TABLE ONLY codeproperty ADD CONSTRAINT codeproperty_ukey UNIQUE (codelistid,propertyname);
			RAISE INFO 'Constraint `codeproperty_ukey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `codeproperty_ukey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE table_name='mailbox' AND constraint_name='mailbox_ukey') THEN
		BEGIN
			ALTER TABLE ONLY mailbox ADD CONSTRAINT mailbox_ukey UNIQUE (name);
			RAISE INFO 'Constraint `mailbox_ukey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `mailbox_ukey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE table_name='product' AND constraint_name='product_ukey') THEN
		BEGIN
			ALTER TABLE ONLY product ADD CONSTRAINT product_ukey UNIQUE (name);
			RAISE INFO 'Constraint `product_ukey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `product_ukey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE table_name='role' AND constraint_name='role_ukey') THEN
		BEGIN
			ALTER TABLE ONLY role ADD CONSTRAINT role_ukey UNIQUE (userid,rolename);
			RAISE INFO 'Constraint `role_ukey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `role_ukey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE table_name='users' AND constraint_name='users_ukey') THEN
		BEGIN
			ALTER TABLE ONLY users ADD CONSTRAINT users_ukey UNIQUE (client,name);
			RAISE INFO 'Constraint `users_ukey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `users_ukey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;
	
	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE table_name='users' AND constraint_name='users_ext_ukey') THEN
		BEGIN
			ALTER TABLE ONLY users ADD CONSTRAINT users_ext_ukey UNIQUE (client,external_user_id);
			RAISE INFO 'Constraint `users_ext_ukey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `users_ext_ukey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;
	
	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE table_name='users' AND constraint_name='users_widget_token_ukey') THEN
		BEGIN
			ALTER TABLE ONLY users ADD CONSTRAINT users_widget_token_ukey UNIQUE (widget_token);
			RAISE INFO 'Constraint `users_widget_token_ukey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `users_widget_token_ukey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;
	
	-- Add foreign keys
	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE constraint_name='client_product_fkey') THEN
		BEGIN
			ALTER TABLE client ADD CONSTRAINT client_product_fkey FOREIGN KEY (product) REFERENCES product(id);
			RAISE INFO 'Constraint `client_product_fkey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO'Error occurred while adding constraint `client_product_fkey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE constraint_name='routing_client_fkey') THEN
		BEGIN
			ALTER TABLE routing ADD CONSTRAINT routing_client_fkey FOREIGN KEY (client) REFERENCES client(id) ON DELETE CASCADE;
			RAISE INFO 'Constraint `routing_client_fkey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `routing_client_fkey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE constraint_name='users_client_fkey') THEN
		BEGIN
			ALTER TABLE users ADD CONSTRAINT users_client_fkey FOREIGN KEY (client) REFERENCES client(id) ON DELETE CASCADE;
			RAISE INFO 'Constraint `users_client_fkey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `users_client_fkey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE constraint_name='role_users_fkey') THEN
		BEGIN
			DELETE FROM role WHERE NOT EXISTS (SELECT 1 FROM users u WHERE userid = u.id);
			ALTER TABLE role ADD CONSTRAINT role_users_fkey FOREIGN KEY (userid) REFERENCES users(id) ON DELETE CASCADE;
			RAISE INFO 'Constraint `role_users_fkey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `role_users_fkey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE constraint_name='import_info_fkey') THEN
		BEGIN
			ALTER TABLE import_info ADD CONSTRAINT import_info_fkey FOREIGN KEY (client) REFERENCES client(id) ON DELETE CASCADE;
			RAISE INFO 'Constraint `import_info_fkey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `import_info_fkey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE constraint_name='codelist_def_client_fkey') THEN
		BEGIN
			ALTER TABLE codelist_def ADD CONSTRAINT codelist_def_client_fkey FOREIGN KEY (client) REFERENCES client(id) ON DELETE CASCADE;
			RAISE INFO 'Constraint `codelist_def_client_fkey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `codelist_def_client_fkey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE constraint_name='codelist_client_fkey') THEN
		BEGIN
			ALTER TABLE codelist ADD CONSTRAINT codelist_client_fkey FOREIGN KEY (client) REFERENCES client(id) ON DELETE CASCADE;
			RAISE INFO 'Constraint `codelist_client_fkey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `codelist_client_fkey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	IF NOT EXISTS (SELECT * FROM information_schema.constraint_table_usage WHERE constraint_name='codeproperty_codelist_fkey') THEN
		BEGIN
			DELETE FROM codeproperty WHERE NOT EXISTS (SELECT 1 FROM codelist c WHERE codelistid = c.id);
			ALTER TABLE codeproperty ADD CONSTRAINT codeproperty_codelist_fkey FOREIGN KEY (codelistid) REFERENCES codelist(id) ON DELETE CASCADE;
			RAISE INFO 'Constraint `codeproperty_codelist_fkey` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding constraint `codeproperty_codelist_fkey`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	-- Add cascading to certain fk's (consolidation of current production status)
	
	IF NOT EXISTS (SELECT 1 FROM pg_catalog.pg_constraint r WHERE conname = 'event_document_client_fkey' and r.contype = 'f' and pg_catalog.pg_get_constraintdef(r.oid, true) like '%CASCADE%') THEN
		ALTER TABLE event_document DROP CONSTRAINT event_document_client_fkey;
		ALTER TABLE event_document ADD CONSTRAINT event_document_client_fkey FOREIGN KEY (client) REFERENCES client(id) ON DELETE CASCADE;
		RAISE INFO 'Constraint event_document_client_fkey updated';
	END IF;
	IF NOT EXISTS (SELECT 1 FROM pg_catalog.pg_constraint r WHERE conname = 'event_document_userid_fkey' and r.contype = 'f' and pg_catalog.pg_get_constraintdef(r.oid, true) like '%CASCADE%') THEN
		ALTER TABLE event_document DROP CONSTRAINT event_document_userid_fkey;
		ALTER TABLE event_document ADD CONSTRAINT event_document_userid_fkey FOREIGN KEY (userid) REFERENCES users(id) ON DELETE CASCADE;
		RAISE INFO 'Constraint event_document_userid_fkey updated';
	END IF;
	IF NOT EXISTS (SELECT 1 FROM pg_catalog.pg_constraint r WHERE conname = 'event_history_userid_fkey' and r.contype = 'f' and pg_catalog.pg_get_constraintdef(r.oid, true) like '%CASCADE%') THEN
		ALTER TABLE event_history DROP CONSTRAINT event_history_userid_fkey;
		ALTER TABLE event_history ADD CONSTRAINT event_history_userid_fkey FOREIGN KEY (userid) REFERENCES users(id) ON DELETE CASCADE;
		RAISE INFO 'Constraint event_history_userid_fkey updated';
	END IF;
	IF NOT EXISTS (SELECT 1 FROM pg_catalog.pg_constraint r WHERE conname = 'event_history_trxmlid_fkey' and r.contype = 'f' and pg_catalog.pg_get_constraintdef(r.oid, true) like '%CASCADE%') THEN
		ALTER TABLE event_history DROP CONSTRAINT event_history_trxmlid_fkey;
		ALTER TABLE event_history ADD CONSTRAINT event_history_trxmlid_fkey FOREIGN KEY (trxmlid) REFERENCES event_document(trxmlid) ON DELETE CASCADE;
		RAISE INFO 'Constraint event_history_trxmlid_fkey updated';
	END IF;

	IF NOT EXISTS (SELECT 1 FROM pg_catalog.pg_constraint r WHERE conname = 'trxml_object_client_fkey' and r.contype = 'f' and pg_catalog.pg_get_constraintdef(r.oid, true) like '%CASCADE%') THEN
		ALTER TABLE trxml_object DROP CONSTRAINT trxml_object_client_fkey;
		ALTER TABLE trxml_object ADD CONSTRAINT trxml_object_client_fkey FOREIGN KEY (client) REFERENCES client(id) ON DELETE CASCADE;
		RAISE INFO 'Constraint trxml_object_client_fkey updated';
	END IF;
	IF NOT EXISTS (SELECT 1 FROM pg_catalog.pg_constraint r WHERE conname = 'trxml_object_userid_fkey' and r.contype = 'f' and pg_catalog.pg_get_constraintdef(r.oid, true) like '%CASCADE%') THEN
		ALTER TABLE trxml_object DROP CONSTRAINT trxml_object_userid_fkey;
		ALTER TABLE trxml_object ADD CONSTRAINT trxml_object_userid_fkey FOREIGN KEY (userid) REFERENCES users(id) ON DELETE CASCADE;
		RAISE INFO 'Constraint trxml_object_userid_fkey updated';
	END IF;

	-- Add auditing, give date_created a default value
	
	IF EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name='event_document' AND column_name='date_created' AND column_default IS NULL) THEN
	BEGIN
		RAISE INFO 'Column `event_document.date_created` changing';
		UPDATE event_document SET date_created = ('01-01-2000') WHERE date_created IS NULL;
		ALTER TABLE event_document ALTER COLUMN date_created SET DEFAULT now();
		ALTER TABLE event_document ALTER COLUMN date_created SET NOT NULL;
		RAISE INFO 'Column `event_document.date_created` successfully changed';
		EXCEPTION WHEN OTHERS THEN
			RAISE INFO 'Error occurred while changing column `event_document.date_created`';
			RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
	END;
	END IF;

	IF EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name='trxml_attachment' AND column_name='date_created' AND column_default IS NULL) THEN
	BEGIN
		RAISE INFO 'Column `trxml_attachment.date_created` changing';
		UPDATE trxml_attachment SET date_created = ('01-01-2000') WHERE date_created IS NULL;
		ALTER TABLE trxml_attachment ALTER COLUMN date_created SET DEFAULT current_date;
		ALTER TABLE trxml_attachment ALTER COLUMN date_created SET NOT NULL;
		RAISE INFO 'Column `trxml_attachment.date_created` successfully changed';
		EXCEPTION WHEN OTHERS THEN
			RAISE INFO 'Error occurred while changing column `trxml_attachment.date_created`';
			RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
	END;
	END IF;

	IF EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name='trxml_object' AND column_name='date_created' AND column_default IS NULL) THEN
	BEGIN
		RAISE INFO 'Column `trxml_object.date_created` changing';
		UPDATE trxml_object SET date_created = ('01-01-2000') WHERE date_created IS NULL;
		ALTER TABLE trxml_object ALTER COLUMN date_created SET DEFAULT now();
		ALTER TABLE trxml_object ALTER COLUMN date_created SET NOT NULL;
		RAISE INFO 'Column `trxml_object.date_createled` successfully changed';
		EXCEPTION WHEN OTHERS THEN
			RAISE INFO 'Error occurred while changing column `trxml_object.date_created`';
			RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
	END;
	END IF;

	IF NOT EXISTS (SELECT 1 FROM pg_proc where proname = 'auditing_stamp') THEN
		CREATE OR REPLACE FUNCTION auditing_stamp() RETURNS trigger AS $auditing_stamp$
    	BEGIN
        	NEW.date_modified := current_timestamp;
	        RETURN NEW;
    	END;
		$auditing_stamp$ LANGUAGE plpgsql;
		RAISE INFO 'Trigger function auditing_stamp created';
	END IF;

	IF NOT EXISTS (SELECT * FROM information_schema.triggers WHERE trigger_name = 'trxml_attachment_bru') THEN
		BEGIN
			CREATE TRIGGER trxml_attachment_bru BEFORE UPDATE ON trxml_attachment
   				FOR EACH ROW EXECUTE PROCEDURE auditing_stamp();
			RAISE INFO 'Trigger `trxml_attachment_bru` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding trigger `trxml_attachment_bru`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;
	
	IF NOT EXISTS (SELECT * FROM information_schema.triggers WHERE trigger_name = 'users_bru') THEN
		BEGIN
			CREATE TRIGGER users_bru BEFORE UPDATE ON users
    			FOR EACH ROW EXECUTE PROCEDURE auditing_stamp();

			RAISE INFO 'Trigger `users_bru` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding trigger `users_bru`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	IF NOT EXISTS (SELECT * FROM information_schema.triggers WHERE trigger_name = 'client_bru') THEN
		BEGIN
			CREATE TRIGGER client_bru BEFORE UPDATE ON client
    			FOR EACH ROW EXECUTE PROCEDURE auditing_stamp();

			RAISE INFO 'Trigger `client_bru` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding trigger `client_bru`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	IF NOT EXISTS (SELECT * FROM information_schema.triggers WHERE trigger_name = 'product_bru') THEN
		BEGIN
			CREATE TRIGGER product_bru BEFORE UPDATE ON product
    			FOR EACH ROW EXECUTE PROCEDURE auditing_stamp();

			RAISE INFO 'Trigger `product_bru` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding trigger `product_bru`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	IF NOT EXISTS (SELECT * FROM information_schema.triggers WHERE trigger_name = 'role_bru') THEN
		BEGIN
			CREATE TRIGGER role_bru BEFORE UPDATE ON role
    			FOR EACH ROW EXECUTE PROCEDURE auditing_stamp();

			RAISE INFO 'Trigger `role_bru` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding trigger `role_bru`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	IF NOT EXISTS (SELECT * FROM information_schema.triggers WHERE trigger_name = 'routing_bru') THEN
		BEGIN
			CREATE TRIGGER routing_bru BEFORE UPDATE ON routing
    			FOR EACH ROW EXECUTE PROCEDURE auditing_stamp();

			RAISE INFO 'Trigger `routing_bru` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding trigger `routing_bru`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	IF NOT EXISTS (SELECT * FROM information_schema.triggers WHERE trigger_name = 'codelist_def_bru') THEN
		BEGIN
			CREATE TRIGGER codelist_def_bru BEFORE UPDATE ON codelist_def
    			FOR EACH ROW EXECUTE PROCEDURE auditing_stamp();

			RAISE INFO 'Trigger `codelist_def_bru` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding trigger `codelist_def_bru`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	IF NOT EXISTS (SELECT * FROM information_schema.triggers WHERE trigger_name = 'codelist_bru') THEN
		BEGIN
			CREATE TRIGGER codelist_bru BEFORE UPDATE ON codelist
    			FOR EACH ROW EXECUTE PROCEDURE auditing_stamp();

			RAISE INFO 'Trigger `codelist_bru` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding trigger `codelist_bru`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

	IF NOT EXISTS (SELECT * FROM information_schema.triggers WHERE trigger_name = 'codeproperty_bru') THEN
		BEGIN
			CREATE TRIGGER codeproperty_bru BEFORE UPDATE ON codeproperty
    			FOR EACH ROW EXECUTE PROCEDURE auditing_stamp();

			RAISE INFO 'Trigger `codeproperty_bru` successfully added';
			EXCEPTION WHEN OTHERS THEN
				RAISE INFO 'Error occurred while adding trigger `codeproperty_bru`';
				RAISE EXCEPTION 'NUM:%, DETAILS:%', SQLSTATE, SQLERRM;
		END;
	END IF;

    RETURN true;

END;
$_$;


ALTER FUNCTION public.create_or_adapt_sourcebox_schema(owner_user character varying, webapp_user character varying) OWNER TO postgres;

--
-- Name: FUNCTION create_or_adapt_sourcebox_schema(owner_user character varying, webapp_user character varying); Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON FUNCTION create_or_adapt_sourcebox_schema(owner_user character varying, webapp_user character varying) IS 'Creates or adapts the sourcebox schema to the one used by SB 3.0';


--
-- Name: make_data_consistent(character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION make_data_consistent(source_table character varying, referred_table character varying, source_pk character varying, referred_pk character varying) RETURNS boolean
    LANGUAGE plpgsql
    AS $$BEGIN
	EXECUTE 'DELETE FROM ' || source_table || ' WHERE ' || source_pk || ' IN (SELECT s.' || source_pk || ' FROM ' || source_table || ' s WHERE NOT EXISTS (SELECT r.' || referred_pk || ' FROM ' || referred_table || ' r WHERE s.' || source_pk || ' = r.' || referred_pk || '))';
	RETURN TRUE;
END;$$;


ALTER FUNCTION public.make_data_consistent(source_table character varying, referred_table character varying, source_pk character varying, referred_pk character varying) OWNER TO postgres;

--
-- Name: FUNCTION make_data_consistent(source_table character varying, referred_table character varying, source_pk character varying, referred_pk character varying); Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON FUNCTION make_data_consistent(source_table character varying, referred_table character varying, source_pk character varying, referred_pk character varying) IS 'before adding a foreign key constraint on non-empty table, makes data consistent within the two tables bound together by the FK';


--
-- Name: trxml_attachment_require_trxml_object(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION trxml_attachment_require_trxml_object() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

	BEGIN
	    PERFORM 1 FROM trxml_object tobj WHERE NEW.trxml_object = tobj.id;
	    IF NOT FOUND THEN
		RAISE EXCEPTION 'No trxml_object with id %', NEW.trxml_object;
		END IF;
	    RETURN NEW;
	END;
	$$;


ALTER FUNCTION public.trxml_attachment_require_trxml_object() OWNER TO postgres;

--
-- Name: trxml_object_delete_attachment_on_delete(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION trxml_object_delete_attachment_on_delete() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	BEGIN

	    DELETE FROM trxml_attachment WHERE trxml_object = OLD.id;
	    RETURN OLD;

	END;
	$$;


ALTER FUNCTION public.trxml_object_delete_attachment_on_delete() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = true;

--
-- Name: client; Type: TABLE; Schema: public; Owner: web; Tablespace: 
--

CREATE TABLE client (
    id integer DEFAULT nextval(('client_id_seq'::text)::regclass) NOT NULL,
    enabled character(1) DEFAULT 't'::bpchar,
    valid_until date,
    name character varying,
    credit_limit integer,
    product integer,
    account character varying,
    fourweekperiod character(1) DEFAULT 'f'::bpchar,
    savepath character varying,
    has_custom_save_path character(1) DEFAULT 'f'::bpchar,
    model character varying,
    start_date date,
    lang character varying,
    interface_lang character varying,
    external_repository character varying,
    external_repository_activated character(1),
    external_account_id character varying,
    persisting character(1) DEFAULT 'f'::bpchar,
    cassandra_enabled character(1) DEFAULT 'f'::bpchar,
    saml_idp character varying,
    saml_domain_mapping character varying,
    parent integer,
    custom_string_1 character varying,
    custom_string_2 character varying,
    custom_string_3 character varying,
    custom_string_4 character varying,
    custom_string_5 character varying,
    custom_string_6 character varying,
    custom_string_7 character varying,
    custom_string_8 character varying,
    custom_string_9 character varying,
    custom_string_10 character varying,
    custom_string_11 character varying,
    custom_string_12 character varying,
    custom_string_13 character varying,
    custom_string_14 character varying,
    custom_string_15 character varying,
    custom_string_16 character varying,
    custom_string_17 character varying,
    custom_string_18 character varying,
    custom_string_19 character varying,
    custom_string_20 character varying,
    custom_int_1 integer,
    custom_int_2 integer,
    custom_int_3 integer,
    custom_int_4 integer,
    custom_tstamp_1 timestamp without time zone,
    custom_tstamp_2 timestamp without time zone,
    custom_boolean_1 character(1),
    custom_boolean_2 character(1),
    custom_boolean_3 character(1),
    custom_boolean_4 character(1),
    custom_boolean_5 character(1),
    custom_boolean_6 character(1),
    linkedin_email character varying,
    linkedin_password character varying,
    linkedin_access_token character varying,
    linkedin_secret_token character varying,
    linkedin_activated character(1),
    search_activated character(1),
    search_config character varying,
    search_config_password character varying,
    search_url character varying,
    indexing_url character varying,
    indexing_config character varying,
    indexing_config_password character varying,
    last_indexing_update bigint,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    date_modified timestamp without time zone,
    bullhorn_subscription_id character varying,
    bullhorn_last_request_id bigint,
    custom_search_roles_ro character varying,
    document_sharing_ro character varying
);


ALTER TABLE client OWNER TO web;

--
-- Name: client_id_seq; Type: SEQUENCE; Schema: public; Owner: web
--

CREATE SEQUENCE client_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE client_id_seq OWNER TO web;

--
-- Name: codelist; Type: TABLE; Schema: public; Owner: web; Tablespace: 
--

CREATE TABLE codelist (
    id integer DEFAULT nextval(('codelist_id_seq'::text)::regclass) NOT NULL,
    client integer,
    numcode character varying,
    alphacode character varying,
    fieldname character varying,
    version character varying,
    versiondate date,
    description character varying,
    codeid character varying,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    date_modified timestamp without time zone
);


ALTER TABLE codelist OWNER TO web;

--
-- Name: codelist_def; Type: TABLE; Schema: public; Owner: web; Tablespace: 
--

CREATE TABLE codelist_def (
    id integer NOT NULL,
    client integer,
    fieldname character varying,
    version character varying,
    versiondate date,
    current_ind character(1),
    versiontime time without time zone,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    date_modified timestamp without time zone
);


ALTER TABLE codelist_def OWNER TO web;

--
-- Name: codelist_def_id_seq; Type: SEQUENCE; Schema: public; Owner: web
--

CREATE SEQUENCE codelist_def_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE codelist_def_id_seq OWNER TO web;

--
-- Name: codelist_def_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: web
--

ALTER SEQUENCE codelist_def_id_seq OWNED BY codelist_def.id;


--
-- Name: codelist_id_seq; Type: SEQUENCE; Schema: public; Owner: web
--

CREATE SEQUENCE codelist_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE codelist_id_seq OWNER TO web;

--
-- Name: codeproperty; Type: TABLE; Schema: public; Owner: web; Tablespace: 
--

CREATE TABLE codeproperty (
    id integer DEFAULT nextval(('codeproperty_id_seq'::text)::regclass) NOT NULL,
    codelistid integer,
    propertyname character varying,
    propertyvalue character varying,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    date_modified timestamp without time zone
);


ALTER TABLE codeproperty OWNER TO web;

--
-- Name: codeproperty_id_seq; Type: SEQUENCE; Schema: public; Owner: web
--

CREATE SEQUENCE codeproperty_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE codeproperty_id_seq OWNER TO web;

SET default_with_oids = false;

--
-- Name: event_document; Type: TABLE; Schema: public; Owner: web; Tablespace: 
--

CREATE TABLE event_document (
    trxmlid integer NOT NULL,
    userid integer NOT NULL,
    client integer NOT NULL,
    document_name character varying,
    sent_date timestamp without time zone,
    received_date timestamp without time zone,
    ext_candidate_id character varying,
    oportunity_id character varying,
    candidate_name character varying,
    candidate_email character varying,
    candidate_birthdate date,
    unitid integer,
    date_created timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE event_document OWNER TO web;

--
-- Name: trxml_update_seq; Type: SEQUENCE; Schema: public; Owner: web
--

CREATE SEQUENCE trxml_update_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE trxml_update_seq OWNER TO web;

--
-- Name: event_history; Type: TABLE; Schema: public; Owner: web; Tablespace: 
--

CREATE TABLE event_history (
    id integer DEFAULT nextval(('event_history_seq'::text)::regclass) NOT NULL,
    trxmlid integer,
    userid integer,
    event_type character varying,
    event_date timestamp without time zone,
    event_content character varying,
    event_note character varying,
    update_seq bigint DEFAULT nextval('trxml_update_seq'::regclass)
);


ALTER TABLE event_history OWNER TO web;

--
-- Name: event_history_seq; Type: SEQUENCE; Schema: public; Owner: web
--

CREATE SEQUENCE event_history_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE event_history_seq OWNER TO web;

SET default_with_oids = true;

--
-- Name: import_info; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE import_info (
    id integer DEFAULT nextval(('import_info_seq'::text)::regclass) NOT NULL,
    client integer NOT NULL,
    import_activated character(1) DEFAULT 'f'::bpchar,
    import_type character varying,
    import_file character varying,
    import_status character varying DEFAULT 'DONE'::character varying,
    update_period_start timestamp without time zone,
    update_period_end timestamp without time zone
);


ALTER TABLE import_info OWNER TO postgres;

--
-- Name: import_info_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE import_info_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE import_info_seq OWNER TO postgres;

--
-- Name: mailbox; Type: TABLE; Schema: public; Owner: web; Tablespace: 
--

CREATE TABLE mailbox (
    id integer DEFAULT nextval(('mailbox_seq'::text)::regclass) NOT NULL,
    name character varying,
    enabled character(1),
    keepmailonserver character(1) DEFAULT 'f'::bpchar,
    skipdeduplication character(1) DEFAULT 'f'::bpchar,
    skipstore character(1) DEFAULT 't'::bpchar,
    dovalidation character(1) DEFAULT 'f'::bpchar,
    server character varying,
    password character varying,
    protocol character varying,
    account character varying,
    pop3port integer DEFAULT 110 NOT NULL,
    email_address character varying,
    divisionname character varying,
    secretarymail character varying,
    sourceboxuser integer NOT NULL,
    acceptedsenderlist character varying,
    archivemailbox character varying,
    work_directory_path character varying,
    output_directory_path character varying,
    sweeping character(1),
    mbx_comment character varying,
    last_swept timestamp without time zone,
    txtoradress character varying,
    dir_tstamp timestamp without time zone,
    mbx_mode integer DEFAULT 1 NOT NULL,
    status integer DEFAULT 0,
    status_flags character varying,
    status_tstamp timestamp without time zone DEFAULT now(),
    reset character(1) DEFAULT 'f'::bpchar NOT NULL,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    date_modified timestamp without time zone,
    CONSTRAINT mailbox_protocol CHECK (((protocol)::text = ANY ((ARRAY['pop3'::character varying, 'pop3s'::character varying])::text[])))
);


ALTER TABLE mailbox OWNER TO web;

--
-- Name: mailbox_seq; Type: SEQUENCE; Schema: public; Owner: web
--

CREATE SEQUENCE mailbox_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mailbox_seq OWNER TO web;

--
-- Name: processing_unit; Type: TABLE; Schema: public; Owner: web; Tablespace: 
--

CREATE TABLE processing_unit (
    id integer NOT NULL,
    pu_date date DEFAULT ('now'::text)::date NOT NULL,
    "time" time(0) without time zone DEFAULT ('now'::text)::time with time zone NOT NULL,
    client integer NOT NULL,
    username character varying NOT NULL,
    documentname character varying,
    test character(1),
    branch character varying,
    cancel character(1)
);


ALTER TABLE processing_unit OWNER TO web;

--
-- Name: processing_unit_id_seq; Type: SEQUENCE; Schema: public; Owner: web
--

CREATE SEQUENCE processing_unit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE processing_unit_id_seq OWNER TO web;

--
-- Name: processing_unit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: web
--

ALTER SEQUENCE processing_unit_id_seq OWNED BY processing_unit.id;


--
-- Name: product; Type: TABLE; Schema: public; Owner: web; Tablespace: 
--

CREATE TABLE product (
    id integer DEFAULT nextval(('product_id_seq'::text)::regclass) NOT NULL,
    name character varying,
    screenname character varying,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    date_modified timestamp without time zone
);


ALTER TABLE product OWNER TO web;

--
-- Name: product_id_seq; Type: SEQUENCE; Schema: public; Owner: web
--

CREATE SEQUENCE product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_id_seq OWNER TO web;

--
-- Name: role; Type: TABLE; Schema: public; Owner: web; Tablespace: 
--

CREATE TABLE role (
    id integer DEFAULT nextval(('role_id_seq'::text)::regclass) NOT NULL,
    rolename character varying,
    userid integer,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    date_modified timestamp without time zone
);


ALTER TABLE role OWNER TO web;

--
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: web
--

CREATE SEQUENCE role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE role_id_seq OWNER TO web;

--
-- Name: routing; Type: TABLE; Schema: public; Owner: web; Tablespace: 
--

CREATE TABLE routing (
    id integer DEFAULT nextval(('routing_id_seq'::text)::regclass) NOT NULL,
    task character varying NOT NULL,
    address character varying NOT NULL,
    type character varying,
    target character varying,
    client integer NOT NULL,
    lang character varying NOT NULL,
    enabled character(1) NOT NULL,
    is_default character(1) NOT NULL,
    extra_textractor_parameters character varying,
    step integer DEFAULT 1 NOT NULL,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    date_modified timestamp without time zone,
    code_tables character(1) DEFAULT 'f'::bpchar NOT NULL
);


ALTER TABLE routing OWNER TO web;

--
-- Name: routing_id_seq; Type: SEQUENCE; Schema: public; Owner: web
--

CREATE SEQUENCE routing_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE routing_id_seq OWNER TO web;

SET default_with_oids = false;

--
-- Name: status; Type: TABLE; Schema: public; Owner: web; Tablespace: 
--

CREATE TABLE status (
    name character varying NOT NULL,
    status character varying,
    start_date timestamp without time zone,
    last_email_date timestamp without time zone
);


ALTER TABLE status OWNER TO web;

SET default_with_oids = true;

--
-- Name: trxml_attachment; Type: TABLE; Schema: public; Owner: web; Tablespace: 
--

CREATE TABLE trxml_attachment (
    id integer DEFAULT nextval(('trxml_attachment_seq'::text)::regclass) NOT NULL,
    trxml_object integer NOT NULL,
    attachmentfilename character varying NOT NULL,
    attachmentfiledisplayname character varying NOT NULL,
    attachmentcontenttype character varying NOT NULL,
    date_created date DEFAULT ('now'::text)::date NOT NULL,
    date_modified date,
    attachmentlanguage character varying,
    attachmentcharset character varying
);


ALTER TABLE trxml_attachment OWNER TO web;

--
-- Name: trxml_attachment_seq; Type: SEQUENCE; Schema: public; Owner: web
--

CREATE SEQUENCE trxml_attachment_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE trxml_attachment_seq OWNER TO web;

--
-- Name: trxml_object; Type: TABLE; Schema: public; Owner: web; Tablespace: 
--

CREATE TABLE trxml_object (
    id integer DEFAULT nextval(('trxml_object_seq'::text)::regclass) NOT NULL,
    userid integer NOT NULL,
    client integer NOT NULL,
    document_name character varying,
    original_document text,
    trxml text,
    insert_state integer NOT NULL,
    division character varying,
    sent_date timestamp without time zone,
    received_date timestamp without time zone,
    ext_candidate_id character varying,
    oportunity_id character varying,
    candidate_name character varying,
    candidate_email character varying,
    candidate_birthdate date,
    candidate_first_name character varying,
    candidate_last_name character varying,
    candidate_middle_name character varying,
    candidate_initials character varying,
    candidate_photo character varying,
    candidate_city character varying,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    date_modified timestamp without time zone,
    store_file character varying,
    store_path character varying,
    content_type character varying,
    dedup_style character varying,
    editing character varying,
    locked character varying,
    attachment_id integer,
    email_sent character(6),
    distance integer,
    years_of_experience integer,
    editing_user integer,
    edited_date timestamp without time zone,
    custom_tstamp_1 timestamp without time zone,
    custom_int_1 integer,
    custom_string_1 character varying,
    custom_string_2 character varying,
    custom_string_3 character varying,
    custom_string_4 character varying,
    custom_string_5 character varying,
    custom_string_6 character varying,
    custom_string_7 character varying,
    custom_string_8 character varying,
    custom_string_9 character varying,
    custom_int_2 integer,
    custom_int_3 integer,
    custom_int_4 integer,
    custom_tstamp_2 timestamp without time zone,
    touch_count integer,
    custom_boolean_1 character(1),
    custom_boolean_2 character(1),
    custom_boolean_3 character(1),
    custom_boolean_4 character(1),
    custom_boolean_5 character(1),
    custom_boolean_6 character(1),
    update_seq bigint DEFAULT nextval('trxml_update_seq'::regclass)
);


ALTER TABLE trxml_object OWNER TO web;

--
-- Name: trxml_object_seq; Type: SEQUENCE; Schema: public; Owner: web
--

CREATE SEQUENCE trxml_object_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE trxml_object_seq OWNER TO web;

--
-- Name: users; Type: TABLE; Schema: public; Owner: web; Tablespace: 
--

CREATE TABLE users (
    id integer DEFAULT nextval(('users_id_seq'::text)::regclass) NOT NULL,
    client integer,
    enabled character(1) DEFAULT 't'::bpchar,
    valid_until date,
    name character varying,
    password character varying,
    email character varying,
    test character(1) DEFAULT 'f'::bpchar,
    fullname character varying,
    branch character varying,
    custom_int_1 integer,
    custom_int_2 integer,
    custom_int_3 integer,
    custom_int_4 integer,
    custom_tstamp_1 timestamp without time zone,
    custom_tstamp_2 timestamp without time zone,
    custom_string_1 character varying,
    custom_string_2 character varying,
    custom_string_3 character varying,
    custom_string_4 character varying,
    custom_string_5 character varying,
    custom_string_6 character varying,
    custom_string_7 character varying,
    custom_string_8 character varying,
    custom_string_9 character varying,
    custom_string_10 character varying,
    custom_string_11 character varying,
    custom_string_12 character varying,
    custom_string_13 character varying,
    custom_string_14 character varying,
    custom_string_15 character varying,
    custom_string_16 character varying,
    custom_string_17 character varying,
    custom_string_18 character varying,
    custom_string_19 character varying,
    custom_string_20 character varying,
    custom_boolean_1 character(1),
    custom_boolean_2 character(1),
    custom_boolean_3 character(1),
    custom_boolean_4 character(1),
    custom_boolean_5 character(1),
    custom_boolean_6 character(1),
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    date_modified timestamp without time zone,
    monster_cat character varying,
    external_user_id character varying,
    auto_created character(1) DEFAULT 'f'::bpchar,
    custom_product character varying,
    custom_target character varying,
    custom_search_roles_ro character varying,
    document_sharing_ro character varying,
    widget_token character varying,
    widget_domain character varying
);


ALTER TABLE users OWNER TO web;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: web
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO web;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: web
--

ALTER TABLE ONLY codelist_def ALTER COLUMN id SET DEFAULT nextval('codelist_def_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: web
--

ALTER TABLE ONLY processing_unit ALTER COLUMN id SET DEFAULT nextval('processing_unit_id_seq'::regclass);


--
-- Data for Name: client; Type: TABLE DATA; Schema: public; Owner: web
--

COPY client (id, enabled, valid_until, name, credit_limit, product, account, fourweekperiod, savepath, has_custom_save_path, model, start_date, lang, interface_lang, external_repository, external_repository_activated, external_account_id, persisting, cassandra_enabled, saml_idp, saml_domain_mapping, parent, custom_string_1, custom_string_2, custom_string_3, custom_string_4, custom_string_5, custom_string_6, custom_string_7, custom_string_8, custom_string_9, custom_string_10, custom_string_11, custom_string_12, custom_string_13, custom_string_14, custom_string_15, custom_string_16, custom_string_17, custom_string_18, custom_string_19, custom_string_20, custom_int_1, custom_int_2, custom_int_3, custom_int_4, custom_tstamp_1, custom_tstamp_2, custom_boolean_1, custom_boolean_2, custom_boolean_3, custom_boolean_4, custom_boolean_5, custom_boolean_6, linkedin_email, linkedin_password, linkedin_access_token, linkedin_secret_token, linkedin_activated, search_activated, search_config, search_config_password, search_url, indexing_url, indexing_config, indexing_config_password, last_indexing_update, date_created, date_modified, bullhorn_subscription_id, bullhorn_last_request_id, custom_search_roles_ro, document_sharing_ro) FROM stdin;
1	t	\N	Textkernel BV	\N	6	textkernel	f	/tmp/textkernel	t	TextkernelAdministrator.xml	\N	english	english	\N	\N	\N	t	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	2016-02-22 16:54:54.165892	\N	\N	\N	\N	\N
2	t	\N	erecruit CV Processor	0	6	erecruit_match_cv	f	\N	f	MatchCVModelEN.xml	\N	\N	english		f	\N	f	f			\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N					f	f							\N	2016-02-22 22:00:35.544506	2016-02-22 22:48:23.720349	\N	\N		
3	t	\N	erecruit Vacancy Processor	0	6	erecruit_match_vac	f	erecruit_match_vac	f	ERecruitVac.xml	\N	\N	english		f	\N	f	f			\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N					f	f							\N	2016-02-22 22:38:24.20748	2016-02-22 22:49:23.082122	\N	\N		
\.


--
-- Name: client_id_seq; Type: SEQUENCE SET; Schema: public; Owner: web
--

SELECT pg_catalog.setval('client_id_seq', 3, true);


--
-- Data for Name: codelist; Type: TABLE DATA; Schema: public; Owner: web
--

COPY codelist (id, client, numcode, alphacode, fieldname, version, versiondate, description, codeid, date_created, date_modified) FROM stdin;
\.


--
-- Data for Name: codelist_def; Type: TABLE DATA; Schema: public; Owner: web
--

COPY codelist_def (id, client, fieldname, version, versiondate, current_ind, versiontime, date_created, date_modified) FROM stdin;
\.


--
-- Name: codelist_def_id_seq; Type: SEQUENCE SET; Schema: public; Owner: web
--

SELECT pg_catalog.setval('codelist_def_id_seq', 1, false);


--
-- Name: codelist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: web
--

SELECT pg_catalog.setval('codelist_id_seq', 1, false);


--
-- Data for Name: codeproperty; Type: TABLE DATA; Schema: public; Owner: web
--

COPY codeproperty (id, codelistid, propertyname, propertyvalue, date_created, date_modified) FROM stdin;
\.


--
-- Name: codeproperty_id_seq; Type: SEQUENCE SET; Schema: public; Owner: web
--

SELECT pg_catalog.setval('codeproperty_id_seq', 1, false);


--
-- Data for Name: event_document; Type: TABLE DATA; Schema: public; Owner: web
--

COPY event_document (trxmlid, userid, client, document_name, sent_date, received_date, ext_candidate_id, oportunity_id, candidate_name, candidate_email, candidate_birthdate, unitid, date_created) FROM stdin;
1	2	2	WayneBlair.pdf	\N	2016-02-22 22:35:57.236		\N	Blair	wblair0@gmail.com	\N	1	2016-02-22 22:35:57.236
\.


--
-- Data for Name: event_history; Type: TABLE DATA; Schema: public; Owner: web
--

COPY event_history (id, trxmlid, userid, event_type, event_date, event_content, event_note, update_seq) FROM stdin;
1	\N	1	login	2016-02-22 21:45:29.022	50.169.84.120/50.169.84.120	\N	1
2	\N	1	login	2016-02-22 21:58:55.784	50.169.84.120/50.169.84.120	\N	2
3	\N	1	logout	2016-02-22 22:33:58.715	\N	\N	3
4	\N	2	login	2016-02-22 22:35:02.567	50.169.84.120/50.169.84.120	\N	4
5	1	2	create	2016-02-22 22:35:57.331	/tmp/erecruit_match_cverecruit--33-1456180550085.pdf	\N	6
6	1	2	startEditing	2016-02-22 22:35:57.358	5EE25BE0C3E3E3B534280C04D4775B87	\N	7
7	1	\N	stopEditing	2016-02-22 22:37:18.681	5EE25BE0C3E3E3B534280C04D4775B87	\N	14
8	1	\N	delete	2016-02-22 22:37:18.746	\N	non persisting	15
9	\N	2	logout	2016-02-22 22:37:18.759	\N	\N	16
10	\N	1	login	2016-02-22 22:37:30.744	50.169.84.120/50.169.84.120	\N	17
\.


--
-- Name: event_history_seq; Type: SEQUENCE SET; Schema: public; Owner: web
--

SELECT pg_catalog.setval('event_history_seq', 10, true);


--
-- Data for Name: import_info; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY import_info (id, client, import_activated, import_type, import_file, import_status, update_period_start, update_period_end) FROM stdin;
\.


--
-- Name: import_info_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('import_info_seq', 1, false);


--
-- Data for Name: mailbox; Type: TABLE DATA; Schema: public; Owner: web
--

COPY mailbox (id, name, enabled, keepmailonserver, skipdeduplication, skipstore, dovalidation, server, password, protocol, account, pop3port, email_address, divisionname, secretarymail, sourceboxuser, acceptedsenderlist, archivemailbox, work_directory_path, output_directory_path, sweeping, mbx_comment, last_swept, txtoradress, dir_tstamp, mbx_mode, status, status_flags, status_tstamp, reset, date_created, date_modified) FROM stdin;
\.


--
-- Name: mailbox_seq; Type: SEQUENCE SET; Schema: public; Owner: web
--

SELECT pg_catalog.setval('mailbox_seq', 1, false);


--
-- Data for Name: processing_unit; Type: TABLE DATA; Schema: public; Owner: web
--

COPY processing_unit (id, pu_date, "time", client, username, documentname, test, branch, cancel) FROM stdin;
1	2016-02-22	22:35:57	2	erecruit	WayneBlair.pdf	f		f
\.


--
-- Name: processing_unit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: web
--

SELECT pg_catalog.setval('processing_unit_id_seq', 1, true);


--
-- Data for Name: product; Type: TABLE DATA; Schema: public; Owner: web
--

COPY product (id, name, screenname, date_created, date_modified) FROM stdin;
1	bullhorn	Bullhorn	2016-02-22 16:54:54.165892	\N
2	foederis	Foederis	2016-02-22 16:54:54.165892	\N
3	carerixnew	Carerix	2016-02-22 16:54:54.165892	\N
4	connexys	Connexys	2016-02-22 16:54:54.165892	\N
5	demo	Demo	2016-02-22 16:54:54.165892	\N
6	getprofile	Get Profile	2016-02-22 16:54:54.165892	\N
7	icams	iCams	2016-02-22 16:54:54.165892	\N
8	max	MAX	2016-02-22 16:54:54.165892	\N
9	maxstore	MAXStore	2016-02-22 16:54:54.165892	\N
10	multipartxml	YER	2016-02-22 16:54:54.165892	\N
11	novesta	Embedded	2016-02-22 16:54:54.165892	\N
12	profile2000	Profile 2000	2016-02-22 16:54:54.165892	\N
13	search	Search!	2016-02-22 16:54:54.165892	\N
14	sftp	SFTP	2016-02-22 16:54:54.165892	\N
15	simplexml	Simple XML Post	2016-02-22 16:54:54.165892	\N
16	soap	Soap	2016-02-22 16:54:54.165892	\N
17	taleo	Taleo	2016-02-22 16:54:54.165892	\N
18	trxmlid	Trxmlid	2016-02-22 16:54:54.165892	\N
19	vitaeflex	vitaeflex	2016-02-22 16:54:54.165892	\N
20	peakitsf	PeakIT SF	2016-02-22 16:54:54.165892	\N
21	postprofile	Post Profile Redirect	2016-02-22 16:54:54.165892	\N
22	umantis	Umantis	2016-02-22 16:54:54.165892	\N
23	rest	REST	2016-02-22 16:54:54.165892	\N
24	formpost	Post Form	2016-02-22 16:54:54.165892	\N
25	multipartformpost	Post Multi-Part Form	2016-02-22 16:54:54.165892	\N
\.


--
-- Name: product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: web
--

SELECT pg_catalog.setval('product_id_seq', 25, true);


--
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: web
--

COPY role (id, rolename, userid, date_created, date_modified) FROM stdin;
1	admin	1	2016-02-22 16:54:54.165892	\N
\.


--
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: web
--

SELECT pg_catalog.setval('role_id_seq', 1, true);


--
-- Data for Name: routing; Type: TABLE DATA; Schema: public; Owner: web
--

COPY routing (id, task, address, type, target, client, lang, enabled, is_default, extra_textractor_parameters, step, date_created, date_modified, code_tables) FROM stdin;
51	preprocessing	localhost:60000		\N	2	none	t	f	DP_HTML_PRESENTATION=1	1	2016-02-22 22:48:23.805075	\N	f
52	processing	localhost:50200		\N	2	english	t	t	DP_DEFAULT_COUNTRY=US	1	2016-02-22 22:48:23.814289	\N	f
53	normalization	localhost:48000		\N	2	english	t	t		1	2016-02-22 22:48:23.824196	\N	t
54	normalization	localhost:55700		\N	2	english	t	t		2	2016-02-22 22:48:23.833366	\N	f
55	templating	localhost:48700		match_tkdemo_cv_xml_en	2	english	t	t		1	2016-02-22 22:48:23.842939	\N	f
61	preprocessing	localhost:23000		\N	3	none	t	f		1	2016-02-22 22:49:23.342127	\N	f
62	processing	localhost:20800		\N	3	english	t	t		1	2016-02-22 22:49:23.351785	\N	f
63	processing	localhost:21400		\N	3	english	t	t		2	2016-02-22 22:49:23.361823	\N	f
64	normalization	localhost:55600		\N	3	english	t	t		1	2016-02-22 22:49:23.37094	\N	t
65	templating	localhost:48700		match_vac_xml_en	3	english	t	t		1	2016-02-22 22:49:23.39702	\N	f
\.


--
-- Name: routing_id_seq; Type: SEQUENCE SET; Schema: public; Owner: web
--

SELECT pg_catalog.setval('routing_id_seq', 65, true);


--
-- Data for Name: status; Type: TABLE DATA; Schema: public; Owner: web
--

COPY status (name, status, start_date, last_email_date) FROM stdin;
\.


--
-- Data for Name: trxml_attachment; Type: TABLE DATA; Schema: public; Owner: web
--

COPY trxml_attachment (id, trxml_object, attachmentfilename, attachmentfiledisplayname, attachmentcontenttype, date_created, date_modified, attachmentlanguage, attachmentcharset) FROM stdin;
\.


--
-- Name: trxml_attachment_seq; Type: SEQUENCE SET; Schema: public; Owner: web
--

SELECT pg_catalog.setval('trxml_attachment_seq', 1, true);


--
-- Data for Name: trxml_object; Type: TABLE DATA; Schema: public; Owner: web
--

COPY trxml_object (id, userid, client, document_name, original_document, trxml, insert_state, division, sent_date, received_date, ext_candidate_id, oportunity_id, candidate_name, candidate_email, candidate_birthdate, candidate_first_name, candidate_last_name, candidate_middle_name, candidate_initials, candidate_photo, candidate_city, date_created, date_modified, store_file, store_path, content_type, dedup_style, editing, locked, attachment_id, email_sent, distance, years_of_experience, editing_user, edited_date, custom_tstamp_1, custom_int_1, custom_string_1, custom_string_2, custom_string_3, custom_string_4, custom_string_5, custom_string_6, custom_string_7, custom_string_8, custom_string_9, custom_int_2, custom_int_3, custom_int_4, custom_tstamp_2, touch_count, custom_boolean_1, custom_boolean_2, custom_boolean_3, custom_boolean_4, custom_boolean_5, custom_boolean_6, update_seq) FROM stdin;
\.


--
-- Name: trxml_object_seq; Type: SEQUENCE SET; Schema: public; Owner: web
--

SELECT pg_catalog.setval('trxml_object_seq', 1, true);


--
-- Name: trxml_update_seq; Type: SEQUENCE SET; Schema: public; Owner: web
--

SELECT pg_catalog.setval('trxml_update_seq', 17, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: web
--

COPY users (id, client, enabled, valid_until, name, password, email, test, fullname, branch, custom_int_1, custom_int_2, custom_int_3, custom_int_4, custom_tstamp_1, custom_tstamp_2, custom_string_1, custom_string_2, custom_string_3, custom_string_4, custom_string_5, custom_string_6, custom_string_7, custom_string_8, custom_string_9, custom_string_10, custom_string_11, custom_string_12, custom_string_13, custom_string_14, custom_string_15, custom_string_16, custom_string_17, custom_string_18, custom_string_19, custom_string_20, custom_boolean_1, custom_boolean_2, custom_boolean_3, custom_boolean_4, custom_boolean_5, custom_boolean_6, date_created, date_modified, monster_cat, external_user_id, auto_created, custom_product, custom_target, custom_search_roles_ro, document_sharing_ro, widget_token, widget_domain) FROM stdin;
1	1	t	\N	admin	21232f297a57a5a743894a0e4a801fc3	service@textkernel.nl	f	Administrator	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2016-02-22 16:54:54.165892	\N	\N	\N	f	\N	\N	\N	\N	\N	\N
2	2	t	\N	erecruit	374d643b6581061c6714d9901251a29d		f	erecruit		\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2016-02-22 22:06:10.449048	\N		\N	f					\N	\N
3	2	t	\N	query	f8c9d02aee9a77109af609ab8d1ad6ac		f	query		\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2016-02-22 22:06:42.953499	2016-02-22 22:07:30.295658		\N	f		match_cv_query			\N	
4	3	t	\N	erecruit	374d643b6581061c6714d9901251a29d		f	erecruit		\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2016-02-22 22:43:18.675071	\N		\N	f					\N	\N
5	3	t	\N	query	374d643b6581061c6714d9901251a29d		f	query		\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2016-02-22 22:45:38.73344	\N		\N	f		match_vac_query			\N	\N
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: web
--

SELECT pg_catalog.setval('users_id_seq', 5, true);


--
-- Name: client_ext_ukey; Type: CONSTRAINT; Schema: public; Owner: web; Tablespace: 
--

ALTER TABLE ONLY client
    ADD CONSTRAINT client_ext_ukey UNIQUE (external_account_id);


--
-- Name: client_pkey; Type: CONSTRAINT; Schema: public; Owner: web; Tablespace: 
--

ALTER TABLE ONLY client
    ADD CONSTRAINT client_pkey PRIMARY KEY (id);


--
-- Name: client_ukey; Type: CONSTRAINT; Schema: public; Owner: web; Tablespace: 
--

ALTER TABLE ONLY client
    ADD CONSTRAINT client_ukey UNIQUE (account);


--
-- Name: codelist_def_pkey; Type: CONSTRAINT; Schema: public; Owner: web; Tablespace: 
--

ALTER TABLE ONLY codelist_def
    ADD CONSTRAINT codelist_def_pkey PRIMARY KEY (id);


--
-- Name: codelist_def_ukey; Type: CONSTRAINT; Schema: public; Owner: web; Tablespace: 
--

ALTER TABLE ONLY codelist_def
    ADD CONSTRAINT codelist_def_ukey UNIQUE (client, fieldname);


--
-- Name: codelist_pkey; Type: CONSTRAINT; Schema: public; Owner: web; Tablespace: 
--

ALTER TABLE ONLY codelist
    ADD CONSTRAINT codelist_pkey PRIMARY KEY (id);


--
-- Name: codelist_ukey; Type: CONSTRAINT; Schema: public; Owner: web; Tablespace: 
--

ALTER TABLE ONLY codelist
    ADD CONSTRAINT codelist_ukey UNIQUE (client, fieldname, codeid);


--
-- Name: codeproperty_pkey; Type: CONSTRAINT; Schema: public; Owner: web; Tablespace: 
--

ALTER TABLE ONLY codeproperty
    ADD CONSTRAINT codeproperty_pkey PRIMARY KEY (id);


--
-- Name: codeproperty_ukey; Type: CONSTRAINT; Schema: public; Owner: web; Tablespace: 
--

ALTER TABLE ONLY codeproperty
    ADD CONSTRAINT codeproperty_ukey UNIQUE (codelistid, propertyname);


--
-- Name: event_document_pkey; Type: CONSTRAINT; Schema: public; Owner: web; Tablespace: 
--

ALTER TABLE ONLY event_document
    ADD CONSTRAINT event_document_pkey PRIMARY KEY (trxmlid);


--
-- Name: event_history_pkey; Type: CONSTRAINT; Schema: public; Owner: web; Tablespace: 
--

ALTER TABLE ONLY event_history
    ADD CONSTRAINT event_history_pkey PRIMARY KEY (id);


--
-- Name: import_info_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY import_info
    ADD CONSTRAINT import_info_pkey PRIMARY KEY (id);


--
-- Name: mailbox_pkey; Type: CONSTRAINT; Schema: public; Owner: web; Tablespace: 
--

ALTER TABLE ONLY mailbox
    ADD CONSTRAINT mailbox_pkey PRIMARY KEY (id);


--
-- Name: mailbox_ukey; Type: CONSTRAINT; Schema: public; Owner: web; Tablespace: 
--

ALTER TABLE ONLY mailbox
    ADD CONSTRAINT mailbox_ukey UNIQUE (name);


--
-- Name: processing_unit_pkey; Type: CONSTRAINT; Schema: public; Owner: web; Tablespace: 
--

ALTER TABLE ONLY processing_unit
    ADD CONSTRAINT processing_unit_pkey PRIMARY KEY (id);


--
-- Name: product_pkey; Type: CONSTRAINT; Schema: public; Owner: web; Tablespace: 
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);


--
-- Name: product_ukey; Type: CONSTRAINT; Schema: public; Owner: web; Tablespace: 
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_ukey UNIQUE (name);


--
-- Name: role_pkey; Type: CONSTRAINT; Schema: public; Owner: web; Tablespace: 
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- Name: role_ukey; Type: CONSTRAINT; Schema: public; Owner: web; Tablespace: 
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_ukey UNIQUE (userid, rolename);


--
-- Name: routing_pkey; Type: CONSTRAINT; Schema: public; Owner: web; Tablespace: 
--

ALTER TABLE ONLY routing
    ADD CONSTRAINT routing_pkey PRIMARY KEY (id);


--
-- Name: status_pkey; Type: CONSTRAINT; Schema: public; Owner: web; Tablespace: 
--

ALTER TABLE ONLY status
    ADD CONSTRAINT status_pkey PRIMARY KEY (name);


--
-- Name: trxml_attachment_pkey; Type: CONSTRAINT; Schema: public; Owner: web; Tablespace: 
--

ALTER TABLE ONLY trxml_attachment
    ADD CONSTRAINT trxml_attachment_pkey PRIMARY KEY (id);


--
-- Name: trxml_object_pkey; Type: CONSTRAINT; Schema: public; Owner: web; Tablespace: 
--

ALTER TABLE ONLY trxml_object
    ADD CONSTRAINT trxml_object_pkey PRIMARY KEY (id);


--
-- Name: users_ext_ukey; Type: CONSTRAINT; Schema: public; Owner: web; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_ext_ukey UNIQUE (client, external_user_id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: web; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users_ukey; Type: CONSTRAINT; Schema: public; Owner: web; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_ukey UNIQUE (client, name);


--
-- Name: users_widget_token_ukey; Type: CONSTRAINT; Schema: public; Owner: web; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_widget_token_ukey UNIQUE (widget_token);


--
-- Name: codelist_ac_codeid; Type: INDEX; Schema: public; Owner: web; Tablespace: 
--

CREATE INDEX codelist_ac_codeid ON codelist USING btree (client, fieldname, codeid);


--
-- Name: codelist_ac_description; Type: INDEX; Schema: public; Owner: web; Tablespace: 
--

CREATE INDEX codelist_ac_description ON codelist USING btree (client, fieldname, description);


--
-- Name: codelist_codeid; Type: INDEX; Schema: public; Owner: web; Tablespace: 
--

CREATE INDEX codelist_codeid ON codelist USING btree (codeid);


--
-- Name: codelist_description; Type: INDEX; Schema: public; Owner: web; Tablespace: 
--

CREATE INDEX codelist_description ON codelist USING btree (description);


--
-- Name: codelist_index; Type: INDEX; Schema: public; Owner: web; Tablespace: 
--

CREATE INDEX codelist_index ON codelist USING btree (client, fieldname);


--
-- Name: codeproperty_codelistid; Type: INDEX; Schema: public; Owner: web; Tablespace: 
--

CREATE INDEX codeproperty_codelistid ON codeproperty USING btree (codelistid);


--
-- Name: codeproperty_index; Type: INDEX; Schema: public; Owner: web; Tablespace: 
--

CREATE INDEX codeproperty_index ON codeproperty USING btree (propertyname, propertyvalue);


--
-- Name: eh_idx_event_type; Type: INDEX; Schema: public; Owner: web; Tablespace: 
--

CREATE INDEX eh_idx_event_type ON event_history USING btree (event_type);


--
-- Name: eh_idx_update_seq; Type: INDEX; Schema: public; Owner: web; Tablespace: 
--

CREATE INDEX eh_idx_update_seq ON event_history USING btree (update_seq);


--
-- Name: eh_idx_userid; Type: INDEX; Schema: public; Owner: web; Tablespace: 
--

CREATE INDEX eh_idx_userid ON event_history USING btree (userid);


--
-- Name: pu_idx_client; Type: INDEX; Schema: public; Owner: web; Tablespace: 
--

CREATE INDEX pu_idx_client ON processing_unit USING btree (client);


--
-- Name: ta_idx_to; Type: INDEX; Schema: public; Owner: web; Tablespace: 
--

CREATE INDEX ta_idx_to ON trxml_attachment USING btree (trxml_object);


--
-- Name: to_idx_client; Type: INDEX; Schema: public; Owner: web; Tablespace: 
--

CREATE INDEX to_idx_client ON trxml_object USING btree (client);


--
-- Name: to_idx_editing; Type: INDEX; Schema: public; Owner: web; Tablespace: 
--

CREATE INDEX to_idx_editing ON trxml_object USING btree (editing);


--
-- Name: to_idx_ext_cand_id; Type: INDEX; Schema: public; Owner: web; Tablespace: 
--

CREATE INDEX to_idx_ext_cand_id ON trxml_object USING btree (ext_candidate_id);


--
-- Name: to_idx_locked; Type: INDEX; Schema: public; Owner: web; Tablespace: 
--

CREATE INDEX to_idx_locked ON trxml_object USING btree (locked);


--
-- Name: to_idx_moddate; Type: INDEX; Schema: public; Owner: web; Tablespace: 
--

CREATE INDEX to_idx_moddate ON trxml_object USING btree (date_modified);


--
-- Name: to_idx_received_date; Type: INDEX; Schema: public; Owner: web; Tablespace: 
--

CREATE INDEX to_idx_received_date ON trxml_object USING btree (received_date);


--
-- Name: to_idx_update_seq; Type: INDEX; Schema: public; Owner: web; Tablespace: 
--

CREATE INDEX to_idx_update_seq ON trxml_object USING btree (update_seq);


--
-- Name: to_idx_userid; Type: INDEX; Schema: public; Owner: web; Tablespace: 
--

CREATE INDEX to_idx_userid ON trxml_object USING btree (userid);


--
-- Name: client_bru; Type: TRIGGER; Schema: public; Owner: web
--

CREATE TRIGGER client_bru BEFORE UPDATE ON client FOR EACH ROW EXECUTE PROCEDURE auditing_stamp();


--
-- Name: codelist_bru; Type: TRIGGER; Schema: public; Owner: web
--

CREATE TRIGGER codelist_bru BEFORE UPDATE ON codelist FOR EACH ROW EXECUTE PROCEDURE auditing_stamp();


--
-- Name: codelist_def_bru; Type: TRIGGER; Schema: public; Owner: web
--

CREATE TRIGGER codelist_def_bru BEFORE UPDATE ON codelist_def FOR EACH ROW EXECUTE PROCEDURE auditing_stamp();


--
-- Name: codeproperty_bru; Type: TRIGGER; Schema: public; Owner: web
--

CREATE TRIGGER codeproperty_bru BEFORE UPDATE ON codeproperty FOR EACH ROW EXECUTE PROCEDURE auditing_stamp();


--
-- Name: delete_attachment_on_delete; Type: TRIGGER; Schema: public; Owner: web
--

CREATE TRIGGER delete_attachment_on_delete BEFORE DELETE ON trxml_object FOR EACH ROW EXECUTE PROCEDURE trxml_object_delete_attachment_on_delete();


--
-- Name: product_bru; Type: TRIGGER; Schema: public; Owner: web
--

CREATE TRIGGER product_bru BEFORE UPDATE ON product FOR EACH ROW EXECUTE PROCEDURE auditing_stamp();


--
-- Name: role_bru; Type: TRIGGER; Schema: public; Owner: web
--

CREATE TRIGGER role_bru BEFORE UPDATE ON role FOR EACH ROW EXECUTE PROCEDURE auditing_stamp();


--
-- Name: routing_bru; Type: TRIGGER; Schema: public; Owner: web
--

CREATE TRIGGER routing_bru BEFORE UPDATE ON routing FOR EACH ROW EXECUTE PROCEDURE auditing_stamp();


--
-- Name: trxml_attachment_bru; Type: TRIGGER; Schema: public; Owner: web
--

CREATE TRIGGER trxml_attachment_bru BEFORE UPDATE ON trxml_attachment FOR EACH ROW EXECUTE PROCEDURE auditing_stamp();


--
-- Name: trxml_object_require; Type: TRIGGER; Schema: public; Owner: web
--

CREATE TRIGGER trxml_object_require BEFORE INSERT ON trxml_attachment FOR EACH ROW EXECUTE PROCEDURE trxml_attachment_require_trxml_object();


--
-- Name: users_bru; Type: TRIGGER; Schema: public; Owner: web
--

CREATE TRIGGER users_bru BEFORE UPDATE ON users FOR EACH ROW EXECUTE PROCEDURE auditing_stamp();


--
-- Name: client_parent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: web
--

ALTER TABLE ONLY client
    ADD CONSTRAINT client_parent_fkey FOREIGN KEY (parent) REFERENCES client(id) ON DELETE SET NULL;


--
-- Name: client_product_fkey; Type: FK CONSTRAINT; Schema: public; Owner: web
--

ALTER TABLE ONLY client
    ADD CONSTRAINT client_product_fkey FOREIGN KEY (product) REFERENCES product(id);


--
-- Name: codelist_client_fkey; Type: FK CONSTRAINT; Schema: public; Owner: web
--

ALTER TABLE ONLY codelist
    ADD CONSTRAINT codelist_client_fkey FOREIGN KEY (client) REFERENCES client(id) ON DELETE CASCADE;


--
-- Name: codelist_def_client_fkey; Type: FK CONSTRAINT; Schema: public; Owner: web
--

ALTER TABLE ONLY codelist_def
    ADD CONSTRAINT codelist_def_client_fkey FOREIGN KEY (client) REFERENCES client(id) ON DELETE CASCADE;


--
-- Name: codeproperty_codelist_fkey; Type: FK CONSTRAINT; Schema: public; Owner: web
--

ALTER TABLE ONLY codeproperty
    ADD CONSTRAINT codeproperty_codelist_fkey FOREIGN KEY (codelistid) REFERENCES codelist(id) ON DELETE CASCADE;


--
-- Name: event_document_client_fkey; Type: FK CONSTRAINT; Schema: public; Owner: web
--

ALTER TABLE ONLY event_document
    ADD CONSTRAINT event_document_client_fkey FOREIGN KEY (client) REFERENCES client(id) ON DELETE CASCADE;


--
-- Name: event_document_userid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: web
--

ALTER TABLE ONLY event_document
    ADD CONSTRAINT event_document_userid_fkey FOREIGN KEY (userid) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: event_history_trxmlid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: web
--

ALTER TABLE ONLY event_history
    ADD CONSTRAINT event_history_trxmlid_fkey FOREIGN KEY (trxmlid) REFERENCES event_document(trxmlid) ON DELETE CASCADE;


--
-- Name: event_history_userid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: web
--

ALTER TABLE ONLY event_history
    ADD CONSTRAINT event_history_userid_fkey FOREIGN KEY (userid) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: import_info_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY import_info
    ADD CONSTRAINT import_info_fkey FOREIGN KEY (client) REFERENCES client(id) ON DELETE CASCADE;


--
-- Name: mailbox_sourceboxuser_fkey; Type: FK CONSTRAINT; Schema: public; Owner: web
--

ALTER TABLE ONLY mailbox
    ADD CONSTRAINT mailbox_sourceboxuser_fkey FOREIGN KEY (sourceboxuser) REFERENCES users(id);


--
-- Name: role_users_fkey; Type: FK CONSTRAINT; Schema: public; Owner: web
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_users_fkey FOREIGN KEY (userid) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: routing_client_fkey; Type: FK CONSTRAINT; Schema: public; Owner: web
--

ALTER TABLE ONLY routing
    ADD CONSTRAINT routing_client_fkey FOREIGN KEY (client) REFERENCES client(id) ON DELETE CASCADE;


--
-- Name: trxml_object_client_fkey; Type: FK CONSTRAINT; Schema: public; Owner: web
--

ALTER TABLE ONLY trxml_object
    ADD CONSTRAINT trxml_object_client_fkey FOREIGN KEY (client) REFERENCES client(id) ON DELETE CASCADE;


--
-- Name: trxml_object_userid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: web
--

ALTER TABLE ONLY trxml_object
    ADD CONSTRAINT trxml_object_userid_fkey FOREIGN KEY (userid) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: users_client_fkey; Type: FK CONSTRAINT; Schema: public; Owner: web
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_client_fkey FOREIGN KEY (client) REFERENCES client(id) ON DELETE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO web;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: client; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON TABLE client FROM PUBLIC;
REVOKE ALL ON TABLE client FROM web;
GRANT ALL ON TABLE client TO web;


--
-- Name: client_id_seq; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON SEQUENCE client_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE client_id_seq FROM web;
GRANT ALL ON SEQUENCE client_id_seq TO web;


--
-- Name: codelist; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON TABLE codelist FROM PUBLIC;
REVOKE ALL ON TABLE codelist FROM web;
GRANT ALL ON TABLE codelist TO web;


--
-- Name: codelist_def; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON TABLE codelist_def FROM PUBLIC;
REVOKE ALL ON TABLE codelist_def FROM web;
GRANT ALL ON TABLE codelist_def TO web;


--
-- Name: codelist_def_id_seq; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON SEQUENCE codelist_def_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE codelist_def_id_seq FROM web;
GRANT ALL ON SEQUENCE codelist_def_id_seq TO web;


--
-- Name: codelist_id_seq; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON SEQUENCE codelist_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE codelist_id_seq FROM web;
GRANT ALL ON SEQUENCE codelist_id_seq TO web;


--
-- Name: codeproperty; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON TABLE codeproperty FROM PUBLIC;
REVOKE ALL ON TABLE codeproperty FROM web;
GRANT ALL ON TABLE codeproperty TO web;


--
-- Name: codeproperty_id_seq; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON SEQUENCE codeproperty_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE codeproperty_id_seq FROM web;
GRANT ALL ON SEQUENCE codeproperty_id_seq TO web;


--
-- Name: event_document; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON TABLE event_document FROM PUBLIC;
REVOKE ALL ON TABLE event_document FROM web;
GRANT ALL ON TABLE event_document TO web;


--
-- Name: trxml_update_seq; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON SEQUENCE trxml_update_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE trxml_update_seq FROM web;
GRANT ALL ON SEQUENCE trxml_update_seq TO web;


--
-- Name: event_history; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON TABLE event_history FROM PUBLIC;
REVOKE ALL ON TABLE event_history FROM web;
GRANT ALL ON TABLE event_history TO web;


--
-- Name: event_history_seq; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON SEQUENCE event_history_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE event_history_seq FROM web;
GRANT ALL ON SEQUENCE event_history_seq TO web;


--
-- Name: import_info; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE import_info FROM PUBLIC;
REVOKE ALL ON TABLE import_info FROM postgres;
GRANT ALL ON TABLE import_info TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE import_info TO web;


--
-- Name: import_info_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE import_info_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE import_info_seq FROM postgres;
GRANT ALL ON SEQUENCE import_info_seq TO postgres;
GRANT ALL ON SEQUENCE import_info_seq TO web;


--
-- Name: mailbox; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON TABLE mailbox FROM PUBLIC;
REVOKE ALL ON TABLE mailbox FROM web;
GRANT ALL ON TABLE mailbox TO web;


--
-- Name: mailbox_seq; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON SEQUENCE mailbox_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE mailbox_seq FROM web;
GRANT ALL ON SEQUENCE mailbox_seq TO web;


--
-- Name: processing_unit; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON TABLE processing_unit FROM PUBLIC;
REVOKE ALL ON TABLE processing_unit FROM web;
GRANT ALL ON TABLE processing_unit TO web;


--
-- Name: processing_unit_id_seq; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON SEQUENCE processing_unit_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE processing_unit_id_seq FROM web;
GRANT ALL ON SEQUENCE processing_unit_id_seq TO web;


--
-- Name: product; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON TABLE product FROM PUBLIC;
REVOKE ALL ON TABLE product FROM web;
GRANT ALL ON TABLE product TO web;


--
-- Name: product_id_seq; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON SEQUENCE product_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE product_id_seq FROM web;
GRANT ALL ON SEQUENCE product_id_seq TO web;


--
-- Name: role; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON TABLE role FROM PUBLIC;
REVOKE ALL ON TABLE role FROM web;
GRANT ALL ON TABLE role TO web;


--
-- Name: role_id_seq; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON SEQUENCE role_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE role_id_seq FROM web;
GRANT ALL ON SEQUENCE role_id_seq TO web;


--
-- Name: routing; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON TABLE routing FROM PUBLIC;
REVOKE ALL ON TABLE routing FROM web;
GRANT ALL ON TABLE routing TO web;


--
-- Name: routing_id_seq; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON SEQUENCE routing_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE routing_id_seq FROM web;
GRANT ALL ON SEQUENCE routing_id_seq TO web;


--
-- Name: status; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON TABLE status FROM PUBLIC;
REVOKE ALL ON TABLE status FROM web;
GRANT ALL ON TABLE status TO web;


--
-- Name: trxml_attachment; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON TABLE trxml_attachment FROM PUBLIC;
REVOKE ALL ON TABLE trxml_attachment FROM web;
GRANT ALL ON TABLE trxml_attachment TO web;


--
-- Name: trxml_attachment_seq; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON SEQUENCE trxml_attachment_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE trxml_attachment_seq FROM web;
GRANT ALL ON SEQUENCE trxml_attachment_seq TO web;


--
-- Name: trxml_object; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON TABLE trxml_object FROM PUBLIC;
REVOKE ALL ON TABLE trxml_object FROM web;
GRANT ALL ON TABLE trxml_object TO web;


--
-- Name: trxml_object_seq; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON SEQUENCE trxml_object_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE trxml_object_seq FROM web;
GRANT ALL ON SEQUENCE trxml_object_seq TO web;


--
-- Name: users; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON TABLE users FROM PUBLIC;
REVOKE ALL ON TABLE users FROM web;
GRANT ALL ON TABLE users TO web;


--
-- Name: users_id_seq; Type: ACL; Schema: public; Owner: web
--

REVOKE ALL ON SEQUENCE users_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE users_id_seq FROM web;
GRANT ALL ON SEQUENCE users_id_seq TO web;


--
-- PostgreSQL database dump complete
--

