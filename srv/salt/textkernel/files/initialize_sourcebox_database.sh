#!/bin/bash

## ########################################## ##
## ########################################## ##
##             ### WARNING ###                ##
##       This file is managed by salt.        ##
##       Local changes will not persist.      ##
##       Any changes from Textkernel will     ##
##       need to be merged in with these      ##
##       erecruit specific details.           ##
## ########################################## ##
## ########################################## ##


SCRIPT_DIR="$(cd $(dirname "$0");pwd)"

# Sourcebox initialization script for Unix-like systems
# check if we have enough arguments
if [ -z "$5" ] ;
then
    echo "Not enough arguments."
    echo
    echo "Usage: $0 [-h HOST] <postgres port> <update | create> <database superuser> <sourcebox database> <sourcebox user>"
    echo
    echo "example for local socket connection: $0 5432 create postgres sourcebox web"
    echo "example for network connection: $0 -h localhost 5432 update postgres sourcebox web"

    exit 1;
fi

#Process the options
while getopts h: opt
do
   case "$opt" in
      h) POSTGRES_HOST=$OPTARG
      	 ;;
   esac
done
shift $(( OPTIND-1 ))

#Process the arguments
DB_ACTION=$2

if [ "${DB_ACTION}" ==  "update" -o "${DB_ACTION}" == "UPDATE" ]
then
	DB_ACTION="update"
elif [ "${DB_ACTION}" ==  "create" -o "${DB_ACTION}" == "CREATE" ]
then
	DB_ACTION="create"
else
	echo "Error: action must be either update or create. Exiting..."
	exit 1
fi

POSTGRES_PORT=$1
POSTGRES_SUPERUSER=$3
DB_NAME=$4
DB_USER=$5

INIT_SCRIPT="$SCRIPT_DIR/initialize_sourcebox_database.sql"
POSTGRES_ENV="--port=${POSTGRES_PORT}"

if [ ! -z "${POSTGRES_HOST}" ]
then
	echo setting host: $POSTGRES_HOST
	POSTGRES_ENV="--host=$POSTGRES_HOST $POSTGRES_ENV"
else
    POSTGRES_HOST="local socket"
fi

SUPER_USER_ENV="${POSTGRES_ENV} --username=${POSTGRES_SUPERUSER}"
USER_ENV="${POSTGRES_ENV} --username=${DB_USER}"
DB_DESC="${POSTGRES_HOST}:${POSTGRES_PORT}"

# check super user connect
if psql ${SUPER_USER_ENV} postgres -c 'select 1' >/dev/null
then
	echo "Connecting to postgres server: ${DB_DESC} as user: ${POSTGRES_SUPERUSER}" 
else
	echo "Error: can't connect to postgres with user ${POSTGRES_SUPERUSER} on ${DB_DESC}, exiting."
	exit 1
fi

# create sourcebox user if necessary
if psql ${POSTGRES_ENV} --username=${DB_USER} postgres -c 'select 1' >/dev/null  
then
	echo "Using sourcebox user: ${DB_USER}"
else
	echo "Cannot connect as user: ${DB_USER}, creating new user..."
	createuser ${SUPER_USER_ENV} --pwprompt --createdb --no-createrole --no-superuser ${DB_USER}
fi

# create sourcebox database if necessary  
if psql ${USER_ENV} ${DB_NAME} -c 'select 1' > /dev/null
then
	echo "FOUND sourcebox database: ${DB_NAME}"
	if [ ${DB_ACTION} == "update" ]
	then
		echo "Action UPDATE specified - database ${DB_NAME} will be updated."
	else
		echo "Action CREATE specified with EXISTING database - no further actions will happen. Exiting..."
		exit 1
	fi
else
	echo "Cannot connect as user: ${DB_USER} to database: ${DB_NAME}."
	if [ ${DB_ACTION} == "create" ]
	then
		echo "Action CREATE specified - creating new database..."
		createdb ${SUPER_USER_ENV} --encoding="UTF-8" --owner=${DB_USER} ${DB_NAME}
	else
		echo "Action UPDATE specified with NO existing database - no database will be created. Exiting..."
		exit 1	
	fi
fi

# create plgsql language if necessary
if createlang ${SUPER_USER_ENV} --dbname=${DB_NAME} plpgsql >/dev/null
then
	echo "Language plpgsql enabled."
else
	if [ $? == 2 ]
	then
		echo "plpgsql is already enabled for database ${DB_NAME}, continuing."
	else
		echo "Error: enabling plpgsql language for ${DB_NAME} failed, exiting."
		exit 2
	fi
fi

# execute the update
if [ -r ${INIT_SCRIPT} ]
then
	echo "Initializing/Upgrading database ${DB_NAME} using plpgsql script '${INIT_SCRIPT}'..."
	psql ${SUPER_USER_ENV} --dbname=${DB_NAME} -f ${INIT_SCRIPT}
	if psql ${SUPER_USER_ENV} --dbname=${DB_NAME} -c "SELECT create_or_adapt_sourcebox_schema('${DB_USER}', '${DB_USER}')"
	then
		echo "Initialization/Upgrade successful."
	else
		echo "An error occurred while initializing the database, exiting."
		exit 3
	fi
else
	echo "Error: database initialization script not found: ${INIT_SCRIPT}, exiting"
	exit 4
fi
