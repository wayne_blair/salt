#!/bin/bash

# Wrapper script to invoke the Textkernel install script
# and avoid output in stderr (or if needed, always return success.)
# The Textkernel script writes
# to stderr and salt considers this a failure.
#
# $1 is the path to the textkernel install.sh script

f_error () {
    echo "ERROR: $0, Expected argument for textkernel install.sh missing"
    exit 1
}

[ -z $1 ] && f_error

DIR=`dirname $1`
cat <<EOF > ${DIR}/install_wrapper.log
===================================================
`date`
START ${DIR}/install_wrapper.log
Starting directory is "${PWD}"
Target  directory  is "${DIR}"
Input args: "$1"
===================================================
EOF

cd ${DIR} || printf "\n\nFAILED TO CD INTO \"${DIR}\"\n\n" | tee -a ${DIR}/install_wrapper.log  
printf  "Current directory is \"$PWD\"\n" | tee -a ${DIR}/install_wrapper.log
bash $1 2>&1 | tee -a ${DIR}/install_wrapper.log
cd - 

cat <<EOF >> ${DIR}/install_wrapper.log


===================================================
`date`
END ${DIR}/install_wrapper.log
===================================================
EOF
