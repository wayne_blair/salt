# TODO: The paths are used acros many state files. Move the paths into pillars
#       to be a common defition.
#
# NOTE: The zip file is being fetch from private s3 storage
#        that has public read access.
#
# TODO:  A better way to coordinate JAVA_HOME
{% set TK_ROOT = "/opt/textkernel" %}
{% set TOMCAT_ROOT = "/opt/apache-tomcat" %}
{% set TOMCAT_SERVICE = "apache-tomcat" %}
{% set TOMCAT_OWNER = "apache-tomcat" %}
{% set TOMCAT_GROUP = "apache-tomcat" %}
{% set NAME = "search" %}
{% set APPNAME = "SearchBox" %}
{% set VERSION = '3.x-beta-3b' %}
{% set SKR = "SearchBoxKnowledgeRelease_1.13" %}

#include:
#  - apache-tomcat
#  - textkernel.extract
#  - cassandra
#  - elasticsearch


# NOTE:
#  *  This zip file was extracted from the Rick Lackey provided zip.
#  *  The war file is delivered in this zip but I will manage the war as a
#     seperate entity for now becuase it is less messy than states to
#     copy files around on a minion file system.
#  *  Keep the downloaded archive in the minion's cache. Useful
#     during development. Might was to disable this later.

stop-apache-tomcat:
  service.dead:
    - name: {{ TOMCAT_SERVICE }}

# Force the ownership of the directory tree before unpacking.
# If a pre-existing directory tree has wrong ownership, the 
# archive.extracted stae does not correct ownership.

remove_symlink:
  file.absent:
    - name: {{ TK_ROOT }}/{{ NAME }}

remove_dir:
  file.absent:
    - name: {{ TK_ROOT }}/{{ NAME }}-{{ VERSION }} 
    - require:
      - file: remove_symlink

unpack_search:
  archive.extracted:
    - keep: True
    - name: {{ TK_ROOT }}/
    - source: https://s3-us-west-2.amazonaws.com/erecruit-us-west-2/wb-textkernel/Search-match-3.x-beta-3b-Linux-64bit.zip
    - source_hash: md5=ed72fa4ce21630c00c8f410f0c3a2324
    - archive_format: zip
#    - user: {{ TOMCAT_OWNER }}
#    - group: {{ TOMCAT_GROUP }}
    - if_missing: {{ TK_ROOT }}/{{ NAME }}-{{ VERSION }}
    - require:
      - file: remove_dir
#      - file: chown_search_directory
#      - sls: apache-tomcat
#      - sls: textkernel.extract
#      - sls: cassandra
#      - sls: elasticsearch

{{ NAME }}-symlink:
  file.symlink:
      - name: {{ TK_ROOT }}/{{ NAME }}
      - target: {{ TK_ROOT }}/{{ NAME }}-{{ VERSION }}
      - force: True
      - require:
        - archive: unpack_search

# NOTE: This war file was extracted from the search.zip file that was inside the
#       Rick Lackey provided zip file. Ugly? Yes!

search-war:
  service.dead:
    - name: {{ TOMCAT_SERVICE }}

  file.managed:
    - name: {{ TOMCAT_ROOT }}/webapps/{{ APPNAME }}.war
    - source: http://s3-us-west-2.amazonaws.com/erecruit-us-west-2/wb-textkernel/{{ APPNAME }}-{{ VERSION }}.war
    - source_hash: md5=7675da8af7c2d2b57aea35dea52d87e0
    - user: {{ TOMCAT_OWNER }}
    - group: {{ TOMCAT_GROUP }}
    - requires:
      - file: search_config
#      - sls: apache-tomcat

# NOTE: This config file was extracted from the search.war, modified for out use, then
#       added to the salt tree

search_config:
  service.dead:
    - name: {{ TOMCAT_SERVICE }}

  file.managed:
    - name: {{ TOMCAT_ROOT }}/conf/Catalina/localhost/SearchBox.xml
    - source: salt://textkernel/files/SearchBox.xml
    - user: {{ TOMCAT_OWNER }}
    - group: {{ TOMCAT_GROUP }}
    - makedirs: True

# NOTE: 
#   * Keep the downloaded archive in the minion's cache. Useful
#     during development. Might was to disable this later.
#
#   * The archive.extracted state does not support
#     passing options to unzip
#       * The archive.unzip module does claim to pass options
#         but I cannot get it to find the zip file in the specified
#         location.
#       * Forced to break out to the shell for this operation
#   * zip will overwrite preexisting files in target dirctory

unpack_searchKnowledge:
  service.dead:
    - name: {{ TOMCAT_SERVICE }}

  file.managed:
    - name: /tmp/{{ SKR }}.zip
    - source: https://s3-us-west-2.amazonaws.com/erecruit-us-west-2/wb-textkernel/{{ SKR }}.zip
    - source_hash: md5=bbbc86a22071548499d32e1ec4402ac1
    - mode: 664
    - user: {{ TOMCAT_OWNER }}
    - group: {{ TOMCAT_GROUP }}

  cmd.run:
    - name: unzip -o -j /tmp/{{ SKR }}.zip -d {{ TK_ROOT }}/{{ NAME }}/{{ SKR }} && rm -f /tmp/{{ SKR }}.zip
#    - if_missing: {{ TK_ROOT }}/{{ NAME }}/{{ SKR }}

{{ SKR }}-symlink:
  file.symlink:
      - name: {{ TK_ROOT }}/{{ NAME }}/knowledge
      - target: {{ TK_ROOT }}/{{ NAME }}/{{ SKR }}
      - force: True
      - require:
        - file: unpack_searchKnowledge
        - file: {{ NAME }}-symlink

elasticsearch-custom-config:
  service.dead:
    - name: {{ TOMCAT_SERVICE }}

  file.managed:
    - name: {{ TK_ROOT }}/{{ NAME }}/elasticsearch/config/custom.yml
    - source: salt://textkernel/files/search-elasticsearch-custom.yml
    - user: {{ TOMCAT_OWNER }}
    - group: {{ TOMCAT_GROUP }}
    - makedirs: True
    - require:
      - file: {{ NAME }}-symlink

# Lastly ensure everything is owned by tomcat
chown_search_directory:
  service.dead:
    - name: {{ TOMCAT_SERVICE }}

  file.directory:
    - name: {{ TK_ROOT }}/{{ NAME }}-{{ VERSION }}
    - user: {{ TOMCAT_OWNER }}
    - group: {{ TOMCAT_GROUP }}
    - recurse:
      - user
      - group

search-start-apache-tomcat:
  service.running:
    - name: apache-tomcat

