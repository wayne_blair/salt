# NOTE: Textkernel package and service naming conventions are inconsistent.
#       The server package name is "KnowledgeServer" but the service name 
#       is "jobfeed_knowledge"
#
#       The RPMs are currently fetched from a private external bucket, not the salt tree
#
#       The RPM creates a user beyond my control. I've had situation where
#       temp files and log directories don't get deleted upon uninstall.
#       Reinstalling the packages might not get the same UID/GID and ownership mismatch on the
#       stranded directories will cause really obscure errors like:
#            "polkitd[696]: Unregistered Authentication Agent for unix-process..."
#       Force the ownership of the directories to the user/group name (ACCOUNT) created by the RPMs.
#       I don't know if Debian/Ubuntu behaves the same way.

{% set KNOWLEDGE_RPM = 'KnowledgeServer-1.7-201601151620.fc20.x86_64.rpm' %}
{% set JOBFEED_RPM = 'JobfeedKnowledge-static-1.4-201601151619.fc20.noarch.rpm' %}
{% set ACCOUNT = 'jfknowlg' %}

packages:
  pkg.installed:
    - sources:
       - KnowledgeServer: https://s3-us-west-2.amazonaws.com/erecruit-us-west-2/wb-textkernel/{{ KNOWLEDGE_RPM }} 
       - JobfeedKnowledge-static: https://s3-us-west-2.amazonaws.com/erecruit-us-west-2/wb-textkernel/{{ JOBFEED_RPM }}

# The salt service module cannot be use for managing SYS-V serivces.
# I have to break this down into seperate cmd.run calls
# The salt service functionality runs systemctl list-unit-files and expect to find a .service
# file.  That file does not exist for SYS-V scripts because it is auto-generated when 
# systemd is initialized or reloaded.

#  defensive_chown
{% for DIR in '/tmp/pdk-jfknowlg', '/var/log/jobfeed_knowledge' %}
{{ DIR }}:
  file.directory:
    - user: {{ ACCOUNT }}
    - group: {{ ACCOUNT }}
    - dir_mode: 755
    - file_mode: 644
    - recurse:
      - user
      - group
      - mode
{% endfor %}

KnowledgeServer_enable:
  cmd.run:
    - name: "systemctl enable jobfeed_knowledge"
    - requires:
      - pkg: KnowledgeServer
      - pkg: JobfeedKnowledge-static
      - file: /tmp/pdk-jfknowlg
      - file: /var/log/jobfeed_knowledge

KnowledgeServer_start:
  cmd.run:
    - name: "systemctl start jobfeed_knowledge"
    - require:
      - cmd: KnowledgeServer_enable

