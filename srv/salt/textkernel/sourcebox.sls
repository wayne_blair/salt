# TODO: 
#     * IMPORTANT: 
#          * The postgres user "web" password needs to be coordinated between this file
#            and sourcebox.xml until I move this into pillars.
#          * DEFINE TOMCAT ROOT IN SOME CENTRAL PLACE. Until then coordinate this 
#            with apache-tomcat state
#
#     * Figure salt tomcat state to deploy or restart the sourcebox webapp
#
#     * Consider replacing the call to the textkernel sourcebox DB init script
#       with salt postgres state. Right now the postgres state/modules are 
#       lacking needed functionality or it's too convoluted to figure out.
#
#     * I'm explictly setting a postgres version here. Including the postgres.sls
#       does nt give us access to that states variables.  States are not paramaterized
#       so we cannt easily pass variables around.  SOme suggest jinga templates,
#       others using import. It's not stright forward. See these URLS for more info.
#         http://stackoverflow.com/questions/22503810/passing-variables-between-salt-states
#         https://docs.saltstack.com/en/latest/ref/states/vars.html
#         https://github.com/saltstack/salt/issues/8878
#
#     * IMPORTANT: The postgres user "web" password needs to be coordinated between this file
#       and sourcebox.xml until I move this into pillars.
#
#     * Disabled the ID's for cassandra, elasticsearch, textkernel.knowledge, and apache-tomcat. The first two
#       are not needed for the embedded services (and will have port conflicts if enabled).
#       textkernel.knowledge and apache-tomcat are disabled because I'm getting circular dependencies and will
#       assume the textkernel init.sls got knowledge installed. apache-tomcat circular dependencies need to get fixed.
#
# NOTE:
#     * sourcebox embedded elastic saearch will fail if  /opt/textkernel/search/conf/custom.yml is missing.
#       a zero length file is required. Specif file name is defined in /opt/apache-tomcat/conf/Catalina/localhost/SearchBox.xml
#
#     * I intentionally avoid user/group/directories named "tomcat" to avoid as many package collisions as I can.
#
# The raid state will create mount /opt/textkernel


{% set POSTGRES_SERVICE = 'postgresql-9.4' %}
{% set TKROOT = "/opt/textkernel" %}
{% set TOMCAT_ROOT = "/opt/apache-tomcat" %}
{% set TOMCAT_SERVICE = "apache-tomcat" %}
{% set TOMCAT_OWNER = "apache-tomcat" %}
{% set TOMCAT_GROUP = "apache-tomcat" %}

include:
#  - cassandra
#  - elasticsearch
  - postgresql
  - apache-tomcat
#  - textkernel.knowledge

{% for USER in 'jfknowlg', %}
{{ USER }}:
  group:
    - present
  user:
    - present
    - gid_from_name: True
    - require:
      - group: {{ USER }}
{% endfor %}


{{ TKROOT }}/sourcebox:
  file.directory:
    - require:
      - user: {{ TOMCAT_OWNER }}
      - group: {{ TOMCAT_GROUP }}
    - user: {{ TOMCAT_OWNER }}
    - group: {{ TOMCAT_GROUP }}
    - mode: '0755'
    - makedirs: True
    - recurse:
      - user
      - group


{% for DIRS in 'modelfiles', 'configuration', 'upload' %}
{{ TKROOT }}/sourcebox/{{ DIRS }}:
  file.directory:
    - require:
      - file: {{ TKROOT }}
      - user: {{ TOMCAT_OWNER }}
      - group: {{ TOMCAT_GROUP }}
    - user: {{ TOMCAT_OWNER }}
    - group: {{ TOMCAT_GROUP }}
    - mode: 770
    - makedirs: True
    - recurse:
      - user
      - group
    - require:
      - user: {{ TOMCAT_OWNER }}
{% endfor %}


# NOTE: The various zip files need to be unpacked to obtain the
#       sourcebox.war. sourcebox.xml is META-INF/context.xml
#       extracted from the war, customized, and added to the salt tree.
#       Same thing with the database initialization files.
#       


sourcebox_config_dir:
  service.dead:
    - name: {{ TOMCAT_SERVICE }}

  file.directory:
    - name: {{ TOMCAT_ROOT }}/conf/Catalina/localhost
    - require:
      - user: {{ TOMCAT_OWNER }}
      - group: {{ TOMCAT_GROUP }}
#      - sls: apache-tomcat
    - user: {{ TOMCAT_OWNER }}
    - group: {{ TOMCAT_GROUP }}
    - mode: '0755'
    - makedirs: True
    - recurse:
      - user
      - group
 
sourcebox_config:
  service.dead:
    - name: {{ TOMCAT_SERVICE }}

  file.managed:
    - name: {{ TOMCAT_ROOT }}/conf/Catalina/localhost/sourcebox.xml
    - source: salt://textkernel/files/sourcebox.xml
    - user: {{ TOMCAT_OWNER }}
    - group: {{ TOMCAT_GROUP }}
    - require:
      - file: sourcebox_config_dir

sourcebox_war:
  service.dead:
    - name: {{ TOMCAT_SERVICE }}

  file.managed:
    - name: {{TOMCAT_ROOT }}/webapps/sourcebox.war
    - source: salt://textkernel/files/sourcebox.war
    - user: {{TOMCAT_OWNER }}
    - group: {{ TOMCAT_GROUP }}
    - requires:
      - file: sourcebox_config
#     - sls: apache-tomcat
#      - sls: cassandra
#      - sls: elasticsearch
#      - sls: textkernel.knowledge


{% for MODEL in 'ERecruitVac.xml', 'MatchCVModelEN.xml', 'TextkernelAdministrator.xml' %}
{{ TKROOT }}/sourcebox/modelfiles/{{ MODEL }}:
  file.managed:
    - source: salt://textkernel/files/{{ MODEL }}
    - user: {{ TOMCAT_OWNER }}
    - group: {{ TOMCAT_GROUP }}
    - mode: 550
    - require_in:
      - file: sourcebox_war
{% endfor %}



#       This state must only run if the DB does not exist
#
# ###############################################
# disable the call to the TK script for 
# now because we are going to import
# a backup of a pre-initialized DB 
# with erecruit test accounts, workflows, users
# ###############################################
#

#sourcebox_database_sql:
#  file.managed:
#    - name: /tmp/initialize_sourcebox_database.sql
#    - source: salt://textkernel/files/initialize_sourcebox_database.sql
#    - user: postgres
#    - group: postgres
#    - mode: 440
#    - requires:
#      - sls: postgresql
#      - postgres_user: web
#
#{% set SCRIPT = "/tmp/initialize_sourcebox_database.sh" %}
#sourcebox_database_init:
#  file.managed:
#    - name: {{ SCRIPT }}
#    - source: salt://textkernel/files/initialize_sourcebox_database.sh
#    - user: postgres
#    - group: postgres
#    - mode: 550
#    - requires:
#      - file: sourcebox_database_sql
#
#  cmd.script:
#    - name: {{ SCRIPT }}
#    - args:  "5432 create postgres sourcebox web"
#    - cwd: /tmp
#    - requires:
#      - file: sourcebox_database_init
#
#
# ######################## New stuff populate workflows, etc from backup dump #####################
# NOTE: This state requires an initial sourcebox postgres database dump.
#       Database was created by tools provided with sourcebox. For example
#         /tmp/sourcebox-3.3.60.2/postgresql/sql/initialize_sourcebox_database.sh \
#             5432 \
#             create \
#             postgres \
#             sourcebox \
#             web
# 

sourcebox_database_user:
  postgres_user.present:
    - name: web
    - password: Ready4summer
    - require:
      - service: {{ POSTGRES_SERVICE }}

sourcebox_dump_file:
  service.dead:
    - name: {{ TOMCAT_SERVICE }}

  file.managed:
    - name: /tmp/sourcebox.dmp
    - source: salt://textkernel/files/backup-sear-tk01-full-sourcebox-dump-2016-02-26-15-46-B.sql
    - owner: postgres
    - requires: create_sourcebox_db

create_sourcebox_db:
  service.dead:
    - name: {{ TOMCAT_SERVICE }}

  postgres_database.present:
    - name: sourcebox
    - owner: web
    - user: postgres
    - db_password: Ready4summer

populate_sourcebox_db:
  service.dead:
    - name: {{ TOMCAT_SERVICE }}

  cmd.run:
    - name: su - postgres -c "psql sourcebox < /tmp/sourcebox.dmp"
    - require:
      - file: sourcebox_dump_file


# Lastly, restart tomcat

sourcebox-start-apache-tomcat:
  service.running:
    - name: {{ TOMCAT_SERVICE }}
