# NOTE:  The extract zip file is being fetch from private s3 storage
#        that has public read access.
#
# TODO:  A better way to coordinate JAVA_HOME
{% set JHOME = "/opt/apache-tomcat" %}
{% set TK_ROOT = "/opt/textkernel" %}
{% set TMPDIR = "/tmp/extract" %}

textkernel-ancillary-packages:
  pkg.latest:
    - pkgs:
      - unzip
      - fontconfig.x86_64
      - fontconfig.i686
      - zlib.x86_64
      - zlib.i686
      - libXext.x86_64
      - libXext.i686
      - libstdc++.x86_64
      - libstdc++.i686

{% for USER in 'txtor3',  %}
{{ USER }}:
  group:
    - present
  user:
    - present
    - gid_from_name: True
    - require:
      - group: {{ USER }}
{% endfor %}


txtor3-env-setup:
  file.blockreplace:
    - name: /home/txtor3/.bashrc
    - marker_start: "###### BLOCK TOP: Managed by salt for textkernel, do not edit the block"
    - marker_end:   "###### BLOCK END: End of salt managed block"
    - content: |

        export JAVA_HOME=/usr/lib/jvm/jre/
        export PATH=$PATH:${JAVA_HOME}/bin

    - show_changes: True
    - append_if_not_found: True
    - require:
      - user: txtor3

tk_root_dir_check:
  file.directory:
    - name: {{ TK_ROOT }}
    - user: root
    - group: root
    - mode: '0755'
    - makedirs: True
#    - recurse:
#      - user
#      - group
#      - mode

{{ TK_ROOT }}/extract:
  file.directory:
    - requires:
      - user: txtor3
      - group: txtor3
    - user: txtor3
    - group: txtor3
    - mode: '0755'
    - makedirs: True
    - recurse:
      - user
      - group

tmp_setup:
  file.absent:
    - name: {{ TMPDIR }}/

# NOTE: The trailing slash on the archive name is required
#       This is the name of the directory UNDER which the archive
#       is extracted. This assumes the top level of the 
#       archive contains only a directory.
#
#       Do not set ownership of the extracted directory becuase
#       it will also change the parent directory and things
#       get messy when you change the ownership of everything
#       in /tmp.  It would be better to use salt.module.temp 

unpack_extract:
  archive:
    - extracted
    - name: /tmp/
    - source: https://s3-us-west-2.amazonaws.com/erecruit-us-west-2/wb-textkernel/extract.zip
    - source_hash: md5=e526dff889dd29634ef5cb89369c92d8
    - archive_format: zip
#    - user: txtor3
#    - group: txtor3
    - if_missing: {{ TMPDIR }}

# Ensure the default license file is removed
# then replace it with the erecruit license file

delete_default_license:
  file.absent:
    - name: {{ TMPDIR }}/txtor.license
    - require:
      - archive: unpack_extract

# NOTE: The license file is delivered in erecruit_textractor_license.zip.
#       Extract the license and check it into SCM.
erecruit_license:
  file.managed:
    - name: {{ TMPDIR }}/txtor.license
    - source: salt://textkernel/files/txtor.license
    - requires:
      - user: txtor3
      - group: txtor3
      - archive: unpack_extract
    - user: txtor3
    - group: txtor3

# Run the textkernel install script. I don't want to do this
# but this means we don't have to port textkernel fixes into
# our own implementation. The extract.zip has canned txtor.license
# file that we have to remove and replace with our license file.
# it must be located in the same directory as their install.sh and
# is must be owned by txtor3 becuase they don't set ownership.
# The wrong ownership will not be noticed until the textractor
# service starups fail.
#
# The wrapper script is simple to cd into the extract temporary
# directory becuase their install script can't cope with
# being started outside the directory.
#
# The creates clause checking for the Models directory
# below intentionally prevents the shell script running more than once.
# I create and ensure the ownercship of the ./extract directory 
# to the cmd.run needs to check for something else.
# If this is a new install there will be no ./extract/Models directory
# and the install script will run otherwise it will find the directory
# and intentionally not run the script.
#
# The TK script might support updates but it takes
# about 10-15 minutes to run and we can't afford that
# because scheduled minon runs will backup.
# 
#
# The TK installer will stop the extractors but 
# not sourcebox so there is a wide window where source
# box could fail due to no running extractors.
# 
# TODO: 
#    * Get TK to produce RPMs that deal with this stuff.
#
#    * Until then we should test for the license file under
#      extract and exit before we fetch and unpack the zip file.
#      A new release of extract means we need to delete the /opt/textkernel/extract
#      directory and rerun the extract.sls state.manyu hu
#
#    * Clean up temp files (extract.zip adn the /tmp/extract directory)

run_extract_install:
  file.managed:
    - name: {{ TMPDIR }}/install_wrapper.sh
    - source: salt://textkernel/files/install_wrapper.sh
    - mode: 755
    - require:
      - archive: unpack_extract
  cmd.run:
    - name: {{ TMPDIR }}/install_wrapper.sh {{ TMPDIR }}/install.sh
    - creates: {{ TK_ROOT }}/extract/Models
    - requires:
      - user: txtor3
      - group: txtor3

# The TK install.sh fails to set ownership of the license file
# inspite of us setting it in their temp location. They
# don't preserve or set ownership and this causes
# the extractor model service startup to fail.

fix_TK_license:
  file.managed:
    - name: {{ TK_ROOT }}/extract/Textractor/cfg/txtor.license
    - source: salt://textkernel/files/txtor.license
    - requires:
      - user: txtor3
      - group: txtor3
    - user: txtor3
    - group: txtor3

set_owner:
  file.directory:
    - name: {{ TK_ROOT }}/extract
    - requires:
      - user: txtor3
      - group: txtor3
    - user: txtor3
    - group: txtor3
    - recurse:
      - user
      - group

replace_init:
  file.managed:
    - name: /etc/init.d/rc.textkernel
    - source: salt://textkernel/files/rc.textkernel

