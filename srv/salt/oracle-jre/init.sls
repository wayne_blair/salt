# NOTE:
#      * Oracle's paywall is a PITA. We can wget to fetch the rpm with cookies and blood signing
#        the license agreement but I have not figured out how to get yum or salt to install
#        with the PITA options.
#
#        Also, TK is currently requireing jre 1.7 and that is getting harder to find so I 
#        am storing the rpm in private s3 storge for now.
#
#      * Will install to default location but not set up any systemic environment.
#        I expect the consumers to set up their environment to reference 
#        /usr/java/latest (or what ever)

{% set URL = 'https://s3-us-west-2.amazonaws.com/erecruit-us-west-2/wb-textkernel' %}
{% set MAJOR = 'jre-7' %}
{% set UPDATE = 'u79' %}

oracle-jre:
  pkg.installed:
    - sources:
      - jre: {{ URL }}/{{ MAJOR }}{{ UPDATE }}-linux-x64.rpm

