# Define the raid mount point based upon machine naming convention
# of <something>-XY<something>
#
# TODO: This is a first pass at the Jinga
#       Improve this by looping over a list of host types
#       and eliminate this mess
#
{% set MOUNTPOINT = '/mnt/raid0' %}
{% if grains['host'].lower().find('-es') != -1 %}
{% set MOUNTPOINT = '/var/lib/elasticsearch' %}
{% elif grains['host'].lower().find('-tk') != -1 %}
{% set MOUNTPOINT = '/opt/textkernel' %}
{% endif %}

raid_packages:
  pkg.installed:
    - pkgs:
      - mdadm
      - lvm2

/dev/md0:
  cmd.run:
    - name: |
        mountpoint -q {{ MOUNTPOINT }} && umount {{ MOUNTPOINT }} || true
        echo "YES" | mdadm --create /dev/md0 --level 0 --raid-devices 4 /dev/sd[cdef] --force --assume-clean
        mkfs.xfs -f /dev/md0
        mkdir -p /etc/mdadm
        mdadm --detail --scan >> /etc/mdadm/mdadm.conf
    - creates: /dev/md0
    - unless: test -b /dev/md0
    - require_in:
      - mount: {{ MOUNTPOINT }}
    - require:
      - pkg: raid_packages

{{ MOUNTPOINT }}:
  mount.mounted:
    - device: /dev/md0
    - fstype: xfs
    - mkmnt: True
    - persist: True
    - opts:
      - defaults
      - noatime
