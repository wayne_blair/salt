# TODO:
#   * This is a first pass to segregate the TK requirement
#     for an older elastic search.  Turn this into a pillar
#     or some other single point of management
#     so I don't have to keep track of conditionalized states.
#
#   * This is currently specific to the domain name we use in Azure
#     not the new amazon environment. The cluster name inserted
#     into the elasticsearch.yml might not be what we want
#
# Ensure the raid array and mount point are in place

{% set RAID = 'raid0Dangerous' %}
{% set JAVA = 'openjdk' %}
{% set PKG = 'elasticsearch' %}
{% set CONFIG = '/etc/elasticsearch/elasticsearch.yml' %}

# Extract the base of the domain name to inject
# it into the yaml configuration file for cluster node names

{% set CLUSTER = grains['domain'].lower().replace('.hosted','') %}
{% set NODE = grains['host'].lower() %}
{% set VERSION = 'latest' %}

{% if grains['host'].lower().find('-tk') != -1 %}
{% set VERSION = '2.1.*' %}
{% endif %}

include:
  - {{ RAID }}
  - {{ JAVA }}

{{ PKG }}:
  pkg.installed:
    - require:
      - file: /etc/yum.repos.d/{{ PKG }}.repo
#      - sls: {{ RAID }}
      - sls: {{ JAVA }}
    - version: {{ VERSION }}

/etc/yum.repos.d/{{ PKG }}.repo:
  file.managed:
    - source: salt://elasticsearch/files/elasticsearch.repo
    - user: root
    - group: root
    - mode: '0440'

{{ CONFIG }}:
  file.managed:
    - source: salt://elasticsearch/files/elasticsearch.yml
    - template: jinja
    - user: root
    - group: elasticsearch
    - mode: '0440'
    - require:
      - pkg: {{ PKG }}
    - context:
        CLUSTER: {{ CLUSTER }}
        NODE:  {{ NODE }}

elasticsearch_service:
  service:
    - name: elasticsearch
    - enable: true
    - running
    - watch:
      - file: {{ CONFIG }}
    - require:
      - pkg: {{ PKG }}

