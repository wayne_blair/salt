{% set FN = "apache-tomcat" %}
{% set SN = "tomcat" %}
{% set MAJOR = "7" %}
{% set MINOR = "0" %}
{% set PATCH = "68" %}
{% set VERSION = MAJOR + "." + MINOR + "." + PATCH %}
{% set URL = "https://s3-us-west-2.amazonaws.com/erecruit-us-west-2/wb-textkernel/"+ FN  + "-" + MAJOR + "." + MINOR + "." + PATCH + ".tar.gz" %}
{% set HASH = "md5=94688679d5f37499d1bd1a65eb9540e7" %}
{% set ROOT = "/opt" %}
{% set ID = "2000" %}
{% set JDBC = "postgresql-9.4.1207.jar" %}

# NOTE: The trailing slash on the archive name is required
#       This is the name of the directory UNDER which the archive
#       is extracted. This assumes the top level of the 
#       archive contains only a directory.
{{ FN }}:
  archive:
    - extracted
    - name: {{ ROOT }}/
    - source: {{ URL }}
    - source_hash: {{ HASH }}
    - archive_format: tar
    - if_missing: {{ ROOT }}/{{ FN }}-{{ VERSION }}
    - require:
      - user: {{ FN }}-user
      - user: {{ FN }}
      - group: {{ FN }}
    - user: {{ FN }}
    - group: {{ FN }}

{{ SN }}-symlink:
  file.symlink:
    - name: {{ ROOT }}/{{ FN }}
    - target: {{ ROOT }}/{{ FN }}-{{ VERSION }}
    - force: True
    - require:
      - archive: {{ FN }}

{{ FN }}-group:
  group.present:
    - name: {{ FN }}
    - gid: {{ ID }}
    - system: False

{{ FN }}-user:
  user.present:
    - name: {{ FN }}
    - shell: /bin/nologin
    - home: {{ ROOT }}/{{ FN }}
    - uid: {{ ID }}
    - gid_from_name: True
    - createhome: False
    - require:
      - group: {{ FN }}-group

{{ FN }}-service-file:
  file.managed:
    - name: /usr/lib/systemd/system/{{ FN }}.service
    - source: salt://{{ FN }}/files/{{ FN }}.service
    - mode: 644
    - require:
      - file: {{ SN }}-symlink

{{ FN }}-jdbc-file:
  file.managed:
    - name: {{ ROOT }}/{{ FN }}/lib/{{ JDBC }}
    - source: salt://{{ FN }}/files/{{ JDBC }}
    - mode: 644
    - user: {{ FN }}
    - group: {{ FN }}
    - require:
      - file: {{ SN }}-symlink

{{ FN }}-ownership:
  file.directory:
    - name: {{ ROOT }}/{{ FN }}
    - user: {{ FN }}
    - group: {{ FN }}
    - recurse:
      - user
      - group


{{ FN }}.service:
  service.running:
    - enable: True
    - requires:
      - file: {{ FN }}-service-file
      - file: {{ FN }}-jdbc-file
    - watch:
      - file: {{ ROOT }}/{{ FN }}/conf/*

{{ SN }}-users-file:
  file.managed:
    - name: {{ ROOT }}/{{ FN }}/conf/{{ SN }}-users.xml
    - source: salt://{{ FN }}/files/{{ SN }}-users.xml
    - mode: 600
    - user: {{ FN }}
    - group: {{ FN }}
    - require:
      - file: {{ SN }}-symlink 
