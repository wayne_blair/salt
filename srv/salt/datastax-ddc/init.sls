# NOTE: Cassandra and Datastax follow a non-standard RPM package version standard
#       The version of the product is defined by the version specific in the repository URL
#       in files/datastac-ddc.repo

# Ensure the raid array and mount point are in place

{% set JAVA = 'openjdk' %}
{% set PKG = 'datastax-ddc' %}

include:
  - {{ JAVA }}

/etc/yum.repos.d/{{ PKG }}.repo:
  file.managed:
    - source: salt://cassandra/files/{{ PKG }}.repo
    - user: root
    - group: root
    - mode: '0440'


{{ PKG }}:
  pkg.installed:
    - require:
      - file: /etc/yum.repos.d/{{ PKG }}.repo
      - sls: {{ JAVA }}

{{ PKG }}-tools:
  pkg.installed:
    - require:
      - file: /etc/yum.repos.d/{{ PKG }}.repo
      - sls: {{ JAVA }}


# The salt service module cannot be use for managing SYS-V serivces.
# I have to break this down into seperate cmd.run calls
# The salt service functionality runs systemctl list-unit-files and expect to find a .service
# file.  That file does not exist for SYS-V scripts because it is auto-generated when
# systemd is initialized or reloaded.

cassandra_enable:
  cmd.run:
    - name: chkconfig cassandra on
    - requires:
      - pkg: {{ PKG }}

cassandra_start:
  cmd.run:
    - name: service cassandra restart
    - require:
      - cmd: cassandra_enable
