ntp_packages:
  pkg.installed:
    - pkgs:
      - ntp
      - ntpdate

ntp_conf_file:
  file.managed:
    - name: /etc/ntp.conf
    - source: salt://ntp/files/ntp.conf
    - user: root
    - group: root 
    - mode: 644
    - require:
      - pkg: ntp_packages

ntp_service:
  service:
    - name: ntpd
    - running
    - enable: true
    - require:
      - file: ntp_conf_file
      - pkg: ntp_packages
    - watch:
      - file: ntp_conf_file

ntpdate_service:
  service:
    - name: ntpdate
    - dead
    - enable: true
    - require:
      - file: ntp_conf_file
      - pkg: ntp_packages
    - watch:
      - file: ntp_conf_file


