include:
  - disk.format

disk_mount:
  mount.mounted:
    - name: /SALT
    - device: /dev/saltVG/saltLV
    - fstype: ext4
    - mkmnt: True
    - opts: defaults
    - persist: True
    - dump: 0
    - pass_num: 0
    - require:
      - blockdev: disk_volume
#    - onfail:
#      - mount: disk_mount_retry

#disk_mount_retry:
#  mount.mounted:
#    - name: /SALT
#    - device: /dev/saltVG/saltLV
#    - fstype: ext4
#    - mkmnt: True
#    - opts: defaults
#    - persist: True
#    - dump: 0
#    - pass_num: 0
#    - require:
#      - blockdev: disk_volume
#
#
