include:
  - lvm

disk_volume:
  blockdev.formatted:
    - name: /dev/saltVG/saltLV
    - fs_type: ext4
    - kwargs: '-f'
    - require:
      - lvm: saltVG

